/*
package com.access.cargo.dashboard.kpi.service;


import com.access.cargo.dashboard.kpi.model.redis.*;
import com.access.cargo.dashboard.kpi.repo.DashboardKpiChartRedisRepo;
import com.access.cargo.dashboard.kpi.repo.DashboardKpiSummaryRedisRepo;
import com.access.cargo.dashboard.kpi.repo.DashboardKpiTableRedisRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

@Slf4j
@Service
public class DashboardKpiServicebak {

    @Autowired
    DashboardKpiSummaryRedisRepo dashboardKpiSummaryRedisRepo;

    @Autowired
    DashboardKpiTableRedisRepo dashboardKpiTableRedisRepo;

    @Autowired
    DashboardKpiChartRedisRepo dashboardKpiChartRedisRepo;

    public void dummyDashboardKpiTableAgentAndAirlinesTodayRedis(String dt1,String dt2) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate1 = LocalDate.parse(dt1, formatter);
        LocalDate localDate2 = LocalDate.parse(dt2, formatter);
        LocalDate date1 = LocalDate.of(localDate1.getYear(),localDate1.getMonth(),localDate1.getDayOfMonth());
        LocalDate date2 = LocalDate.of(localDate2.getYear(),localDate2.getMonth(),localDate2.getDayOfMonth());

        Collection<DashboardKpiTableRedis> DashboardKpiTableRedisAgent = new LinkedList<>();
        Collection<DashboardKpiTableRedis> DashboardKpiTableRedisAirlines = new LinkedList<>();
        int i=1;
        int j=1000;
        for (LocalDate date = date1; date.isBefore(date2); date = date.plusDays(1)) {
            DashboardKpiTableRedis dashboardKpiTableRedisAgent = new DashboardKpiTableRedis();
            DashboardKpiTableRedis dashboardKpiTableRedisAirlines = new DashboardKpiTableRedis();
            DashboardKpiParams dashboardKpiParams = new DashboardKpiParams();
            dashboardKpiParams.setBranch_("ALL");
            dashboardKpiParams.setDate_(date);
            dashboardKpiParams.setMonth_(date.getMonthValue());
            dashboardKpiParams.setYear_(date.getYear());

            //agent
            dashboardKpiTableRedisAgent.setId(Long.valueOf(i));
            dashboardKpiTableRedisAgent.setParams(dashboardKpiParams);
            dashboardKpiTableRedisAgent.setHeader("Agent");
            LinkedList<DashboardKpiTableDataRedis> dashboardKpiTableDataAgentRedis = setTabelDataAgentTodayRedis();
            dashboardKpiTableRedisAgent.setData(dashboardKpiTableDataAgentRedis);
            DashboardKpiTableRedisAgent.add(dashboardKpiTableRedisAgent);

            //airline
            dashboardKpiTableRedisAirlines.setId(Long.valueOf(j));
            dashboardKpiTableRedisAirlines.setParams(dashboardKpiParams);
            dashboardKpiTableRedisAirlines.setHeader("Airlines");
            LinkedList<DashboardKpiTableDataRedis> dashboardKpiTableDataAirlineRedis = setTabelDataAirlineTodayRedis();
            dashboardKpiTableRedisAirlines.setData(dashboardKpiTableDataAirlineRedis);
            DashboardKpiTableRedisAirlines.add(dashboardKpiTableRedisAirlines);
            i++;
            j++;
        }

        dashboardKpiTableRedisRepo.saveAll(DashboardKpiTableRedisAgent);
        dashboardKpiTableRedisRepo.saveAll(DashboardKpiTableRedisAirlines);

    }



    public LinkedList<DashboardKpiTableDataRedis> setTabelDataAgentTodayRedis() {
        LinkedList<DashboardKpiTableDataRedis> dashboardKpiTableDataRedisList = new LinkedList<>();


        int min1 = 10;
        int max1 = 100;
        int min2 = 0;
        int max2 = 50000;

        //JNE
        DashboardKpiTableDataRedis dashboardKpiTableDataRedis = new DashboardKpiTableDataRedis();
        dashboardKpiTableDataRedis.setName("JNE");
        dashboardKpiTableDataRedis.setBkdWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis.setBkdWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis.setRaWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis.setRaWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis.setWhoWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis.setWhoWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis.setWhtWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis.setWhtWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis.setWhiWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis.setWhiWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));

        dashboardKpiTableDataRedisList.add(dashboardKpiTableDataRedis);


        DashboardKpiTableDataRedis dashboardKpiTableDataRedis1 = new DashboardKpiTableDataRedis();
        //TIKI
        dashboardKpiTableDataRedis1.setName("TIKI");
        dashboardKpiTableDataRedis1.setBkdWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis1.setBkdWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis1.setRaWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis1.setRaWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis1.setWhoWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis1.setWhoWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis1.setWhtWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis1.setWhtWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis1.setWhiWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis1.setWhiWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedisList.add(dashboardKpiTableDataRedis1);


        DashboardKpiTableDataRedis dashboardKpiTableDataRedis2 = new DashboardKpiTableDataRedis();

        //JNT
        dashboardKpiTableDataRedis2.setName("JNT");
        dashboardKpiTableDataRedis2.setBkdWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis2.setBkdWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis2.setRaWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis2.setRaWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis2.setWhoWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis2.setWhoWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis2.setWhtWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis2.setWhtWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis2.setWhiWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis2.setWhiWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));

        dashboardKpiTableDataRedisList.add(dashboardKpiTableDataRedis2);


        DashboardKpiTableDataRedis dashboardKpiTableDataRedis3 = new DashboardKpiTableDataRedis();

        //SAP
        dashboardKpiTableDataRedis3.setName("SAP");
        dashboardKpiTableDataRedis3.setBkdWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis3.setBkdWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis3.setRaWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis3.setRaWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis3.setWhoWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis3.setWhoWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis3.setWhtWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis3.setWhtWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis3.setWhiWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis3.setWhiWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedisList.add(dashboardKpiTableDataRedis3);

        return dashboardKpiTableDataRedisList;

    }

    public LinkedList<DashboardKpiTableDataRedis> setTabelDataAirlineTodayRedis() {
        LinkedList<DashboardKpiTableDataRedis> dashboardKpiTableDataRedisList = new LinkedList<>();

        int min1 = 10;
        int max1 = 100;
        int min2 = 0;
        int max2 = 50000;

        //TRIGANA
        DashboardKpiTableDataRedis dashboardKpiTableDataRedis = new DashboardKpiTableDataRedis();
        dashboardKpiTableDataRedis.setName("Trigana");
        dashboardKpiTableDataRedis.setBkdWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis.setBkdWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis.setRaWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis.setRaWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis.setWhoWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis.setWhoWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis.setWhtWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis.setWhtWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis.setWhiWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis.setWhiWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));

        dashboardKpiTableDataRedisList.add(dashboardKpiTableDataRedis);


        DashboardKpiTableDataRedis dashboardKpiTableDataRedis1 = new DashboardKpiTableDataRedis();
        //Garuda Indonesia
        dashboardKpiTableDataRedis1.setName("Garuda Indonesia");
        dashboardKpiTableDataRedis1.setBkdWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis1.setBkdWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis1.setRaWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis1.setRaWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis1.setWhoWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis1.setWhoWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis1.setWhtWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis1.setWhtWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis1.setWhiWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis1.setWhiWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedisList.add(dashboardKpiTableDataRedis1);


        DashboardKpiTableDataRedis dashboardKpiTableDataRedis2 = new DashboardKpiTableDataRedis();

        //Citylink
        dashboardKpiTableDataRedis2.setName("Citylink");
        dashboardKpiTableDataRedis2.setBkdWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis2.setBkdWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis2.setRaWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis2.setRaWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis2.setWhoWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis2.setWhoWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis2.setWhtWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis2.setWhtWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis2.setWhiWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis2.setWhiWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));

        dashboardKpiTableDataRedisList.add(dashboardKpiTableDataRedis2);


        DashboardKpiTableDataRedis dashboardKpiTableDataRedis3 = new DashboardKpiTableDataRedis();

        //Sriwijaya
        dashboardKpiTableDataRedis3.setName("Sriwijaya");
        dashboardKpiTableDataRedis3.setBkdWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis3.setBkdWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis3.setRaWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis3.setRaWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis3.setWhoWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis3.setWhoWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis3.setWhtWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis3.setWhtWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedis3.setWhiWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
        dashboardKpiTableDataRedis3.setWhiWeightPercent(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiTableDataRedisList.add(dashboardKpiTableDataRedis3);

        return dashboardKpiTableDataRedisList;
    }

   public void summaryDummy(String dt1,String dt2) {

       DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
       LocalDate localDate1 = LocalDate.parse(dt1, formatter);
       LocalDate localDate2 = LocalDate.parse(dt2, formatter);
       LocalDate date1 = LocalDate.of(localDate1.getYear(),localDate1.getMonth(),localDate1.getDayOfMonth());
       LocalDate date2 = LocalDate.of(localDate2.getYear(),localDate2.getMonth(),localDate2.getDayOfMonth());

        DashboardKpiSummaryRedis dashboardKpiSummaryRedis1 = new DashboardKpiSummaryRedis();

       Collection<DashboardKpiSummaryRedis> dashboardKpiSummaryRedisCollection = new LinkedList<>();
       int i=1;
       for (LocalDate date = date1; date.isBefore(date2); date = date.plusDays(1)) {
           DashboardKpiParams dashboardKpiParams = new DashboardKpiParams();
           dashboardKpiParams.setBranch_("ALL");
           dashboardKpiParams.setDate_(date);
           dashboardKpiParams.setMonth_(date.getMonthValue());
           dashboardKpiParams.setYear_(date.getYear());
           dashboardKpiSummaryRedis1.setId(Long.valueOf(i));
           dashboardKpiSummaryRedis1.setParams(dashboardKpiParams);

           int min1 = 10000;
           int max1 = 300000;
           int min2 = 10000000;
           int max2 = 150000000;

           dashboardKpiSummaryRedis1.setBkdWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
           dashboardKpiSummaryRedis1.setBkdRevAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
           dashboardKpiSummaryRedis1.setRaWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
           dashboardKpiSummaryRedis1.setRaRevAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
           dashboardKpiSummaryRedis1.setWhoWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
           dashboardKpiSummaryRedis1.setWhoRevAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
           dashboardKpiSummaryRedis1.setWhtWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
           dashboardKpiSummaryRedis1.setWhtRevAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
           dashboardKpiSummaryRedis1.setWhiWeightAmount(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
           dashboardKpiSummaryRedis1.setWhiRevAmount(BigDecimal.valueOf((int)(Math.random() * (max2 - min2 + 1) + min2)));
           dashboardKpiSummaryRedisRepo.save(dashboardKpiSummaryRedis1);
           dashboardKpiSummaryRedisCollection.add(dashboardKpiSummaryRedis1);
           i++;
       }
       dashboardKpiSummaryRedisRepo.saveAll(dashboardKpiSummaryRedisCollection);


    }


    public DashboardKpiSummaryRedis getSummaryToday(String branch_, LocalDate date) {
        DashboardKpiSummaryRedis dashboardKpiSummaryRedis = dashboardKpiSummaryRedisRepo.findAll().stream().filter(x -> x.getParams().getBranch_().equals(branch_) && x.getParams().getDate_().equals(date)).findFirst().orElse(null);
        return dashboardKpiSummaryRedis;
    }

    public DashboardKpiSummaryRedis getSummaryRange(String branch_, LocalDate date1, LocalDate date2) {
        System.out.println(date1);
        System.out.println(date2);
        DashboardKpiSummaryRedis dashboardKpiSummaryRedis = new DashboardKpiSummaryRedis(); //dashboardKpiSummaryRedisRepo.findAll().stream().filter(x -> x.getParams().getBranch_().equals(branch_) && x.getParams().getDate_().equals(date)).collect(Collectors.toList());
        List<DashboardKpiSummaryRedis> dashboardKpiSummaryRedis1 = dashboardKpiSummaryRedisRepo.findAll().stream().filter(x -> x.getParams().getBranch_().equals(branch_) && x.getParams().getDate_().isAfter(date2) && x.getParams().getDate_().isBefore(date1)).collect(Collectors.toList());
        dashboardKpiSummaryRedis1.sort(Comparator.comparing(o -> o.getParams().getDate_()));

        BigDecimal sumBkdWeightAmount = BigDecimal.ZERO;
        BigDecimal sumtBkdRevAmount = BigDecimal.ZERO;
        BigDecimal sumRaWeightAmount = BigDecimal.ZERO;
        BigDecimal sumtRaRevAmount = BigDecimal.ZERO;
        BigDecimal sumWhoWeightAmount = BigDecimal.ZERO;
        BigDecimal sumtWhoRevAmount = BigDecimal.ZERO;
        BigDecimal sumWhiWeightAmount = BigDecimal.ZERO;
        BigDecimal sumtWhiRevAmount = BigDecimal.ZERO;
        BigDecimal sumWhtWeightAmount = BigDecimal.ZERO;
        BigDecimal sumtWhtRevAmount = BigDecimal.ZERO;
        for(DashboardKpiSummaryRedis dashboardKpiSummaryRedisData:dashboardKpiSummaryRedis1){
            sumBkdWeightAmount = sumBkdWeightAmount.add(dashboardKpiSummaryRedisData.getBkdWeightAmount());
            sumtBkdRevAmount = sumtBkdRevAmount.add(dashboardKpiSummaryRedisData.getBkdRevAmount());
            sumRaWeightAmount = sumRaWeightAmount.add(dashboardKpiSummaryRedisData.getRaWeightAmount());
            sumtRaRevAmount = sumtRaRevAmount.add(dashboardKpiSummaryRedisData.getRaRevAmount());
            sumWhoWeightAmount = sumWhoWeightAmount.add(dashboardKpiSummaryRedisData.getWhoWeightAmount());
            sumtWhoRevAmount = sumtWhoRevAmount.add(dashboardKpiSummaryRedisData.getWhoRevAmount());
            sumWhiWeightAmount = sumWhiWeightAmount.add(dashboardKpiSummaryRedisData.getWhiWeightAmount());
            sumtWhiRevAmount = sumtWhiRevAmount.add(dashboardKpiSummaryRedisData.getWhiRevAmount());
            sumWhtWeightAmount = sumWhtWeightAmount.add(dashboardKpiSummaryRedisData.getWhtWeightAmount());
            sumtWhtRevAmount = sumtWhtRevAmount.add(dashboardKpiSummaryRedisData.getWhtRevAmount());
        }

        dashboardKpiSummaryRedis.setBkdWeightAmount(sumBkdWeightAmount);
        dashboardKpiSummaryRedis.setBkdRevAmount(sumtBkdRevAmount);
        dashboardKpiSummaryRedis.setRaWeightAmount(sumRaWeightAmount);
        dashboardKpiSummaryRedis.setRaRevAmount(sumtRaRevAmount);
        dashboardKpiSummaryRedis.setWhoWeightAmount(sumWhoWeightAmount);
        dashboardKpiSummaryRedis.setWhoRevAmount(sumtWhoRevAmount);
        dashboardKpiSummaryRedis.setWhiWeightAmount(sumWhiWeightAmount);
        dashboardKpiSummaryRedis.setWhiRevAmount(sumtWhiRevAmount);
        dashboardKpiSummaryRedis.setWhtWeightAmount(sumWhtWeightAmount);
        dashboardKpiSummaryRedis.setWhtRevAmount(sumtWhtRevAmount);
        return dashboardKpiSummaryRedis;
    }

    public DashboardKpiTableRedis getAgent(String branch_, LocalDate date) {
        DashboardKpiTableRedis dashboardKpiTableRedis = dashboardKpiTableRedisRepo.findAll().stream().filter(x -> x.getHeader().equals("Agent") && x.getParams().getBranch_().equals(branch_) && x.getParams().getDate_().equals(date)).findFirst().orElse(null);

        return dashboardKpiTableRedis;
    }

    public DashboardKpiTableRedis getAirlines(String branch_, LocalDate date) {
        DashboardKpiTableRedis dashboardKpiTableRedis = dashboardKpiTableRedisRepo.findAll().stream().filter(x -> x.getHeader().equals("Airlines") && x.getParams().getBranch_().equals(branch_) && x.getParams().getDate_().equals(date)).findFirst().orElse(null);
        return dashboardKpiTableRedis;
    }

    public List<DashboardKpiChartRedis> getCartsLastWeek(String header, String branch_, String achievementType) {
        LocalDate date1 = LocalDate.now();
        LocalDate date2 = date1.minusWeeks(1);
        List<DashboardKpiChartRedis> dashboardKpiChartRedis = dashboardKpiChartRedisRepo.findAll().stream().filter(x -> x.getAchievementType().equals(achievementType) && x.getHeader().equals(header) && x.getParams().getBranch_().equals(branch_)).collect(Collectors.toList());
        List<DashboardKpiChartRedis> dashboardKpiChartRedis1 = dashboardKpiChartRedis.stream().filter(x -> x.getParams().getDate_().isAfter(date2) && x.getParams().getDate_().isBefore(date1)).collect(Collectors.toList());
        dashboardKpiChartRedis1.sort(Comparator.comparing(o -> o.getParams().getDate_()));
        return dashboardKpiChartRedis1;
    }

    public List<DashboardKpiChartRedis> getCartsLastMonth(String header, String branch_, String achievementType,LocalDate date1, LocalDate date2) {
        List<DashboardKpiChartRedis> dashboardKpiChartRedis = dashboardKpiChartRedisRepo.findAll().stream().filter(x -> x.getAchievementType().equals(achievementType) && x.getHeader().equals(header) && x.getParams().getBranch_().equals(branch_)).collect(Collectors.toList());
        List<DashboardKpiChartRedis> dashboardKpiChartRedis1 = dashboardKpiChartRedis.stream().filter(x -> x.getParams().getDate_().isAfter(date2) && x.getParams().getDate_().isBefore(date1)).collect(Collectors.toList());
        dashboardKpiChartRedis1.sort(Comparator.comparing(o -> o.getParams().getDate_()));

        List<DashboardKpiChartRedis> dashboardKpiChartRedisColl = new LinkedList<>();
        for(DashboardKpiChartRedis dashboardKpiChartRedisdata : dashboardKpiChartRedis1){
            DashboardKpiChartRedis dashboardKpiChartRedis2 = new DashboardKpiChartRedis();
            Integer day = dashboardKpiChartRedisdata.getParams().getDate_().getDayOfMonth();
            LinkedList<DashboardKpiChartDataRedis> dashboardKpiTableDataRedis = dashboardKpiChartRedisdata.getData();
            for(DashboardKpiChartDataRedis dashboardKpiChartDataRedis :dashboardKpiTableDataRedis){
                dashboardKpiChartDataRedis.setLabel(String.valueOf(day));
            }
            BeanUtils.copyProperties(dashboardKpiChartRedisdata,dashboardKpiChartRedis2);
            dashboardKpiChartRedisColl.add(dashboardKpiChartRedis2);
        }

       return dashboardKpiChartRedisColl;
    }

    public List<DashboardKpiChartRedis> getCartsLastYear(String header, String branch_, String achievementType,LocalDate date1, LocalDate date2) {
        List<DashboardKpiChartRedis> dashboardKpiChartRedis = dashboardKpiChartRedisRepo.findAll().stream().filter(x -> x.getAchievementType().equals(achievementType) && x.getHeader().equals(header) && x.getParams().getBranch_().equals(branch_)).collect(Collectors.toList());
        List<DashboardKpiChartRedis> dashboardKpiChartRedis1 = dashboardKpiChartRedis.stream().filter(x -> x.getParams().getDate_().isAfter(date2) && x.getParams().getDate_().isBefore(date1)).collect(Collectors.toList());
        dashboardKpiChartRedis1.sort(Comparator.comparing(o -> o.getParams().getDate_()));



       */
/* Map<Object, List<DashboardKpiChartRedis>> map;
        map = dashboardKpiChartRedis1.stream().collect(Collectors.groupingBy(element
                -> element.getParams().getDate_().getMonth()));

        Map<Object, List<DashboardKpiChartDataRedis>> map1;
        for (Map.Entry<Object, List<DashboardKpiChartRedis>> entry : map.entrySet()) {
            //System.out.println("Key : " + entry.getKey() + " Value : " + entry.getValue());
            for(DashboardKpiChartRedis x : entry.getValue()) {
                System.out.println(x.getData());
                System.out.println("dddddddddddddddddd");
            }
        }*//*


       */
/* Map <Object, List<DashboardKpiChartRedis>> byGender = dashboardKpiChartRedis1.stream()
                .collect(Collectors.groupingBy(p -> p.getParams().getDate_()));
        System.out.println(byGender);
        System.out.println("vvvvvvvvvv");*//*

       */
/*gagal Map<String,List<DashboardKpiChartRedis>> personByCity
                = new HashMap<>();

        for(DashboardKpiChartRedis p : dashboardKpiChartRedis1){
            if(!personByCity.containsKey(p.getParams().getDate_())){
                personByCity.put(String.valueOf(p.getParams().getDate_()), new ArrayList<>());
            }
            personByCity.get(p.getParams().getDate_()).add(p);
        }

        System.out.println("Person grouped by cities : " + personByCity);*//*


            List<DashboardKpiChartRedis> dashboardKpiChartRedisColl = new LinkedList<>();
            List<DashboardKpiChartDataRedis> dashboardKpiChartDataRedisColl = new LinkedList<>();
        for(DashboardKpiChartRedis dashboardKpiChartRedisdata : dashboardKpiChartRedis1){
            DashboardKpiChartRedis dashboardKpiChartRedis2 = new DashboardKpiChartRedis();
            String month = dashboardKpiChartRedisdata.getParams().getDate_().getMonth().name();
            LinkedList<DashboardKpiChartDataRedis> dashboardKpiTableDataRedis = dashboardKpiChartRedisdata.getData();
            for(DashboardKpiChartDataRedis dashboardKpiChartDataRedis :dashboardKpiTableDataRedis){
                dashboardKpiChartDataRedis.setLabel(month);
                dashboardKpiChartDataRedisColl.add(dashboardKpiChartDataRedis);

            }
           */
/* BeanUtils.copyProperties(dashboardKpiChartRedisdata,dashboardKpiChartRedis2);
            dashboardKpiChartRedisColl.add(dashboardKpiChartRedis2);*//*

        }

        Map<Object, List<DashboardKpiChartDataRedis>> postsPerType = dashboardKpiChartDataRedisColl.stream()
                .collect(groupingBy(DashboardKpiChartDataRedis::getName));

        Map<String, Map<String, List<DashboardKpiChartDataRedis>>> map = dashboardKpiChartDataRedisColl.stream()
                .collect(groupingBy(DashboardKpiChartDataRedis::getName, groupingBy(DashboardKpiChartDataRedis::getLabel)));

        Map<String, Map<String, BigDecimal>> result = dashboardKpiChartDataRedisColl
                .stream()
                .collect(groupingBy(DashboardKpiChartDataRedis::getLabel, groupingBy(DashboardKpiChartDataRedis::getName, mapping(DashboardKpiChartDataRedis::getValue, reducing(BigDecimal.ZERO, BigDecimal::add)))));
        System.out.println(result);
        System.out.println("ssssssssss");

        */
/*List<DashboardKpiChartDataRedis> flattened = dashboardKpiChartDataRedisColl.stream()
                .collect(Collectors.groupingBy(
                        DashboardKpiChartDataRedis::getLabel,
                        Collectors.groupingBy(
                                DashboardKpiChartDataRedis::getName,
                                Collectors.reducing(
                                        BigDecimal.ZERO,
                                        DashboardKpiChartDataRedis::getValue,
                                        BigDecimal::add))))
                .entrySet()
                .stream()
                .flatMap(e1 -> e1.getValue()
                        .entrySet()
                        .stream()
                        .map(e2 -> new DashboardKpiChartDataRedis(e2.getValue(), e2.getKey(), e1.getKey())))
                .collect(Collectors.toList());*//*

        return dashboardKpiChartRedisColl;
    }

    */
/*public DashboardKpiChartRedis getCartsLastWeek(String header,String branch_, String achievementType) {
        System.out.println(achievementType +"achievementType");
        LocalDate date1 = LocalDate.now();
        LocalDate date2 = date1.minusWeeks(1);
        List<DashboardKpiTableRedis> dashboardKpiTableRedis = dashboardKpiTableRedisRepo.findAll().stream().filter(x -> x.getHeader().equals(header) && x.getParams().getBranch_().equals(branch_)).collect(Collectors.toList());
//        System.out.println(date1 +"aaaaaaaaaa"+ date2);
        List<DashboardKpiTableRedis> dashboardKpiTableRedis1= dashboardKpiTableRedis.stream().filter(x -> x.getParams().getDate_().isAfter(date2) && x.getParams().getDate_().isBefore(date1)).collect(Collectors.toList());
//        System.out.println(date1 +"bbbbbbbbbb"+ date2 +"ccccc"+ dashboardKpiTableRedis1.size());
        LinkedList<LinkedList<DashboardKpiTableDataRedis>> dashboardKpiTableDataRedisColl = new LinkedList<>();
        for(DashboardKpiTableRedis dashboardKpiTableRedisData :dashboardKpiTableRedis1) {
            LinkedList<DashboardKpiTableDataRedis> dashboardKpiTableDataRedis = dashboardKpiTableRedisData.getData();
            dashboardKpiTableDataRedisColl.add(dashboardKpiTableDataRedis);
//            dashboardKpiTableRedisData.getParams().getDate_().getDayOfWeek().name();
        }
        System.out.println(dashboardKpiTableDataRedisColl);
        DashboardKpiChartRedis dashboardKpiChartRedisdata = new DashboardKpiChartRedis();
        for(LinkedList<DashboardKpiTableDataRedis> dashboardKpiTableDataRedis:dashboardKpiTableDataRedisColl){

        }
        *//*
*/
/*DashboardKpiChartRedis dashboardKpiChartRedisdata = new DashboardKpiChartRedis();
        LinkedList<DashboardKpiChartDataRedis> dashboardKpiChartDataRedisColl = new LinkedList<>();
        for(DashboardKpiTableRedis dashboardKpiTableRedisData :dashboardKpiTableRedis1){
            System.out.println(dashboardKpiTableRedisData);
            System.out.println("wow");
            if(achievementType.equals("BOOKING")){
                System.out.println("i i BOOKING");
                dashboardKpiChartRedisdata.setAchievementType(achievementType);
                DashboardKpiChartDataRedis dashboardKpiChartDataRedis = new DashboardKpiChartDataRedis();

                LinkedList<DashboardKpiTableDataRedis> dashboardKpiTableDataRedisColl = dashboardKpiTableRedisData.getData();
                for(DashboardKpiTableDataRedis dashboardKpiTableDataRedis : dashboardKpiTableDataRedisColl){
                    dashboardKpiChartDataRedis.setName(dashboardKpiTableDataRedis.getName());
                    dashboardKpiChartDataRedis.setValue(dashboardKpiTableDataRedis.getBkdWeightPercent());
                    dashboardKpiChartDataRedis.setLabel(dashboardKpiTableRedisData.getParams().getDate_().getDayOfWeek().name());
                    dashboardKpiChartDataRedisColl.add(dashboardKpiChartDataRedis);
                }
                dashboardKpiChartRedisdata.setData(dashboardKpiChartDataRedisColl);

            }else if(achievementType == "RA"){

            }else if(achievementType == "WHO"){

            }else if(achievementType == "WHI"){

            }else if(achievementType == "WHT"){

            }
        }*//*
*/
/*

        return dashboardKpiChartRedisdata;
    }

*//*


    public void dummyDashboardKpiChartAgentBookingAndAirlinesRedis(String dt1,String dt2) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate1 = LocalDate.parse(dt1, formatter);
        LocalDate localDate2 = LocalDate.parse(dt2, formatter);
        LocalDate date1 = LocalDate.of(localDate1.getYear(),localDate1.getMonth(),localDate1.getDayOfMonth());
        LocalDate date2 = LocalDate.of(localDate2.getYear(),localDate2.getMonth(),localDate2.getDayOfMonth());


        Collection<DashboardKpiChartRedis> DashboardKpiChartRedisAgent = new LinkedList<>();
        Collection<DashboardKpiChartRedis> DashboardKpiChartRedisAirlines = new LinkedList<>();
        int i=1;
        int j=1000;
        for (LocalDate date = date1; date.isBefore(date2); date = date.plusDays(1)) {
            DashboardKpiChartRedis dashboardKpiChartRedisAgent = new DashboardKpiChartRedis();
            DashboardKpiChartRedis dashboardKpiChartRedisAirlines = new DashboardKpiChartRedis();
            DashboardKpiParams dashboardKpiParams = new DashboardKpiParams();
            dashboardKpiParams.setBranch_("ALL");
            dashboardKpiParams.setDate_(date);
            dashboardKpiParams.setMonth_(date.getMonthValue());
            dashboardKpiParams.setYear_(date.getYear());

            //agent
                //BOOKING
            dashboardKpiChartRedisAgent.setId(Long.valueOf(i));
            dashboardKpiChartRedisAgent.setParams(dashboardKpiParams);
            dashboardKpiChartRedisAgent.setAchievementType("BOOKING");
            dashboardKpiChartRedisAgent.setHeader("Agent");
            LinkedList<DashboardKpiChartDataRedis> dashboardKpiChartDataRedisAgent = setKpiChartDataAgentRedis(date.getDayOfWeek().name());
            dashboardKpiChartRedisAgent.setData(dashboardKpiChartDataRedisAgent);
            DashboardKpiChartRedisAgent.add(dashboardKpiChartRedisAgent);

            //airline
            //BOOKING
            dashboardKpiChartRedisAirlines.setId(Long.valueOf(j));
            dashboardKpiChartRedisAirlines.setParams(dashboardKpiParams);
            dashboardKpiChartRedisAirlines.setAchievementType("BOOKING");
            dashboardKpiChartRedisAirlines.setHeader("Airlines");
            LinkedList<DashboardKpiChartDataRedis> dashboardKpiChartDataRedisAirlines = setKpiChartDataAirlinesRedis(date.getDayOfWeek().name());
            dashboardKpiChartRedisAirlines.setData(dashboardKpiChartDataRedisAirlines);
            DashboardKpiChartRedisAirlines.add(dashboardKpiChartRedisAirlines);
            i++;
            j++;
        }

        dashboardKpiChartRedisRepo.saveAll(DashboardKpiChartRedisAgent);
        dashboardKpiChartRedisRepo.saveAll(DashboardKpiChartRedisAirlines);

    }

    public void dummyDashboardKpiChartAgentRAAndAirlinesRedis(String dt1,String dt2) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate1 = LocalDate.parse(dt1, formatter);
        LocalDate localDate2 = LocalDate.parse(dt2, formatter);
        LocalDate date1 = LocalDate.of(localDate1.getYear(),localDate1.getMonth(),localDate1.getDayOfMonth());
        LocalDate date2 = LocalDate.of(localDate2.getYear(),localDate2.getMonth(),localDate2.getDayOfMonth());

        Collection<DashboardKpiChartRedis> DashboardKpiChartRedisAgent = new LinkedList<>();
        Collection<DashboardKpiChartRedis> DashboardKpiChartRedisAirlines = new LinkedList<>();
        int i=2000;
        int j=3000;
        for (LocalDate date = date1; date.isBefore(date2); date = date.plusDays(1)) {
            DashboardKpiChartRedis dashboardKpiChartRedisAgent = new DashboardKpiChartRedis();
            DashboardKpiChartRedis dashboardKpiChartRedisAirlines = new DashboardKpiChartRedis();
            DashboardKpiParams dashboardKpiParams = new DashboardKpiParams();
            dashboardKpiParams.setBranch_("ALL");
            dashboardKpiParams.setDate_(date);
            dashboardKpiParams.setMonth_(date.getMonthValue());
            dashboardKpiParams.setYear_(date.getYear());

            //agent
            //BOOKING
            dashboardKpiChartRedisAgent.setId(Long.valueOf(i));
            dashboardKpiChartRedisAgent.setParams(dashboardKpiParams);
            dashboardKpiChartRedisAgent.setAchievementType("RA");
            dashboardKpiChartRedisAgent.setHeader("Agent");
            LinkedList<DashboardKpiChartDataRedis> dashboardKpiChartDataRedisAgent = setKpiChartDataAgentRedis(date.getDayOfWeek().name());
            dashboardKpiChartRedisAgent.setData(dashboardKpiChartDataRedisAgent);
            DashboardKpiChartRedisAgent.add(dashboardKpiChartRedisAgent);

            //airline
            //BOOKING
            dashboardKpiChartRedisAirlines.setId(Long.valueOf(j));
            dashboardKpiChartRedisAirlines.setParams(dashboardKpiParams);
            dashboardKpiChartRedisAirlines.setAchievementType("RA");
            dashboardKpiChartRedisAirlines.setHeader("Airlines");
            LinkedList<DashboardKpiChartDataRedis> dashboardKpiChartDataRedisAirlines = setKpiChartDataAirlinesRedis(date.getDayOfWeek().name());
            dashboardKpiChartRedisAirlines.setData(dashboardKpiChartDataRedisAirlines);
            DashboardKpiChartRedisAirlines.add(dashboardKpiChartRedisAirlines);
            i++;
            j++;
        }

        dashboardKpiChartRedisRepo.saveAll(DashboardKpiChartRedisAgent);
        dashboardKpiChartRedisRepo.saveAll(DashboardKpiChartRedisAirlines);

    }

    public void dummyDashboardKpiChartAgentWHOAndAirlinesRedis(String dt1,String dt2) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate1 = LocalDate.parse(dt1, formatter);
        LocalDate localDate2 = LocalDate.parse(dt2, formatter);
        LocalDate date1 = LocalDate.of(localDate1.getYear(),localDate1.getMonth(),localDate1.getDayOfMonth());
        LocalDate date2 = LocalDate.of(localDate2.getYear(),localDate2.getMonth(),localDate2.getDayOfMonth());

        Collection<DashboardKpiChartRedis> DashboardKpiChartRedisAgent = new LinkedList<>();
        Collection<DashboardKpiChartRedis> DashboardKpiChartRedisAirlines = new LinkedList<>();
        int i=4000;
        int j=5000;
        for (LocalDate date = date1; date.isBefore(date2); date = date.plusDays(1)) {
            DashboardKpiChartRedis dashboardKpiChartRedisAgent = new DashboardKpiChartRedis();
            DashboardKpiChartRedis dashboardKpiChartRedisAirlines = new DashboardKpiChartRedis();
            DashboardKpiParams dashboardKpiParams = new DashboardKpiParams();
            dashboardKpiParams.setBranch_("ALL");
            dashboardKpiParams.setDate_(date);
            dashboardKpiParams.setMonth_(date.getMonthValue());
            dashboardKpiParams.setYear_(date.getYear());

            //agent
            //BOOKING
            dashboardKpiChartRedisAgent.setId(Long.valueOf(i));
            dashboardKpiChartRedisAgent.setParams(dashboardKpiParams);
            dashboardKpiChartRedisAgent.setAchievementType("WHO");
            dashboardKpiChartRedisAgent.setHeader("Agent");
            LinkedList<DashboardKpiChartDataRedis> dashboardKpiChartDataRedisAgent = setKpiChartDataAgentRedis(date.getDayOfWeek().name());
            dashboardKpiChartRedisAgent.setData(dashboardKpiChartDataRedisAgent);
            DashboardKpiChartRedisAgent.add(dashboardKpiChartRedisAgent);

            //airline
            //BOOKING
            dashboardKpiChartRedisAirlines.setId(Long.valueOf(j));
            dashboardKpiChartRedisAirlines.setParams(dashboardKpiParams);
            dashboardKpiChartRedisAirlines.setAchievementType("WHO");
            dashboardKpiChartRedisAirlines.setHeader("Airlines");
            LinkedList<DashboardKpiChartDataRedis> dashboardKpiChartDataRedisAirlines = setKpiChartDataAirlinesRedis(date.getDayOfWeek().name());
            dashboardKpiChartRedisAirlines.setData(dashboardKpiChartDataRedisAirlines);
            DashboardKpiChartRedisAirlines.add(dashboardKpiChartRedisAirlines);
            i++;
            j++;
        }

        dashboardKpiChartRedisRepo.saveAll(DashboardKpiChartRedisAgent);
        dashboardKpiChartRedisRepo.saveAll(DashboardKpiChartRedisAirlines);

    }

    public void dummyDashboardKpiChartAgentWHIAndAirlinesRedis(String dt1,String dt2) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate1 = LocalDate.parse(dt1, formatter);
        LocalDate localDate2 = LocalDate.parse(dt2, formatter);
        LocalDate date1 = LocalDate.of(localDate1.getYear(),localDate1.getMonth(),localDate1.getDayOfMonth());
        LocalDate date2 = LocalDate.of(localDate2.getYear(),localDate2.getMonth(),localDate2.getDayOfMonth());

        Collection<DashboardKpiChartRedis> DashboardKpiChartRedisAgent = new LinkedList<>();
        Collection<DashboardKpiChartRedis> DashboardKpiChartRedisAirlines = new LinkedList<>();
        int i=6000;
        int j=7000;
        for (LocalDate date = date1; date.isBefore(date2); date = date.plusDays(1)) {
            DashboardKpiChartRedis dashboardKpiChartRedisAgent = new DashboardKpiChartRedis();
            DashboardKpiChartRedis dashboardKpiChartRedisAirlines = new DashboardKpiChartRedis();
            DashboardKpiParams dashboardKpiParams = new DashboardKpiParams();
            dashboardKpiParams.setBranch_("ALL");
            dashboardKpiParams.setDate_(date);
            dashboardKpiParams.setMonth_(date.getMonthValue());
            dashboardKpiParams.setYear_(date.getYear());

            //agent
            //BOOKING
            dashboardKpiChartRedisAgent.setId(Long.valueOf(i));
            dashboardKpiChartRedisAgent.setParams(dashboardKpiParams);
            dashboardKpiChartRedisAgent.setAchievementType("WHI");
            dashboardKpiChartRedisAgent.setHeader("Agent");
            LinkedList<DashboardKpiChartDataRedis> dashboardKpiChartDataRedisAgent = setKpiChartDataAgentRedis(date.getDayOfWeek().name());
            dashboardKpiChartRedisAgent.setData(dashboardKpiChartDataRedisAgent);
            DashboardKpiChartRedisAgent.add(dashboardKpiChartRedisAgent);

            //airline
            //BOOKING
            dashboardKpiChartRedisAirlines.setId(Long.valueOf(j));
            dashboardKpiChartRedisAirlines.setParams(dashboardKpiParams);
            dashboardKpiChartRedisAirlines.setAchievementType("WHI");
            dashboardKpiChartRedisAirlines.setHeader("Airlines");
            LinkedList<DashboardKpiChartDataRedis> dashboardKpiChartDataRedisAirlines = setKpiChartDataAirlinesRedis(date.getDayOfWeek().name());
            dashboardKpiChartRedisAirlines.setData(dashboardKpiChartDataRedisAirlines);
            DashboardKpiChartRedisAirlines.add(dashboardKpiChartRedisAirlines);
            i++;
            j++;
        }

        dashboardKpiChartRedisRepo.saveAll(DashboardKpiChartRedisAgent);
        dashboardKpiChartRedisRepo.saveAll(DashboardKpiChartRedisAirlines);

    }


    public void dummyDashboardKpiChartAgentWHTAndAirlinesRedis(String dt1,String dt2) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate1 = LocalDate.parse(dt1, formatter);
        LocalDate localDate2 = LocalDate.parse(dt2, formatter);
        LocalDate date1 = LocalDate.of(localDate1.getYear(),localDate1.getMonth(),localDate1.getDayOfMonth());
        LocalDate date2 = LocalDate.of(localDate2.getYear(),localDate2.getMonth(),localDate2.getDayOfMonth());

        Collection<DashboardKpiChartRedis> DashboardKpiChartRedisAgent = new LinkedList<>();
        Collection<DashboardKpiChartRedis> DashboardKpiChartRedisAirlines = new LinkedList<>();
        int i=8000;
        int j=9000;
        for (LocalDate date = date1; date.isBefore(date2); date = date.plusDays(1)) {
            DashboardKpiChartRedis dashboardKpiChartRedisAgent = new DashboardKpiChartRedis();
            DashboardKpiChartRedis dashboardKpiChartRedisAirlines = new DashboardKpiChartRedis();
            DashboardKpiParams dashboardKpiParams = new DashboardKpiParams();
            dashboardKpiParams.setBranch_("ALL");
            dashboardKpiParams.setDate_(date);
            dashboardKpiParams.setMonth_(date.getMonthValue());
            dashboardKpiParams.setYear_(date.getYear());

            //agent
            //BOOKING
            dashboardKpiChartRedisAgent.setId(Long.valueOf(i));
            dashboardKpiChartRedisAgent.setParams(dashboardKpiParams);
            dashboardKpiChartRedisAgent.setAchievementType("WHT");
            dashboardKpiChartRedisAgent.setHeader("Agent");
            LinkedList<DashboardKpiChartDataRedis> dashboardKpiChartDataRedisAgent = setKpiChartDataAgentRedis(date.getDayOfWeek().name());
            dashboardKpiChartRedisAgent.setData(dashboardKpiChartDataRedisAgent);
            DashboardKpiChartRedisAgent.add(dashboardKpiChartRedisAgent);

            //airline
            //BOOKING
            dashboardKpiChartRedisAirlines.setId(Long.valueOf(j));
            dashboardKpiChartRedisAirlines.setParams(dashboardKpiParams);
            dashboardKpiChartRedisAirlines.setAchievementType("WHT");
            dashboardKpiChartRedisAirlines.setHeader("Airlines");
            LinkedList<DashboardKpiChartDataRedis> dashboardKpiChartDataRedisAirlines = setKpiChartDataAirlinesRedis(date.getDayOfWeek().name());
            dashboardKpiChartRedisAirlines.setData(dashboardKpiChartDataRedisAirlines);
            DashboardKpiChartRedisAirlines.add(dashboardKpiChartRedisAirlines);
            i++;
            j++;
        }

        dashboardKpiChartRedisRepo.saveAll(DashboardKpiChartRedisAgent);
        dashboardKpiChartRedisRepo.saveAll(DashboardKpiChartRedisAirlines);

    }


    public LinkedList<DashboardKpiChartDataRedis> setKpiChartDataAgentRedis(String name) {
        LinkedList<DashboardKpiChartDataRedis> dashboardKpiChartDataRedisList = new LinkedList<>();

        int min1 = 0;
        int max1 = 100;

        //JNE
        DashboardKpiChartDataRedis dashboardKpiChartDataRedis = new DashboardKpiChartDataRedis();
        dashboardKpiChartDataRedis.setName("JNE");
        dashboardKpiChartDataRedis.setLabel(name);
        dashboardKpiChartDataRedis.setValue(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiChartDataRedisList.add(dashboardKpiChartDataRedis);


        //TIKI
        DashboardKpiChartDataRedis dashboardKpiChartDataRedis1 = new DashboardKpiChartDataRedis();
        dashboardKpiChartDataRedis1.setName("TIKI");
        dashboardKpiChartDataRedis1.setLabel(name);
        dashboardKpiChartDataRedis1.setValue(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiChartDataRedisList.add(dashboardKpiChartDataRedis1);
        //JNT
        DashboardKpiChartDataRedis dashboardKpiChartDataRedis2 = new DashboardKpiChartDataRedis();
        dashboardKpiChartDataRedis2.setName("JNT");
        dashboardKpiChartDataRedis2.setLabel(name);
        dashboardKpiChartDataRedis2.setValue(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiChartDataRedisList.add(dashboardKpiChartDataRedis2);
        //SAP
        DashboardKpiChartDataRedis dashboardKpiChartDataRedis3 = new DashboardKpiChartDataRedis();
        dashboardKpiChartDataRedis3.setName("SAP");
        dashboardKpiChartDataRedis3.setLabel(name);
        dashboardKpiChartDataRedis3.setValue(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiChartDataRedisList.add(dashboardKpiChartDataRedis3);

        return dashboardKpiChartDataRedisList;
    }

    public LinkedList<DashboardKpiChartDataRedis> setKpiChartDataAirlinesRedis(String name) {
        LinkedList<DashboardKpiChartDataRedis> dashboardKpiChartDataRedisList = new LinkedList<>();

        int min1 = 0;
        int max1 = 100;

        //Trigana
        DashboardKpiChartDataRedis dashboardKpiChartDataRedis = new DashboardKpiChartDataRedis();
        dashboardKpiChartDataRedis.setName("Trigana");
        dashboardKpiChartDataRedis.setLabel(name);
        dashboardKpiChartDataRedis.setValue(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiChartDataRedisList.add(dashboardKpiChartDataRedis);


        //Garuda Indonesia
        DashboardKpiChartDataRedis dashboardKpiChartDataRedis1 = new DashboardKpiChartDataRedis();
        dashboardKpiChartDataRedis1.setName("Garuda Indonesia");
        dashboardKpiChartDataRedis1.setLabel(name);
        dashboardKpiChartDataRedis1.setValue(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiChartDataRedisList.add(dashboardKpiChartDataRedis1);
        //Citylink
        DashboardKpiChartDataRedis dashboardKpiChartDataRedis2 = new DashboardKpiChartDataRedis();
        dashboardKpiChartDataRedis2.setName("Citylink");
        dashboardKpiChartDataRedis2.setLabel(name);
        dashboardKpiChartDataRedis2.setValue(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiChartDataRedisList.add(dashboardKpiChartDataRedis2);
        //Sriwijaya
        DashboardKpiChartDataRedis dashboardKpiChartDataRedis3 = new DashboardKpiChartDataRedis();
        dashboardKpiChartDataRedis3.setName("Sriwijaya");
        dashboardKpiChartDataRedis3.setLabel(name);
        dashboardKpiChartDataRedis3.setValue(BigDecimal.valueOf((int)(Math.random() * (max1 - min1 + 1) + min1)));
        dashboardKpiChartDataRedisList.add(dashboardKpiChartDataRedis3);

        return dashboardKpiChartDataRedisList;
    }

}*/
