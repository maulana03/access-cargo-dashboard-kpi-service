package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@RedisHash("DashboardPreManifestReportDetailRedis")
public class DashboardPreManifestReportDataDetailRedis implements Serializable {
    private static final long serialVersionUID = 4187078079971259399L;
    @Id
    Long id;
    String noPreman;
    String flight;
    String route;
    String type;
    Integer totalAwb;
    Integer totalPieces;
    BigDecimal totalKg;
    LocalDateTime releasedTime;
    String branch;

}

