package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedList;

@Data
@NoArgsConstructor
public class DashboardKpiTableDataRedis implements Serializable {
    private static final long serialVersionUID = 4732818180321503984L;

    String name;

    BigDecimal bkdWeightAmount;
    BigDecimal bkdWeightPercent;

    BigDecimal raWeightAmount;
    BigDecimal raWeightPercent;

    BigDecimal whoWeightAmount;
    BigDecimal whoWeightPercent;

    BigDecimal whtWeightAmount;
    BigDecimal whtWeightPercent;

    BigDecimal whiWeightAmount;
    BigDecimal whiWeightPercent;

}
