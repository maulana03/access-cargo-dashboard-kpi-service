package com.access.cargo.dashboard.kpi.model.wrapper;//package com.access.cargo.account.model.wrapper;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.List;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Grafik {
    Collection cols;
//    Collection rows;
    List<C> rows;
}
