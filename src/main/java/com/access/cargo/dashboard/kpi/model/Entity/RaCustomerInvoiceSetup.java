package com.access.cargo.dashboard.kpi.model.Entity;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.id.enhanced.SequenceStyleGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@DynamicUpdate
@Table(name = "ra_customer_invoice_setup", schema = "public")
public class RaCustomerInvoiceSetup implements Serializable {
    private static final String ID = "ID";
    private static final String CUSTOMER_ID = "CUSTOMER_ID";
    private static final String CUSTOMER_NAME = "CUSTOMER_NAME";
    private static final String PPN_STATUS = "PPN_STATUS";
    private static final String PPN_AMOUNT = "PPN_AMOUNT";
    private static final String PPH_STATUS = "PPH_STATUS";
    private static final String PPH_AMOUNT = "PPH_AMOUNT";
    private static final String DISCOUNT_STATUS = "DISCOUNT_STATUS";
    private static final String DISCOUNT_AMOUNT = "DISCOUNT_AMOUNT";
    private static final String PAYMENT_TYPES = "PAYMENT_TYPES";

    /**/
    private static final String STATUS = "STATUS";
    private static final String STATUS_DESC = "STATUS_DESC";
    private static final String CREATED_DATETIME = "CREATED_DATETIME";
    private static final String CREATED_BY = "CREATED_BY";
    private static final String MODIFIED_DATETIME = "MODIFIED_DATETIME";
    private static final String MODIFIED_BY = "MODIFIED_BY";
    private static final long serialVersionUID = 1995894259792950454L;

    /**/

    @Id
    @GeneratedValue(generator = "ra_customer_invoice_setup_seq-generator")
    @GenericGenerator(
            name = "ra_customer_invoice_setup_seq-generator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.SEQUENCE_PARAM, value = "ra_customer_invoice_setup_seq"),
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.INITIAL_PARAM, value = "1"),
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.INCREMENT_PARAM, value = "1")
            }
    )
    @Column(name = ID)
    private Long id;

    @Column(name = CUSTOMER_ID)
    private Long customerId;

    @Column(name = CUSTOMER_NAME, length = 50)
    private String customerName;

    @Column(name = PPN_STATUS)
    private Integer ppnStatus;

    @Column(name = PPN_AMOUNT, precision = 19, scale = 3)
    private BigDecimal ppnAmount;

    @Column(name = PPH_STATUS)
    private Integer pphStatus;

    @Column(name = PPH_AMOUNT, precision = 19, scale = 3)
    private BigDecimal pphAmount;

    @Column(name = DISCOUNT_STATUS)
    private Integer discountStatus;

    @Column(name = DISCOUNT_AMOUNT, precision = 19, scale = 3)
    private BigDecimal discountAmount;

    @Column(name = PAYMENT_TYPES, length = 20)
    private String paymentTypes;


    /**/

    @Column(name = STATUS)
    private Integer status;

    @Column(name = STATUS_DESC, length = 30)
    private String statusDesc;

    @Column(name = CREATED_DATETIME)
    private LocalDateTime createdDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = CREATED_BY)
    private AccountUser createdBy;

    @Column(name = MODIFIED_DATETIME)
    private LocalDateTime modifiedDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = MODIFIED_BY)
    private AccountUser modifiedBy;

}
