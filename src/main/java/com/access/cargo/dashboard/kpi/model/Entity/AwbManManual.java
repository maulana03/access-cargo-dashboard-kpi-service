package com.access.cargo.dashboard.kpi.model.Entity;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.id.enhanced.SequenceStyleGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "awb_man_manual", schema = "public")
@DynamicUpdate
public class AwbManManual implements Serializable {
    private static final String ID = "ID";
    private static final String CODE = "code";
    private static final String FLIGHT_NO = "flight_no";
    private static final String AIRLINE = "airline";
    private static final String DEPARTURE_DATE = "departure_date";
    private static final String DEPARATUR_TIME = "departure_time";
    private static final String ARRIVAL_DATE = "arrival_date";
    private static final String ARRIVAL_TIME = "arrival_time";
    private static final String ORIGIN = "origin";
    private static final String DESTINATION = "destination";
    private static final String SECTOR = "sector";
    private static final String AGENT_ID = "agent_id";
    private static final String AGENT_NAME = "agent_name";
    private static final String SHIPPER_NAME = "shipper_name";
    private static final String SHIPPER_CITY = "shipper_city";
    private static final String CONSIGNEE_NAME = "consignee_name";
    private static final String CONSIGNEE_CITY = "consignee_city";
    private static final String PIECES = "pieces";
    private static final String PIECES_TOTAL = "pieces_total";
    private static final String GROSS_WEIGHT = "gross_weight";
    private static final String VOLUME_CBM = "volume_cbm";
    private static final String CHARGEABLE_WEIGHT = "chargeable_weight";
    private static final String COMMODITY_CODE = "commodity_code";
    private static final String PRODUCT_CODE = "product_code";
    private static final String SHC = "shc";
    private static final String NOG = "nog";



    /**/
    private static final String STATUS = "STATUS";
    private static final String STATUS_DESC = "STATUS_DESC";
    private static final String CREATED_DATETIME = "CREATED_DATETIME";
    private static final String CREATED_BY = "CREATED_BY";
    private static final String MODIFIED_DATETIME = "MODIFIED_DATETIME";
    private static final String MODIFIED_BY = "MODIFIED_BY";

    private static final long serialVersionUID = -7754737634308667607L;

    @Id
    @GeneratedValue(generator = "awb_man_manual_seq-generator")
    @GenericGenerator(
            name = "awb_man_manual_seq-generator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.SEQUENCE_PARAM, value = "awb_man_manual_seq"),
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.INITIAL_PARAM, value = "1"),
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.INCREMENT_PARAM, value = "1")
            }
    )
    @Column(name = ID)
    private Long id;

    @Column(name = CODE, length = 15)
    private String code;

    @Column(name = FLIGHT_NO, length = 10)
    private String flightNo;

    @Column(name = AIRLINE, length = 5)
    private String airline;

    @Column(name = DEPARTURE_DATE)
    private LocalDate departureDate;

    @Column(name = DEPARATUR_TIME, length = 6)
    private String departureTime;

    @Column(name = ARRIVAL_DATE)
    private LocalDate arrivalDate;

    @Column(name = ARRIVAL_TIME, length = 6)
    private String arrivalTime;

    @Column(name = ORIGIN, length = 3)
    private String origin;

    @Column(name = DESTINATION, length = 3)
    private String destination;

    @Column(name = SECTOR, length = 15)
    private String sector;

    @Column(name = AGENT_ID)
    private Long agentId;

    @Column(name = AGENT_NAME, length = 125)
    private String agentName;

    @Column(name = SHIPPER_NAME, length = 50)
    private String shipperName;

    @Column(name = SHIPPER_CITY, length = 50)
    private String shipperCity;

    @Column(name = CONSIGNEE_NAME, length = 50)
    private String consigneeName;

    @Column(name = CONSIGNEE_CITY, length = 50)
    private String consigneerCity;

    @Column(name = PIECES)
    private Integer pieces;

    @Column(name = PIECES_TOTAL)
    private Integer piecesTotal;

    @Column(name = GROSS_WEIGHT, precision = 19, scale = 3)
    private BigDecimal grossWeight;

    @Column(name = VOLUME_CBM, precision = 19, scale = 3)
    private BigDecimal volumeCbm;

    @Column(name = CHARGEABLE_WEIGHT, precision = 19, scale = 3)
    private BigDecimal chargeableWeight;

    @Column(name = COMMODITY_CODE, length = 7)
    private String commodityCode;

    @Column(name = PRODUCT_CODE, length = 5)
    private String productCode;

    @Column(name = SHC, length = 5)
    private String shc;

    @Column(name = NOG, length = 50)
    private String nog;


    /**/

    @Column(name = STATUS)
    private Integer status;

    @Column(name = STATUS_DESC, length = 30)
    private String statusDesc;

    @Column(name = "created_datetime")
    private LocalDateTime createdDateTime;

    @Column(name = "modified_datetime")
    private LocalDateTime modifiedDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by")
    private AccountUser createdBy;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "modified_by")
    private AccountUser modifiedBy;

}
