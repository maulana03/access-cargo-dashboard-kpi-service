package com.access.cargo.dashboard.kpi.model.Entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@NoArgsConstructor
@Data
@Entity
@Table(name = "awb_koli")
public class AwbKoli implements Serializable {

    private static final long serialVersionUID = -805351090352429123L;

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "code")
    private String code;

    @ManyToOne
    @JoinColumn(name = "awb")
    private Awb awb;

    public AwbKoli(Long id) {
        this.id = id;
    }
}
