package com.access.cargo.dashboard.kpi.model.Entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "awb")
public class Awb implements Serializable {

    private static final long serialVersionUID = -5114450628956017564L;

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "code", length = 15)
    private String code;

    @Column(name = "gross_weight", precision = 19, scale = 3)
    private BigDecimal grossWeight;

    @Column(name = "volume_cbm", precision = 19, scale = 3)
    private BigDecimal volumeCbm;

    @Column(name = "LENGTH", precision = 19, scale = 3)
    private BigDecimal length;

    @Column(name = "WIDTH", precision = 19, scale = 3)
    private BigDecimal width;

    @Column(name = "HEIGHT", precision = 19, scale = 3)
    private BigDecimal height;

    @Column(name = "volume_weight", precision = 19, scale = 3)
    private BigDecimal volumeWeight;

    @Column(name = "chargeable_weight", precision = 19, scale = 3)
    private BigDecimal chargeableWeight;

    @Column(name = "ra_pieces")
    private Integer raPieces;

    @Column(name = "pieces")
    private Integer pieces;

    @Column(name = "pieces_prev")
    private Integer piecesPrev;

    @Column(name = "pieces_rcs")
    private Integer piecesRcs;

    @Column(name = "origin", length = 3)
    private String origin;

    @Column(name = "destination", length = 3)
    private String destination;

    /*new*/
    @Column(name = "sector", length = 15)
    private String sector;

    @Column(name = "agent_id")
    private Long agentId;

    @Column(name = "agent_name", length = 125)
    private String agentName;

    @Column(name = "shipper_name", length = 50)
    private String shipperName;

    @Column(name = "consignee_name", length = 50)
    private String consigneeName;

    @Column(name = "airline", length = 5)
    private String airline;

    @Column(name = "flight_no", length = 10)
    private String flightNo;

    @Column(name = "flight_date")
    private LocalDate flightDate;

    /*new*/
    @Column(name = "issue_date")
    private LocalDate issueDate;

    @Column(name = "state", length = 5)
    private String state;

    @Column(name = "state1", length = 25)
    private String state1;

    /*new*/
    @Column(name = "state_msg_type_b")
    private Integer stateMsgTypeB = 0;

    /*new*/
    @Column(name = "state_incoming")
    private Integer stateIncoming = 0;

    /*new*/
    @Column(name = "state_outgoing")
    private Integer stateOutgoing = 0;

    /*new*/
    @Column(name = "state_transit")
    private Integer stateTransit = 0;

    /*new*/
    @Column(name = "status_manual")
    private Integer statusManual = 0;

    /*new*/
    @Column(name = "status_buildup_plan")
    private Integer statusBuildupPlan = 0;



    /*-----------------Rates-------------------------*/


    @Column(name = "rates", precision = 19, scale = 3)
    private BigDecimal rates;

    @Column(name = "rates_total", precision = 19, scale = 3)
    private BigDecimal ratesTotal;

    @Column(name = "COMMODITY_CODE", length = 7)
    private String commodityCode;

    @Column(name = "PRODUCT_CODE", length = 5)
    private String productCode;

    @Column(name = "SHC", length = 5)
    private String shc;

    @Column(name = "nog", length = 50)
    private String nog;

    @Column(name = "nog_consol", length = 50)
    private String nogConsol;

    @Column(name = "sph", length = 10)
    private String sph;

    @Column(name = "state_sph")
    private Integer stateSph = 0;

    @Column(name = "taxes", precision = 19, scale = 3)
    private BigDecimal taxes;

    @Column(name = "charge_summary_total", precision = 19, scale = 3)
    private BigDecimal chargeSummaryTotal;


    /*------------------------------------------*/

    @Column(name = "SRC_RSV", length = 30)
    private String srcRsv = ""; /*RA_AWB_REGISTER, RESERVATION*/


    /*------------------------------------------*/

    @Column(name = "created_datetime")
    private LocalDateTime createdDateTime;

    @Column(name = "modified_datetime")
    private LocalDateTime modifiedDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by")
    private AccountUser createdBy;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "modified_by")
    private AccountUser modifiedBy;


}