package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.redis.DashboardSalesReportDataDetailRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardTopUpReportDataDetailRedis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DashboardSalesReportDataDetailRedisRepo extends JpaRepository<DashboardSalesReportDataDetailRedis, String> {
}
