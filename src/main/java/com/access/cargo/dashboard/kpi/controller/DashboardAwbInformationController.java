package com.access.cargo.dashboard.kpi.controller;


import com.access.cargo.dashboard.kpi.config.JwtConstants;
import com.access.cargo.dashboard.kpi.service.AccountService;
import com.access.cargo.dashboard.kpi.service.DashboardAwbInformationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collection;
import java.util.LinkedList;

@CrossOrigin
@Slf4j
//@RestController
@Validated
public class DashboardAwbInformationController {
    static final String AWB_INFORMATION = "awb-information/dashboardKpi";
    static final String AWB_INFORMATION_GET_NO_AWB = "awb-information/noawb";

    @Autowired
    DashboardAwbInformationService dashboardAwbInformationService;

    @Autowired
    AccountService accountService;

    @GetMapping(AWB_INFORMATION_GET_NO_AWB)
    public ResponseEntity getNoAwb(
            @RequestHeader(value = JwtConstants.TOKEN_HEADER) String token) {

        Collection<String> authRoles = new LinkedList<>();
        authRoles.add("ROLE_SUPER_ADMIN");
        authRoles.add("ROLE_ADMIN");
        authRoles.add("ROLE_REPORT");
        authRoles.add("ROLE_REPORT_TONASE");

        if (!accountService.isAuthorized(token, authRoles)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(dashboardAwbInformationService.getNoawb(), HttpStatus.OK);

    }

    @GetMapping(AWB_INFORMATION)
    public ResponseEntity getInformationAwb(
            @RequestHeader(value = JwtConstants.TOKEN_HEADER) String token,
            @RequestParam(value = "noawb", required = false) String noawb) {

        Collection<String> authRoles = new LinkedList<>();
        authRoles.add("ROLE_SUPER_ADMIN");
        authRoles.add("ROLE_REPORT");
        authRoles.add("ROLE_ADMIN");
        authRoles.add("ROLE_REPORT_TONASE");

        if (!accountService.isAuthorized(token, authRoles)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(dashboardAwbInformationService.getDetail(noawb), HttpStatus.OK);

    }


}
