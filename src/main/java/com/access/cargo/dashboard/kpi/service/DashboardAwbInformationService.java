package com.access.cargo.dashboard.kpi.service;


import com.access.cargo.dashboard.kpi.model.redis.DashboardAwbInformationRedis;
import com.access.cargo.dashboard.kpi.repo.DashboardAwbInformationRedisRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Slf4j
@Service
public class DashboardAwbInformationService {
    @Autowired
    DashboardAwbInformationRedisRepo dashboardAwbInformationRedisRepo;


    public DashboardAwbInformationRedis getDetail(String noawb){
        DashboardAwbInformationRedis dashboardAwbInformationRedis;

        dashboardAwbInformationRedis = dashboardAwbInformationRedisRepo.findAll().stream().filter(x -> x.getNoAWB().equals(noawb)).findFirst().orElse(null);

        return dashboardAwbInformationRedis;
    }

    public List<String> getNoawb(){
        List<DashboardAwbInformationRedis> dashboardAwbInformationRedis;

        dashboardAwbInformationRedis = dashboardAwbInformationRedisRepo.findAll();

        LinkedList<String> data = new LinkedList<>();
        for(DashboardAwbInformationRedis dashboardAwbInformationRedis1 : dashboardAwbInformationRedis){

            data.add(dashboardAwbInformationRedis1.getNoAWB());
        }

        return data;
    }

}