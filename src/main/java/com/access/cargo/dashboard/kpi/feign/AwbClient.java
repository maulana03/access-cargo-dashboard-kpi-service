package com.access.cargo.dashboard.kpi.feign;

import com.access.cargo.dashboard.kpi.config.JwtConstants;
import com.access.cargo.dashboard.kpi.model.wrapper.AwbRedis;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "access-cargo-awb-service")
@RibbonClient(name = "access-cargo-awb-service")
public interface AwbClient {

    String PATH_ = "/access-cargo-awb-service";
    String AWBS_V2_BY_CODE = PATH_ + "/awbs/v2/code/{awbCode}";

    @GetMapping(AWBS_V2_BY_CODE)
    public ResponseEntity<AwbRedis> getAwbV2ByAwbCode(
            @RequestHeader(value = JwtConstants.TOKEN_HEADER) String token
            , @PathVariable("awbCode") String awbCode
    );

}

