package com.access.cargo.dashboard.kpi.model.Entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "awb_offloading")
public class AwbOffLoading implements Serializable {

    private static final long serialVersionUID = 6640988947308979163L;
    @Id

    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "awb")
    private Awb awb;

    @Column(name = "pieces", columnDefinition = "integer default 0")
    private Integer pieces = 0;

    @Column(name = "pieces_total", columnDefinition = "integer default 0")
    private Integer piecesTotal = 0;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gerobak")
    private Gerobak gerobak;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uld")
    private Uld uld;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "flight_sch")
    private FlightList flightSch;


    /**/

    @Column(name = "state")
    private Integer state;

    @Column(name = "created_datetime")
    private LocalDateTime createdDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by")
    private AccountUser createdBy;

    @Column(name = "modified_datetime")
    private LocalDateTime modifiedDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "modified_by")
    private AccountUser modifiedBy;
}
