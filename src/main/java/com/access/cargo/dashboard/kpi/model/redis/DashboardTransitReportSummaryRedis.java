package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class DashboardTransitReportSummaryRedis implements Serializable {

    private static final long serialVersionUID = 837244733282546102L;
    @Id
    Long id;

    BigDecimal transitFlight;
    BigDecimal transitAwb;
    BigDecimal transitPieces;
    BigDecimal transitKg;
}
