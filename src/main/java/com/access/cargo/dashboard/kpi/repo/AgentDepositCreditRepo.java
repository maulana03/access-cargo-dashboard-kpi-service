package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.Entity.AgentDepositCredit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface AgentDepositCreditRepo extends JpaRepository<AgentDepositCredit, Long> {

    Collection<AgentDepositCredit> findAllByStatus(Integer status);


}
