package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.redis.DashboardManifestReportDataDetailRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardPreManifestReportDataDetailRedis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DashboardManifestReportDataDetailRedisRepo extends JpaRepository<DashboardManifestReportDataDetailRedis, String> {
}
