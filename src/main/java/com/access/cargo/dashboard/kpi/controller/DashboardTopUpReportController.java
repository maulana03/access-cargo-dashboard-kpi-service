package com.access.cargo.dashboard.kpi.controller;


import com.access.cargo.dashboard.kpi.config.JwtConstants;
import com.access.cargo.dashboard.kpi.model.DashboardKpiConstants;
import com.access.cargo.dashboard.kpi.model.redis.*;
import com.access.cargo.dashboard.kpi.repo.DashboardKpiTableRedisRepo;
import com.access.cargo.dashboard.kpi.service.AccountService;
import com.access.cargo.dashboard.kpi.service.DashboardKpiService;
import com.access.cargo.dashboard.kpi.service.DashboardTopUpService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@CrossOrigin
@Slf4j
@RestController
@Validated
public class DashboardTopUpReportController {
    static final String TOP_UP_REPORT = "topup/dashboardKpi";
    static final String TOP_UP_REPORT_SUMMARY = TOP_UP_REPORT + "/summary";
    static final String TOP_UP_REPORT_DETAIL = TOP_UP_REPORT + "/detail";

    @Autowired
    DashboardTopUpService dashboardTopUpService;

    @Autowired
    AccountService accountService;

    @GetMapping(TOP_UP_REPORT_SUMMARY)
    public ResponseEntity getSumaryTopUpReport(
            @RequestHeader(value = JwtConstants.TOKEN_HEADER) String token
            , @ModelAttribute DashboardReportParams params) {
        Collection<String> authRoles = new LinkedList<>();
        authRoles.add("ROLE_SUPER_ADMIN");
        authRoles.add("ROLE_REPORT");
        authRoles.add("ROLE_ADMIN");
        authRoles.add("ROLE_REPORT_TONASE");
        authRoles.add("ROLE_ASYST_OPS");

        if (!accountService.isAuthorized(token, authRoles)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(dashboardTopUpService.getSummaryTopUp(params.getStartDate_(),params.getEndDate_(),params.getAgent_()), HttpStatus.OK);

    }

    @GetMapping(TOP_UP_REPORT_DETAIL)
    public ResponseEntity getDetailTopUpReport(
            @RequestHeader(value = JwtConstants.TOKEN_HEADER) String token
            , @ModelAttribute DashboardReportParams params) {
        Collection<String> authRoles = new LinkedList<>();
        authRoles.add("ROLE_SUPER_ADMIN");
        authRoles.add("ROLE_REPORT");
        authRoles.add("ROLE_ADMIN");
        authRoles.add("ROLE_REPORT_TONASE");
        authRoles.add("ROLE_ASYST_OPS");

        if (!accountService.isAuthorized(token, authRoles)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(dashboardTopUpService.getDetailTopUp(params.getStartDate_(),params.getEndDate_(),params.getAgent_()), HttpStatus.OK);

    }
}
