package com.access.cargo.dashboard.kpi.service;


import com.access.cargo.dashboard.kpi.model.Entity.Awb;
import com.access.cargo.dashboard.kpi.model.Entity.AwbWeighing;
import com.access.cargo.dashboard.kpi.model.Entity.Reservation;
import com.access.cargo.dashboard.kpi.model.redis.AgentAccountTransactionRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardCcaReportDataDetailRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardCcaReportSummaryRedis;
import com.access.cargo.dashboard.kpi.repo.AwbRepo;
import com.access.cargo.dashboard.kpi.repo.AwbWeighingRepo;
import com.access.cargo.dashboard.kpi.repo.DashboardCcaReportDataDetailRedisRepo;
import com.access.cargo.dashboard.kpi.repo.ReservationRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DashboardCcaService {
    @Autowired
    DashboardCcaReportDataDetailRedisRepo dashboardCcaReportDataDetailRedisRepo;

    @Autowired
    AgentAccountService agentAccountService;

    @Autowired
    AwbWeighingRepo awbWeighingRepo;

    @Autowired
    AwbRepo awbRepo;

    @Autowired
    ReservationRepo reservationRepo;

    public DashboardCcaReportSummaryRedis getSummary(String token,LocalDate dt1,LocalDate dt2,String agent,Long agentId,String branch){
        Pageable pageable= null;
        if (pageable == null || pageable.getPageSize() == 20){
            pageable = PageRequest.of(0,1000, Sort.by(Sort.Direction.DESC,"createdDateTime"));
        }

        Collection<AgentAccountTransactionRedis> agentAccountTransactionRedisColl= agentAccountService.getAgentAccountByParams(token,agentId,dt1,dt2,pageable).stream().filter(x -> x.getRemarks().indexOf("AWB") >=0).collect(Collectors.toList());
        agentAccountTransactionRedisColl.stream().filter(distinctByKey(x -> x.getReference())).collect(Collectors.toList());

        BigDecimal sumKgCCAPlus = BigDecimal.ZERO;
        BigDecimal sumKgCCAMinus = BigDecimal.ZERO;
        BigDecimal sumTotalCCA= BigDecimal.ZERO;

        for (AgentAccountTransactionRedis amt : agentAccountTransactionRedisColl) {
            if(amt.getTransactionTypeCode().equals("CREDIT")){
                sumKgCCAPlus = sumKgCCAPlus.add(amt.getAmount());
            }else{
                sumKgCCAMinus = sumKgCCAMinus.add(amt.getAmount());
            }
        }
        sumTotalCCA = sumKgCCAPlus.subtract(sumKgCCAMinus);
        DashboardCcaReportSummaryRedis dashboardCcaReportSummaryRedis =  new DashboardCcaReportSummaryRedis();
        dashboardCcaReportSummaryRedis.setAwbCCA(BigDecimal.valueOf(agentAccountTransactionRedisColl.size()));
        dashboardCcaReportSummaryRedis.setKgCCAPlus(sumKgCCAPlus);
        dashboardCcaReportSummaryRedis.setKgCCAMinus(sumKgCCAMinus);
        dashboardCcaReportSummaryRedis.setTotalCCA(sumTotalCCA);
        return dashboardCcaReportSummaryRedis;
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    public Collection<DashboardCcaReportDataDetailRedis> getDetail(String token,LocalDate dt1,LocalDate dt2,String agent,Long agentId,String branch){
        Collection<DashboardCcaReportDataDetailRedis> dashboardCcaReportDataDetailRedis = new LinkedList<>();
        Pageable pageable= null;
        if (pageable == null || pageable.getPageSize() == 20){
            pageable = PageRequest.of(0,1000, Sort.by(Sort.Direction.DESC,"createdDateTime"));
        }

        Collection<AgentAccountTransactionRedis> agentAccountTransactionRedisColl= agentAccountService.getAgentAccountByParams(token,agentId,dt1,dt2,pageable).stream().filter(x -> x.getRemarks().indexOf("AWB") >=0).collect(Collectors.toList());
        agentAccountTransactionRedisColl.stream().filter(distinctByKey(x -> x.getReference())).collect(Collectors.toList());
        for (AgentAccountTransactionRedis amt : agentAccountTransactionRedisColl) {
            DashboardCcaReportDataDetailRedis dashboardCcaReportDataDetailRedis1 = new DashboardCcaReportDataDetailRedis();
            dashboardCcaReportDataDetailRedis1.setIdTranscation(amt.getCode());
            dashboardCcaReportDataDetailRedis1.setAgent(amt.getAgentName());
            dashboardCcaReportDataDetailRedis1.setAwb(amt.getReference());
            dashboardCcaReportDataDetailRedis1.setTransaction(amt.getRemarks());//
            dashboardCcaReportDataDetailRedis1.setTransactionTypeCode(amt.getTransactionTypeCode());
            dashboardCcaReportDataDetailRedis1.setAmount(amt.getAmount());


            AwbWeighing awbWeighing = awbWeighingRepo.findFirstByAwbCodeAndStatus(amt.getReference(),1);
            BigDecimal kgcca = BigDecimal.ZERO;
            if(awbWeighing != null){
                kgcca = awbWeighing.getChargeableWeight();
            }

//            Awb awb = awbRepo.findFirstByCode(amt.getReference());
            Reservation reservation1 = reservationRepo.findFirstByAwb(amt.getReference());
            BigDecimal kgbook = BigDecimal.ZERO;
            LocalDate dt = null;
            if(reservation1 != null){
                kgbook = reservation1.getChargeableWeight();
                dt = reservation1.getFlightDate();
            }

            dashboardCcaReportDataDetailRedis1.setKgCca(kgcca);//
            dashboardCcaReportDataDetailRedis1.setKgBook(kgbook);
            dashboardCcaReportDataDetailRedis1.setDate(dt);
            dashboardCcaReportDataDetailRedis1.setBranch("");
            dashboardCcaReportDataDetailRedis1.setStatus("");
            dashboardCcaReportDataDetailRedis.add(dashboardCcaReportDataDetailRedis1);
        }
        return dashboardCcaReportDataDetailRedis;
    }

}