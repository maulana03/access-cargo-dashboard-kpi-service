package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.Entity.AwbAcceptance;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AwbAcceptanceRepo extends JpaRepository<AwbAcceptance, Long> {


}
