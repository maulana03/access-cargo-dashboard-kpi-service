package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.Entity.AwbBuildUp;
import com.access.cargo.dashboard.kpi.model.Entity.AwbOffLoading;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.Collection;

public interface OffLoadingRepo extends JpaRepository<AwbOffLoading, Long> {
    Collection<AwbOffLoading> findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndState(LocalDateTime dateTime1, LocalDateTime dateTime2, Integer state);
    Collection<AwbOffLoading> findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStateAndAwb_AgentName(LocalDateTime dateTime1, LocalDateTime dateTime2, Integer state, String agentName);

}
