package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedList;

@Data
@NoArgsConstructor
@RedisHash("DashboardAWBInformation")
public class DashboardAwbInformationRedis implements Serializable {

    private static final long serialVersionUID = 7748723136214576701L;
    @Id
    Long id;

    String noAWB;
    String agent;
    String flight;
    String ori;
    String des;
    BigDecimal pieces;
    BigDecimal kg;
    String commodity;

    LinkedList<DashboardAwbMilestoneRedis> data;
}
