package com.access.cargo.dashboard.kpi.model.wrapper;

public class AwbStateConstants {
    public static final String EXE = "EXE";
    public static final String FOH = "FOH";
    public static final String RCS = "RCS";
    public static final String PRE = "PRE";
    public static final String MAN = "MAN";
    public static final String ARR = "ARR";
    public static final String DEP = "DEP";
    public static final String RCF = "RCF";
    public static final String VOID = "VOID";
    public static final String CAN = "CAN";

    /**/
    public static final String ISSUED = "ISSUED";
    public static final String RA_XRAY = "RA_XRAY";
    public static final String RA_WEIGHING = "RA_WEIGHING";
    public static final String RA_ACCEPTANCE = "RA_ACCEPTANCE";
    public static final String ACCEPTANCE = "ACCEPTANCE";
    public static final String WEIGHING = "WEIGHING ";
    public static final String PLACEMENT = "PLACEMENT";
    public static final String BUILDUP = "BUILDUP";
    public static final String LOADING = "LOADING";
    public static final String OFFLOAD = "OFFLOAD";
    public static final String DEPARTURE = "DEPARTURE";
    public static final String ARRIVAL = "ARRIVAL";
    public static final String RECEIVING = "RECEIVING";
    public static final String UNLOADING_INCOMING = "UNLOADING_INCOMING";
    public static final String PLACEMENT_INCOMING = "PLACEMENT_INCOMING";
    public static final String CANCELED = "CANCELED";
    public static final String REJECTED = "REJECTED";

    /**/
    public static final String SRC_RA_AWB_REGISTER = "SRC_RA_AWB_REGISTER";
    public static final String SRC_RESERVATION = "SRC_RESERVATION";
    public static final String SRC_MANIFEST= "SRC_MANIFEST";
}
