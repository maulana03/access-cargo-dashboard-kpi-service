package com.access.cargo.dashboard.kpi.model.wrapper.tes;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@NoArgsConstructor
@Data
public class Root {
    String name;
    BigDecimal bkdWeightAmount;
    BigDecimal bkdRevAmount;

    BigDecimal raWeightAmount;
    BigDecimal raRevAmount;

    BigDecimal whoWeightAmount;
    BigDecimal whoRevAmount;

    BigDecimal whmWeightAmount;
    BigDecimal whmRevAmount;

    BigDecimal whiWeightAmount;
    BigDecimal whiRevAmount;

//    BigDecimal whtWeightAmount;
//    BigDecimal whtRevAmount;


}