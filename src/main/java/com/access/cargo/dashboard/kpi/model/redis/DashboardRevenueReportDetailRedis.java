package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.LinkedList;

@Data
@NoArgsConstructor
@RedisHash("DashboardRevenueReportDetailRedis")
public class DashboardRevenueReportDetailRedis implements Serializable {
    private static final long serialVersionUID = 2294781635335136145L;
    @Id
    Long id;
    DashboardReportParams params;
    DashboardRevenueReportDataDetailRedis data;
}
