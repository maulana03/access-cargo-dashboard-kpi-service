package com.access.cargo.dashboard.kpi.model.Entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "ra_acceptance", schema = "public")
public class RaAcceptance implements Serializable {

    @Id
    @Column(name = "id")
    private Long id;


    @Column(name = "weight")
    private BigDecimal weight;

    @Column(name = "rates_total")
    private BigDecimal ratesTotal;

    @Column(name = "status")
    private Integer status;

    @Column(name = "status_desc")
    private String statusDesc;

    @Column(name = "agent")
    private String agent;

    @Column(name = "modified_datetime")
    private LocalDateTime modifiedDateTime;

    @Column(name = "created_datetime")
    private LocalDateTime createdDateTime;

    @Column(name = "airline")
    private String airline;

    @Column(name = "pieces")
    private Integer pieces;

    @Column(name = "awb")
    private String awb;

    @Column(name = "xray_status")
    private String xrayStatus;

    @Column(name = "weight_actual")
    private BigDecimal weightActual;

    @Column(name = "pieces_actual")
    private Integer piecesActual;

    @Column(name = "agent_id")
    private Long agentId;

}
