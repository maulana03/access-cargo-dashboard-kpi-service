package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.Entity.AwbWeighing;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Collection;

@Repository
public interface AwbWeighingRepo extends PagingAndSortingRepository<AwbWeighing, Long> {

    Collection<AwbWeighing> findAll();

    Collection<AwbWeighing> findAllByCreatedDateTimeAfterAndStatus(LocalDateTime dateTime, Integer status);

    Collection<AwbWeighing> findAllByCreatedDateTimeAfterAndStatusConfirmationAndStatus(LocalDateTime dateTime, Integer statusConfirmation, Integer status);
    Collection<AwbWeighing> findAllByAgentNameAndCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatusConfirmationAndStatus(String agentName,LocalDateTime dateTime1, LocalDateTime dateTime2,Integer statusConfirmation, Integer status);
    Collection<AwbWeighing> findAllByAwbCodeAndCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatusConfirmationAndStatus(String awbCode,LocalDateTime dateTime1, LocalDateTime dateTime2,Integer statusConfirmation, Integer status);
    Collection<AwbWeighing> findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatusConfirmationAndStatus(LocalDateTime dateTime1, LocalDateTime dateTime2,Integer statusConfirmation, Integer status);
    Collection<AwbWeighing> findAllByAgentNameAndStatusConfirmationAndStatus(String agentName,Integer statusConfirmation, Integer status);
    Collection<AwbWeighing> findAllByAwbCodeAndStatusConfirmationAndStatus(String awbCode,Integer statusConfirmation, Integer status);

    AwbWeighing findFirstByAwbCodeAndStatus(String awbCode, Integer status);

    Collection<AwbWeighing>findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(LocalDateTime dateTime1, LocalDateTime dateTime2, Integer status);

    boolean existsByAwbCodeAndStatus(String awbCode, Integer status);

}
