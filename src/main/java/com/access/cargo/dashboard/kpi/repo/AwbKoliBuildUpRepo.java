package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.Entity.AwbKoliBuildUp;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;

public interface AwbKoliBuildUpRepo extends JpaRepository<AwbKoliBuildUp, Long> {
    AwbKoliBuildUp findFirstByFlightSch_DeparatureDateAndFlightSch_DeparatureTime(LocalDateTime depDate, String depTime);
}
