package com.access.cargo.dashboard.kpi.model.wrapper;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@NoArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountUserResp extends CommonResp implements Serializable {
    private static final long serialVersionUID = -7500318853324524475L;

    Long id;
    String username;
    String name;
    String email;
    String mobilePhone;

    String status;
    Integer statusActive;
    Long createdById;
    String createdByName;
    LocalDateTime createdDateTime;

    Long modifiedById;
    String modifiedByName;
    LocalDateTime modifiedDateTime;
}
