package com.access.cargo.dashboard.kpi.model.Entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "flight_list")
public class FlightList {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "airline_code", length = 3)
    private String airlineCode;

    @Column(name = "sector", length = 3)
    private String sector;

    @Column(name = "departure_date")
    private LocalDateTime deparatureDate;

    @Column(name = "departure_time")
    private String deparatureTime;

}
