package com.access.cargo.dashboard.kpi.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@Slf4j
//@RestController
@Validated
public class dashboardKpiRedisInsertController {
  /*  @Autowired
    DashboardKpiService dashboardKpiService;

    @Autowired
    DashboardKpiTableRedisRepo dashboardKpiTableRedisRepo;

    @Autowired
    DashboardKpiSummaryRedisRepo dashboardKpiSummaryRedisRepo;

    @Autowired
    DashboardKpiChartRedisRepo dashboardKpiChartRedisRepo;

    @PostMapping("today-redis")
    public ResponseEntity insertDummy(@RequestParam(value = "dt1") String dt1, @RequestParam(value = "dt2") String dt2) {
        dashboardKpiService.summaryDummy(dt1,dt2);
        dashboardKpiService.dummyDashboardKpiTableAgentAndAirlinesTodayRedis(dt1,dt2);
        dashboardKpiService.dummyDashboardKpiChartAgentBookingAndAirlinesRedis(dt1,dt2);
        dashboardKpiService.dummyDashboardKpiChartAgentWHIAndAirlinesRedis(dt1,dt2);
        dashboardKpiService.dummyDashboardKpiChartAgentWHOAndAirlinesRedis(dt1,dt2);
        dashboardKpiService.dummyDashboardKpiChartAgentWHTAndAirlinesRedis(dt1,dt2);
        dashboardKpiService.dummyDashboardKpiChartAgentRAAndAirlinesRedis(dt1,dt2);
        return new ResponseEntity("sukses", HttpStatus.OK);
    }

    @DeleteMapping("DeleteAllDashboardKpiChartRedis-redis")
    public ResponseEntity delAllDashboardKpiChartRedis(
    ) {
        dashboardKpiChartRedisRepo.deleteAll();
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("DeleteAlldashboardKpiTableRedisRepo-redis")
    public ResponseEntity delAlldashboardKpiTableRedisRepoRedis(
    ) {
        dashboardKpiTableRedisRepo.deleteAll();
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("DeleteAlldashboardKpiSummaryRedisRepo-redis")
    public ResponseEntity delAlldashboardKpiSummaryRedisRepoRedis(
    ) {
        dashboardKpiSummaryRedisRepo.deleteAll();
        return new ResponseEntity(HttpStatus.OK);
    }
*/
}
