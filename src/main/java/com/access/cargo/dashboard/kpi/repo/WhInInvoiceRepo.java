package com.access.cargo.dashboard.kpi.repo;


import com.access.cargo.dashboard.kpi.model.Entity.WhInInvoice;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface WhInInvoiceRepo extends PagingAndSortingRepository<WhInInvoice, Long> {

    Collection<WhInInvoice> findAll();

    boolean existsByWhCodeAndAwbAndStatus(String whCode, String awb, Integer status);

    boolean existsByWhCodeAndAwbAndStatusAndIdIsNot(String whCode, String awb, Integer status, Long id);
    boolean existsByAwbAndStatus(String awb, Integer status);
    WhInInvoice findFirstByAwbAndStatus(String awb, Integer status);
}
