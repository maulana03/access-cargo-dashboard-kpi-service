package com.access.cargo.dashboard.kpi.model.Entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@NoArgsConstructor
@Data
@Entity
@Table(name = "account_user")
public class AccountUser implements Serializable {
    private static final long serialVersionUID = -7461152259547990882L;

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;


}
