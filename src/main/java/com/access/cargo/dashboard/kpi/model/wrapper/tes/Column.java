package com.access.cargo.dashboard.kpi.model.wrapper.tes;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Column {
    public String v;
    public String f;


}