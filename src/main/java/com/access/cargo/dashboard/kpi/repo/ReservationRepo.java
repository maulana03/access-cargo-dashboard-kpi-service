package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.Entity.Awb;
import com.access.cargo.dashboard.kpi.model.Entity.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.Collection;

public interface ReservationRepo extends JpaRepository<Reservation, Long> {

    Collection<Reservation> findAllByStatusAndStatusDescAndFlightDate(Integer status, String statatusDesc, LocalDate flightDate);

    Collection<Reservation> findAllByStatusAndStatusDesc(Integer status, String statatusDesc);

    Reservation findFirstByAwb(String awb);

}
