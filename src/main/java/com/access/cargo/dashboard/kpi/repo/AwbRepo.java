package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.Entity.Awb;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;

public interface AwbRepo extends JpaRepository<Awb, Long> {


    Collection<Awb> findAllByFlightDate(LocalDate flightDate);

    Collection<Awb> findAllByCode(String code);

    Collection<Awb> findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndSrcRsvAndState1NotIn(LocalDateTime dateTime1, LocalDateTime dateTime2, String srcRsv, String[] state1List);

    Awb findFirstByCode(String code);

    Collection<Awb> findAllByCodeIn(String[] codeList);

    Collection<Awb> findAllByCodeIn(Collection<String> awbCodes);

}
