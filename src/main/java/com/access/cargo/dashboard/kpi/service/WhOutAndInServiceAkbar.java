package com.access.cargo.dashboard.kpi.service;

import com.access.cargo.dashboard.kpi.config.DateTimeCfg;
import com.access.cargo.dashboard.kpi.model.Entity.Agent;
import com.access.cargo.dashboard.kpi.model.Entity.Awb;
import com.access.cargo.dashboard.kpi.model.Entity.AwbManManual;
import com.access.cargo.dashboard.kpi.model.Entity.WhCustomerInvoiceSetup;
import com.access.cargo.dashboard.kpi.model.redis.WhChargesRedis;
import com.access.cargo.dashboard.kpi.model.redis.WhInInvoiceRedis;
import com.access.cargo.dashboard.kpi.model.redis.WhOutInvoiceRedis;
import com.access.cargo.dashboard.kpi.model.wrapper.AwbRedis;
import com.access.cargo.dashboard.kpi.repo.AgentRepo;
import com.access.cargo.dashboard.kpi.repo.WhCustomerInvoiceSetupRepo;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class WhOutAndInServiceAkbar {

    @Autowired
    AgentRepo agentRepo;

    @Getter
    StringBuilder msg = new StringBuilder();

    @Getter
    HttpStatus httpStatus = HttpStatus.OK;

    @Autowired
    WhCustomerInvoiceSetupRepo whCustomerInvoiceSetupRepo;

    /*WH-Service-Charges*/

    public List<BigDecimal> whServiceCharges(WhCustomerInvoiceSetup whCustomerInvoiceSetup, BigDecimal chargeableWeight, Integer days, String whCode, String whType, LocalDate localDate) {

        BigDecimal rates = BigDecimal.ZERO;
        BigDecimal whServiceChargesTotal = BigDecimal.ZERO;

        if (whCustomerInvoiceSetup.getWhChargesStatus().equals(1)) {
            rates = whCustomerInvoiceSetup.getWhChargesAmount(); /*outoging 650, incoming 700*/
            Integer ratesBaseDays = 3; /*default: 3 days*/


//            WhChargesRedis whChargesRedis = whServiceChargesService.getByWhCodeAndWhTypeAndDate(token, whCode, whType, localDate);
            WhChargesRedis whChargesRedis = new WhChargesRedis();
            whServiceChargesTotal = rates.multiply(chargeableWeight);
            if (days.compareTo(ratesBaseDays) > 0) {
                Collection<WhChargesRedis.AdditionalCharges> additionalCharges = whChargesRedis.getAdditionalCharges();
                boolean anyMatch = additionalCharges.stream().anyMatch(x -> x.getDayFrom() >= days);
                if (anyMatch) {
                    Collection<WhChargesRedis.AdditionalCharges> additionalChargesFiltered = additionalCharges
                            .stream()
                            .filter(x -> x.getDayFrom() <= days)
                            .collect(Collectors.toList());

                    BigDecimal additionalChargesTotal = BigDecimal.ZERO;
                    for (WhChargesRedis.AdditionalCharges charges : additionalChargesFiltered) {
                        additionalChargesTotal.add(charges.getCharges());
                    }

                    whServiceChargesTotal = (rates.add(additionalChargesTotal)).multiply(chargeableWeight);
                }

            }
        }

        List<BigDecimal> list = new ArrayList<BigDecimal>();
        list.add(rates);
        list.add(whServiceChargesTotal);

        return list;

    }

    /*WH-Cargo-Service-Charges*/
    public List<BigDecimal> whCargoServiceCharges(WhCustomerInvoiceSetup whCustomerInvoiceSetup, BigDecimal chargeableWeight) {
        List<BigDecimal> list = new ArrayList<BigDecimal>();
        if (whCustomerInvoiceSetup == null) {
            msg.append("Warehouse need setup Customer Invoice");
            httpStatus = HttpStatus.BAD_REQUEST;
            return null;
        }

        BigDecimal whCargoServiceChargesRates = BigDecimal.ZERO;
        BigDecimal whCargoServiceChargesTotal = BigDecimal.ZERO;

        if (whCustomerInvoiceSetup.getWhCargoChargesStatus().equals(1)) {
            whCargoServiceChargesRates = whCustomerInvoiceSetup.getWhCargoChargesAmount();
            whCargoServiceChargesTotal = whCargoServiceChargesRates.multiply(chargeableWeight);
        }
        list.add(whCargoServiceChargesRates);
        list.add(whCargoServiceChargesTotal);

        return list;
    }

    /*WH-Cargo-Handling-Charges*/
    public List<BigDecimal> whCargoHandlingCharges(WhCustomerInvoiceSetup whCustomerInvoiceSetup, BigDecimal chargeableWeight) {
        List<BigDecimal> list = new ArrayList<BigDecimal>();

        if (whCustomerInvoiceSetup == null) {
            msg.append("Warehouse need setup Customer Invoice");
            httpStatus = HttpStatus.BAD_REQUEST;
            return null;
        }
        BigDecimal whHandlingChargesRedisRates = BigDecimal.ZERO;
        BigDecimal whHandlingChargesTotal = BigDecimal.ZERO;
        if (whCustomerInvoiceSetup.getHfChargesStatus().equals(1)) {
            whHandlingChargesRedisRates = whCustomerInvoiceSetup.getHfChargesAmount();
            whHandlingChargesTotal = whHandlingChargesRedisRates.multiply(chargeableWeight);
        }
        list.add(whHandlingChargesRedisRates);
        list.add(whHandlingChargesTotal);

        return list;
    }

    /*WH-KADE-Charges*/
    public List<BigDecimal> whKadeCharges(WhCustomerInvoiceSetup whCustomerInvoiceSetup, BigDecimal chargeableWeight) {
        List<BigDecimal> list = new ArrayList<BigDecimal>();

        if (whCustomerInvoiceSetup == null) {
            msg.append("Warehouse need setup Customer Invoice");
            httpStatus = HttpStatus.BAD_REQUEST;
            return null;
        }

        BigDecimal whKadeChargesRedisRates = BigDecimal.ZERO;
        BigDecimal whKadeChargesRedisTotal = BigDecimal.ZERO;
        if (whCustomerInvoiceSetup.getKadeChargesStatus().equals(1)) {
            whKadeChargesRedisRates = whCustomerInvoiceSetup.getKadeChargesAmount();
            whKadeChargesRedisTotal = whKadeChargesRedisRates.multiply(chargeableWeight);
        }
        list.add(whKadeChargesRedisRates);
        list.add(whKadeChargesRedisTotal);

        return list;
    }

    /*WH-DOC-Charges*/

    public List<BigDecimal> whDocCharges(WhCustomerInvoiceSetup whCustomerInvoiceSetup) {
        List<BigDecimal> list = new ArrayList<BigDecimal>();

        if (whCustomerInvoiceSetup == null) {
            msg.append("Warehouse need setup Customer Invoice");
            httpStatus = HttpStatus.BAD_REQUEST;
            return null;
        }

        BigDecimal whDocAmount = BigDecimal.ZERO;
        if (whCustomerInvoiceSetup.getDocChargesStatus().equals(1)) {

            whDocAmount = whCustomerInvoiceSetup.getDocChargesAmount();
        }
        list.add(whDocAmount);

        return list;
    }

    /*PPN*/

    public List<BigDecimal> ppnGet(WhCustomerInvoiceSetup whCustomerInvoiceSetup, BigDecimal subTotal) {
        List<BigDecimal> list = new ArrayList<BigDecimal>();

        if (whCustomerInvoiceSetup == null) {
            msg.append("Warehouse need setup Customer Invoice");
            httpStatus = HttpStatus.BAD_REQUEST;
            return null;
        }

        BigDecimal ppnPersen = BigDecimal.ZERO;
        BigDecimal ppn = BigDecimal.ZERO;
        if (whCustomerInvoiceSetup.getPpnStatus().equals(1)) {
            ppnPersen = whCustomerInvoiceSetup.getPpnAmount();
            ppn = ppnPersen.divide(BigDecimal.valueOf(100)).multiply(subTotal);
        }
        list.add(ppnPersen);
        list.add(ppn);

        return list;
    }

    /*PPH*/

    public List<BigDecimal> pphGet(WhCustomerInvoiceSetup whCustomerInvoiceSetup,BigDecimal subTotal) {
        List<BigDecimal> list = new ArrayList<BigDecimal>();
        BigDecimal pphPersen = BigDecimal.ZERO;
        BigDecimal pph = BigDecimal.ZERO;
        if (whCustomerInvoiceSetup.getPphStatus().equals(1)) {

            pphPersen = whCustomerInvoiceSetup.getPphAmount();
            pph = pphPersen.divide(BigDecimal.valueOf(100)).multiply(subTotal);
        }
        list.add(pphPersen);
        list.add(pph);

        return list;
    }

    public BigDecimal getTotalInvoice(WhCustomerInvoiceSetup redis, Integer nDays, BigDecimal weight) {
        BigDecimal rates = redis.getWhChargesAmount();
        //BigDecimal ratesBase = redis.getRatesBase();
        //Integer ratesBaseDays = redis.getRatesBaseDays(); /*default: 3 days*//
        BigDecimal ratesBase = BigDecimal.valueOf(0); //???
        Integer ratesBaseDays = 3; /*default: 3 days*/

        WhChargesRedis whChargesRedis = null;
        Collection<WhChargesRedis.AdditionalCharges> additionalCharges = new LinkedList<>();


        BigDecimal totalInvoice = ratesBase.multiply(weight);
        if (nDays.compareTo(ratesBaseDays) > 0) {
            Integer diffDays = nDays - ratesBaseDays;
            totalInvoice = totalInvoice.add(rates.multiply(BigDecimal.valueOf(diffDays)).multiply(weight));
        }
        return totalInvoice;
    }

    Boolean checkAgentCredit(String agentName) {
        boolean isExist = false;
        Agent agent = agentRepo.findFirstByNameAndStatus(agentName, 1);
        String payment = "";
        if (agent != null) {
            payment = agent.getPaymentMethods();
        }

        int intIndex = payment.toUpperCase().indexOf("DEPOSIT_CREDIT");
        if (intIndex >= 0) {
            isExist = true;
        }

        return isExist;
    }



    public WhOutInvoiceRedis getByAwbRedisOut(AwbRedis awbRedis) {
        String whCode = "HLP"; /*todo*/
        String whName = "HLP"; /*todo*/
        String whType = "OUTGOING"; /*todo*/
        String[] paymentTypes = new String[0];
        String awb = awbRedis.getCode();
        String airline = "IL"; /*todo*/
        String flightNo = awbRedis.getFlightNo();
        String ori = awbRedis.getOrigin();
        String des = awbRedis.getDestination();
        String route = awbRedis.getSector();
        Integer pieces = awbRedis.getPieces();
        BigDecimal chargeableWeight = awbRedis.getChargeableWeight();
        String agent = awbRedis.getAgentName();
        String shipper = awbRedis.getShipperName();
        String consignee = awbRedis.getConsigneeName();
        LocalDate localDate = LocalDate.now(DateTimeCfg.ZONE_ID);

        String paymentMethods="";
        Integer days = 3; //default
        BigDecimal amount = BigDecimal.ZERO;
        BigDecimal amountChange = BigDecimal.ZERO;



        WhCustomerInvoiceSetup whCustomerInvoiceSetup = whCustomerInvoiceSetupRepo.findFirstByCustomerIdAndWhCodeAndWhTypeAndStatus(awbRedis.getAgentId(),whCode,whType,1);

        if (whCustomerInvoiceSetup == null) {
            whCustomerInvoiceSetup = whCustomerInvoiceSetupRepo.findFirstByCustomerIdAndWhCodeAndWhTypeAndStatus(Long.valueOf(90),whCode,whType,1);
        }

        if(whCustomerInvoiceSetup != null){
            String paymentTypesTemp = whCustomerInvoiceSetup.getPaymentTypes();
            paymentTypes = paymentTypesTemp.split(",");
            if(paymentTypes.length == 1){
                paymentTypes[0] =paymentTypesTemp;
            }
        }

        /*todo: WH-Service-Charges*/
        List<BigDecimal> bigDecimals = whServiceCharges(whCustomerInvoiceSetup,chargeableWeight,days,whCode,whType,localDate);

        BigDecimal rates = bigDecimals.get(0);
        BigDecimal whServiceChargesTotal =  bigDecimals.get(1);

        /*todo: WH-Cargo-Service-Charges*/
        List<BigDecimal> whCargoServiceChargesbigDecimals = whCargoServiceCharges(whCustomerInvoiceSetup,chargeableWeight);
        BigDecimal whCargoServiceChargesRates = whCargoServiceChargesbigDecimals.get(0);
        BigDecimal whCargoServiceChargesTotal =  whCargoServiceChargesbigDecimals.get(1);

        /*todo: WH-Cargo-Handling-Charges*/
        List<BigDecimal> whCargoHandlingChargesbigDecimals = whCargoHandlingCharges(whCustomerInvoiceSetup,chargeableWeight);
        BigDecimal whHandlingChargesRedisRates = whCargoHandlingChargesbigDecimals.get(0);
        BigDecimal whHandlingChargesTotal =  whCargoHandlingChargesbigDecimals.get(1);

        /*todo: WH-KADE-Charges*/

        List<BigDecimal> whKadeChargesRedisRatesbigDecimals = whKadeCharges(whCustomerInvoiceSetup,chargeableWeight);
        BigDecimal whKadeChargesRedisRates = whKadeChargesRedisRatesbigDecimals.get(0);
        BigDecimal whKadeChargesRedisTotal =  whKadeChargesRedisRatesbigDecimals.get(1);


        BigDecimal subTotal = whServiceChargesTotal.add(whCargoServiceChargesTotal).add(whHandlingChargesTotal).add(whKadeChargesRedisTotal);

        /*todo: PPN*/
//        BigDecimal ppnPersen = BigDecimal.ZERO;
//        BigDecimal ppn = BigDecimal.ZERO;

        List<BigDecimal> ppnGet = ppnGet(whCustomerInvoiceSetup,subTotal);
        BigDecimal ppnPersen = ppnGet.get(0);
        BigDecimal ppn =  ppnGet.get(1);


        /*todo: WH-DOC-Charges*/
        List<BigDecimal> whDocChargesRedisbigDecimals = whDocCharges(whCustomerInvoiceSetup);
        BigDecimal whDocAmount = whDocChargesRedisbigDecimals.get(0);

        /*todo: Others*/

        /**/
        BigDecimal others = BigDecimal.ZERO;

        /*todo: PPH*/

        List<BigDecimal> pphGet = pphGet(whCustomerInvoiceSetup,subTotal);
        BigDecimal pphPersen = pphGet.get(0);
        BigDecimal pph =  pphGet.get(1);

        BigDecimal totalTemp = subTotal.add(ppn.add(whDocAmount.add(others)));
        BigDecimal total = totalTemp.subtract(pph);

        WhOutInvoiceRedis whOutInvoiceRedis = new WhOutInvoiceRedis();
        whOutInvoiceRedis.setWhCode(whCode);
        whOutInvoiceRedis.setAwb(awb);
        whOutInvoiceRedis.setAirline(airline);
        whOutInvoiceRedis.setFlightNo(flightNo);
        whOutInvoiceRedis.setOri(ori);
        whOutInvoiceRedis.setDes(des);
        whOutInvoiceRedis.setRoute(route);
        whOutInvoiceRedis.setPieces(pieces);
        whOutInvoiceRedis.setChargeableWeight(chargeableWeight);
        whOutInvoiceRedis.setAgentId(awbRedis.getAgentId());
        String decodedAgent = StringEscapeUtils.unescapeHtml(agent);
        whOutInvoiceRedis.setAgent(decodedAgent);
        whOutInvoiceRedis.setShipper(shipper);
        whOutInvoiceRedis.setConsignee(consignee);
        whOutInvoiceRedis.setRates(rates);
        whOutInvoiceRedis.setWhServiceTotal(whServiceChargesTotal);
        whOutInvoiceRedis.setDays(days);
        whOutInvoiceRedis.setPaymentMethod(paymentMethods);
        whOutInvoiceRedis.setPaymentTypes(paymentTypes);
        whOutInvoiceRedis.setAmount(amount);
        whOutInvoiceRedis.setAmountChange(amountChange);
        whOutInvoiceRedis.setSubTotal(subTotal);
        whOutInvoiceRedis.setPpn(ppn);
        whOutInvoiceRedis.setPpnPercent(ppnPersen);
        whOutInvoiceRedis.setPph(pph);
        whOutInvoiceRedis.setPphPercent(pphPersen);
        whOutInvoiceRedis.setDocCharges(whDocAmount);
        whOutInvoiceRedis.setOthers(others);
        whOutInvoiceRedis.setTotal(total);
        whOutInvoiceRedis.setKadeCharges(whKadeChargesRedisRates);
        whOutInvoiceRedis.setKadeTotal(whKadeChargesRedisTotal);
        whOutInvoiceRedis.setCargoHandlingCharges(whHandlingChargesRedisRates);
        whOutInvoiceRedis.setCargoHandlingTotal(whHandlingChargesTotal);
//        whOutInvoiceRedis.setCargoHandlingCharges(BigDecimal.ZERO);
//        whOutInvoiceRedis.setCargoHandlingTotal(BigDecimal.ZERO);
        whOutInvoiceRedis.setCargoServiceCharges(whCargoServiceChargesRates);
        whOutInvoiceRedis.setCargoServiceTotal(whCargoServiceChargesTotal);

        return whOutInvoiceRedis;
    }

    public WhInInvoiceRedis getByAwbRedisIn(Awb awbRedis, Integer diffDays) {
        String whCode = "HLP"; /*todo*/
        String whName = "HLP"; /*todo*/
        String whType = "INCOMING"; /*todo*/
        String[] paymentTypes = new String[0];
        String awb = awbRedis.getCode();
        String airline = "IL"; /*todo*/
        String flightNo = awbRedis.getFlightNo();
        String ori = awbRedis.getOrigin();
        String des = awbRedis.getDestination();
        String route = awbRedis.getSector();
        Integer pieces = awbRedis.getPieces();
        BigDecimal chargeableWeight = awbRedis.getChargeableWeight();
        /**/
        String agent = awbRedis.getAgentName();
        String shipper = awbRedis.getShipperName();
        String consignee = awbRedis.getConsigneeName();
        /**/
        LocalDate localDate = LocalDate.now(DateTimeCfg.ZONE_ID);

        WhCustomerInvoiceSetup whCustomerInvoiceSetup = whCustomerInvoiceSetupRepo.findFirstByCustomerIdAndWhCodeAndWhTypeAndStatus(awbRedis.getAgentId(),whCode,whType,1);

        if (whCustomerInvoiceSetup == null) {
            whCustomerInvoiceSetup = whCustomerInvoiceSetupRepo.findFirstByCustomerIdAndWhCodeAndWhTypeAndStatus(Long.valueOf(90),whCode,whType,1);
        }
        if(whCustomerInvoiceSetup != null){
            String paymentTypesTemp = whCustomerInvoiceSetup.getPaymentTypes();
            paymentTypes = paymentTypesTemp.split(",");
            if(paymentTypes.length == 1){
                paymentTypes[0] =paymentTypesTemp;
            }
        }

        System.out.println("paymentTypes " +paymentTypes);
        Integer days = diffDays != null ? diffDays : 3;
        /* todo: WH-Service-Charges*/

        List<BigDecimal> bigDecimals = whServiceCharges(whCustomerInvoiceSetup,chargeableWeight,days,whCode,whType,localDate);
        BigDecimal rates = bigDecimals.get(0);
        BigDecimal whServiceChargesTotal =  bigDecimals.get(1);

        /*todo: WH-Cargo-Service-Charges*/

        List<BigDecimal> whCargoServiceChargesbigDecimals = whCargoServiceCharges(whCustomerInvoiceSetup,chargeableWeight);
        BigDecimal whCargoServiceChargesRates = whCargoServiceChargesbigDecimals.get(0);
        BigDecimal whCargoServiceChargesTotal =  whCargoServiceChargesbigDecimals.get(1);

        /*todo: WH-Cargo-Handling-Charges*/
        List<BigDecimal> whCargoHandlingChargesbigDecimals = whCargoHandlingCharges(whCustomerInvoiceSetup,chargeableWeight);
        BigDecimal whHandlingChargesRedisRates = whCargoHandlingChargesbigDecimals.get(0);
        BigDecimal whHandlingChargesTotal =  whCargoHandlingChargesbigDecimals.get(1);


        /*todo: WH-KADE-Charges*/

        List<BigDecimal> whKadeChargesRedisRatesbigDecimals = whKadeCharges(whCustomerInvoiceSetup,chargeableWeight);
        BigDecimal whKadeChargesRedisRates = whKadeChargesRedisRatesbigDecimals.get(0);
        BigDecimal whKadeChargesRedisTotal =  whKadeChargesRedisRatesbigDecimals.get(1);


        BigDecimal subTotal = whServiceChargesTotal.add(whCargoServiceChargesTotal).add(whHandlingChargesTotal).add(whKadeChargesRedisTotal);


        List<BigDecimal> ppnGet = ppnGet(whCustomerInvoiceSetup,subTotal);
        BigDecimal ppnPersen = ppnGet.get(0);
        BigDecimal ppn =  ppnGet.get(1);

       /* if(!whOutAndInService.checkAgentCredit(agent)){
            ppnPersen = BigDecimal.valueOf(10);
            ppn = ppnPersen.divide(BigDecimal.valueOf(100)).multiply(subTotal);
        }*/

        /*todo: WH-DOC-Charges*/

        List<BigDecimal> whDocChargesRedisbigDecimals = whDocCharges(whCustomerInvoiceSetup);
        BigDecimal whDocAmount = whDocChargesRedisbigDecimals.get(0);


        /*todo: Others*/

        /**/
        BigDecimal others = BigDecimal.ZERO;

        /*todo: PPH*/

        List<BigDecimal> pphGet = pphGet(whCustomerInvoiceSetup,subTotal);
        BigDecimal pphPersen = pphGet.get(0);
        BigDecimal pph =  pphGet.get(1);

        BigDecimal totalTemp = subTotal.add(ppn.add(whDocAmount.add(others)));
        BigDecimal total = totalTemp.subtract(pph);

        String paymentMethod = "";
        BigDecimal amount = BigDecimal.ZERO;
        BigDecimal amountChange = BigDecimal.ZERO;


        WhInInvoiceRedis whInvoiceRedis = new WhInInvoiceRedis();
        whInvoiceRedis.setWhCode(whCode);
        whInvoiceRedis.setAwb(awb);
        whInvoiceRedis.setAirline(airline);
        whInvoiceRedis.setFlightNo(flightNo);
        whInvoiceRedis.setOri(ori);
        whInvoiceRedis.setDes(des);
        whInvoiceRedis.setRoute(route);
        whInvoiceRedis.setPieces(pieces);
        whInvoiceRedis.setChargeableWeight(chargeableWeight);
        whInvoiceRedis.setAgentId(awbRedis.getAgentId());
        String decodedAgent = StringEscapeUtils.unescapeHtml(agent);
        whInvoiceRedis.setAgent(decodedAgent);
        whInvoiceRedis.setAgent(agent);
        whInvoiceRedis.setShipper(shipper);
        whInvoiceRedis.setConsignee(consignee);
        whInvoiceRedis.setRates(rates);
        whInvoiceRedis.setWhServiceTotal(whServiceChargesTotal);
        whInvoiceRedis.setDays(days);
        whInvoiceRedis.setPaymentMethod(paymentMethod);
        whInvoiceRedis.setPaymentTypes(paymentTypes);
        whInvoiceRedis.setAmount(amount);
        whInvoiceRedis.setAmountChange(amountChange);
        whInvoiceRedis.setSubTotal(subTotal);
        whInvoiceRedis.setPpn(ppn);
        whInvoiceRedis.setPpnPercent(ppnPersen);
        whInvoiceRedis.setPph(pph);
        whInvoiceRedis.setPphPercent(pphPersen);
        whInvoiceRedis.setDocCharges(whDocAmount);
        whInvoiceRedis.setOthers(others);
        whInvoiceRedis.setTotal(total);
        whInvoiceRedis.setKadeCharges(whKadeChargesRedisRates);
        whInvoiceRedis.setKadeTotal(whKadeChargesRedisTotal);
        whInvoiceRedis.setCargoHandlingCharges(whHandlingChargesRedisRates);
        whInvoiceRedis.setCargoHandlingTotal(whHandlingChargesTotal);
        whInvoiceRedis.setCargoServiceCharges(whCargoServiceChargesRates);
        whInvoiceRedis.setCargoServiceTotal(whCargoServiceChargesTotal);

        return whInvoiceRedis;
    }

}
