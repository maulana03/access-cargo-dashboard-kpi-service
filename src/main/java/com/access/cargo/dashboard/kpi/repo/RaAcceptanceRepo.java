package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.Entity.Awb;
import com.access.cargo.dashboard.kpi.model.Entity.RaAcceptance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.Collection;

public interface RaAcceptanceRepo extends JpaRepository<RaAcceptance, Long> {

//    Collection<RaAcceptance> findAllByStatus(Integer status);

    Collection<RaAcceptance> findAllByAwb(String awb);

    Collection<RaAcceptance> findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(LocalDateTime dateTime1, LocalDateTime dateTime2,Integer status);
    Collection<RaAcceptance> findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatusAndAgent(LocalDateTime dateTime1, LocalDateTime dateTime2,Integer status,String agent);

    RaAcceptance findFirstByAwb(String awb);

}
