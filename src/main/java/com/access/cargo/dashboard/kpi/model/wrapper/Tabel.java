package com.access.cargo.dashboard.kpi.model.wrapper;//package com.access.cargo.account.model.wrapper;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Tabel {
    private String jenis;
    private String name;
    private BigDecimal weight;
    private BigDecimal percent;

}
