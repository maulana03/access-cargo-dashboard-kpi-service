package com.access.cargo.dashboard.kpi.service;


import com.access.cargo.dashboard.kpi.model.redis.DashboardTransitReportDataDetailRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardTransitReportSummaryRedis;
import com.access.cargo.dashboard.kpi.repo.DashboardTransitReportDataDetailRedisRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DashboardTransitService {
    @Autowired
    DashboardTransitReportDataDetailRedisRepo dashboardTransitReportDataDetailRedisRepo;

    public DashboardTransitReportSummaryRedis getSummary(LocalDate dt1,LocalDate dt2,String agent,String branch){
        Collection<DashboardTransitReportDataDetailRedis> dashboardTransitReportDataDetailRedis;
        if(agent.equals("ALL")){
           dashboardTransitReportDataDetailRedis = dashboardTransitReportDataDetailRedisRepo.findAll().stream().filter(x -> x.getDate().isAfter(dt1) && x.getDate().isBefore(dt2)).collect(Collectors.toList());

        }else{
            dashboardTransitReportDataDetailRedis = dashboardTransitReportDataDetailRedisRepo.findAll().stream().filter(x -> x.getAgent().equals(agent) && x.getDate().isAfter(dt1) && x.getDate().isBefore(dt2)).collect(Collectors.toList());

        }


        BigDecimal sumTransitAwb = BigDecimal.ZERO;
        BigDecimal sumTransitPieces = BigDecimal.ZERO;
        BigDecimal sumTransitKg = BigDecimal.ZERO;

        for (DashboardTransitReportDataDetailRedis amt : dashboardTransitReportDataDetailRedis) {

            sumTransitAwb = sumTransitAwb.add(amt.getTransitAwb());
            sumTransitPieces = sumTransitPieces.add(amt.getTransitPieces());
            sumTransitKg = sumTransitKg.add(amt.getTransitKg());

        }

        DashboardTransitReportSummaryRedis dashboardTransitReportSummaryRedis =  new DashboardTransitReportSummaryRedis();
        dashboardTransitReportSummaryRedis.setTransitFlight(BigDecimal.valueOf(dashboardTransitReportDataDetailRedis.size()));
        dashboardTransitReportSummaryRedis.setTransitAwb(sumTransitAwb);
        dashboardTransitReportSummaryRedis.setTransitPieces(sumTransitPieces);
        dashboardTransitReportSummaryRedis.setTransitKg(sumTransitKg);
        return dashboardTransitReportSummaryRedis;
    }

    public Collection<DashboardTransitReportDataDetailRedis> getDetail(LocalDate dt1,LocalDate dt2,String agent,String branch){
        Collection<DashboardTransitReportDataDetailRedis> dashboardTransitReportDataDetailRedis;
        if(agent.equals("ALL")){
            dashboardTransitReportDataDetailRedis = dashboardTransitReportDataDetailRedisRepo.findAll().stream().filter(x -> x.getDate().isAfter(dt1) && x.getDate().isBefore(dt2)).collect(Collectors.toList());

        }else{
            dashboardTransitReportDataDetailRedis = dashboardTransitReportDataDetailRedisRepo.findAll().stream().filter(x -> x.getBranch().equals(branch) &&x.getAgent().equals(agent) && x.getDate().isAfter(dt1) && x.getDate().isBefore(dt2)).collect(Collectors.toList());

        }
        return dashboardTransitReportDataDetailRedis;
    }

}