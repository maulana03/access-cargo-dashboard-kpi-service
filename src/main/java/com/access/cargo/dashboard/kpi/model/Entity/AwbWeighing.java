package com.access.cargo.dashboard.kpi.model.Entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.id.enhanced.SequenceStyleGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@NoArgsConstructor
@Data
@Entity
@DynamicUpdate
@Table(name = "awb_weighing", schema = "public")
public class AwbWeighing implements Serializable {
    private static final String ID = "ID";
    private static final String AWB = "AWB";
    private static final String AWB_CODE = "AWB_CODE";
    private static final String COMMODITY_CODE = "COMMODITY_CODE";
    private static final String PRODUCT_CODE = "PRODUCT_CODE";
    private static final String SHC = "SHC";
    private static final String NOG = "NOG";
    private static final String PIECES = "PIECES";
    private static final String GROSS_WEIGHT = "GROSS_WEIGHT";
    private static final String VOLUME_CBM = "VOLUME_CBM";
    private static final String LENGTH = "LENGTH";
    private static final String WIDTH = "WIDTH";
    private static final String HEIGHT = "HEIGHT";
    private static final String VOLUME_WEIGHT = "VOLUME_WEIGHT";
    private static final String CHARGEABLE_WEIGHT = "CHARGEABLE_WEIGHT";
    /**/
    private static final String AGENT_ID = "AGENT_ID";
    private static final String AGENT_NAME = "AGENT_NAME";

    private static final String STATUS_CONFIRMATION = "STATUS_CONFIRMATION";

    /**/
    private static final String STATUS = "STATUS";
    private static final String STATUS_DESC = "STATUS_DESC";
    private static final String CREATED_DATETIME = "CREATED_DATETIME";
    private static final String CREATED_BY = "CREATED_BY";
    private static final String MODIFIED_DATETIME = "MODIFIED_DATETIME";
    private static final String MODIFIED_BY = "MODIFIED_BY";
    private static final long serialVersionUID = 1191197368183312116L;

    /**/

    @Id
    @GeneratedValue(generator = "awb_weighing_seq-generator")
    @GenericGenerator(
            name = "awb_weighing_seq-generator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.SEQUENCE_PARAM, value = "awb_weighing_seq"),
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.INITIAL_PARAM, value = "1"),
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.INCREMENT_PARAM, value = "1")
            }
    )
    @Column(name = ID)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = AWB)
    private Awb awb;

    @Column(name = AWB_CODE, length = 15)
    private String awbCode;

    @Column(name = COMMODITY_CODE, length = 7)
    private String commodityCode;

    @Column(name = PRODUCT_CODE, length = 5)
    private String productCode;

    @Column(name = SHC, length = 5)
    private String shc;

    @Column(name = NOG, length = 50)
    private String nog;

    @Column(name = PIECES)
    private Integer pieces;

    @Column(name = GROSS_WEIGHT, precision = 19, scale = 3)
    private BigDecimal grossWeight;

    @Column(name = VOLUME_CBM, precision = 19, scale = 3)
    private BigDecimal volumeCbm;

    @Column(name = LENGTH, precision = 19, scale = 3)
    private BigDecimal length;

    @Column(name = WIDTH, precision = 19, scale = 3)
    private BigDecimal width;

    @Column(name = HEIGHT, precision = 19, scale = 3)
    private BigDecimal height;

    @Column(name = VOLUME_WEIGHT, precision = 19, scale = 3)
    private BigDecimal volumeWeight;

    @Column(name = CHARGEABLE_WEIGHT, precision = 19, scale = 3)
    private BigDecimal chargeableWeight;

    /**/

    @Column(name = AGENT_ID)
    private Long agentId;

    @Column(name = AGENT_NAME, length = 50)
    private String agentName;

    @Column(name = STATUS_CONFIRMATION)
    private Integer statusConfirmation;

    /**/

    @Column(name = STATUS)
    private Integer status;

    @Column(name = STATUS_DESC, length = 30)
    private String statusDesc;

    @Column(name = CREATED_DATETIME)
    private LocalDateTime createdDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = CREATED_BY)
    private AccountUser createdBy;

    @Column(name = MODIFIED_DATETIME)
    private LocalDateTime modifiedDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = MODIFIED_BY)
    private AccountUser modifiedBy;

}
