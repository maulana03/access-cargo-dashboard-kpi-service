package com.access.cargo.dashboard.kpi.controller;


import com.access.cargo.dashboard.kpi.config.DateTimeCfg;
import com.access.cargo.dashboard.kpi.config.JwtConstants;
import com.access.cargo.dashboard.kpi.feign.AccountClient;
import com.access.cargo.dashboard.kpi.model.DashboardKpiConstants;
import com.access.cargo.dashboard.kpi.model.Entity.AccountUser;
import com.access.cargo.dashboard.kpi.model.redis.DashboardKpiChartRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardKpiParams;
import com.access.cargo.dashboard.kpi.model.wrapper.DashboardKpiSummary;
import com.access.cargo.dashboard.kpi.repo.DashboardKpiTableRedisRepo;
import com.access.cargo.dashboard.kpi.service.AccountService;
import com.access.cargo.dashboard.kpi.service.DashboardKpiService;
import com.access.cargo.dashboard.kpi.service.DashboardKpiServiceAkbar;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@CrossOrigin
@Slf4j
@RestController
@Validated
public class DashboardKpiController {
    static final String DASHBOARD = "dashboard";
    static final String SUMMARY = DASHBOARD + "/summary";
    static final String AGENTS = DASHBOARD + "/agents";
    static final String AIRLINES = DASHBOARD + "/airlines";
    static final String AGENTS_CHARTS = DASHBOARD + "/agents/charts";
    static final String AIRLINES_CHARTS = DASHBOARD + "/airlines/charts";
    static final String PERIOD = DASHBOARD + "/period";
    static final String ARCHIEVEMENTTYPE = DASHBOARD + "/achievementType";

    @Autowired
    AccountService accountService;

//    @Autowired
//    DashboardKpiService dashboardKpiService;

    @Autowired
    DashboardKpiServiceAkbar dashboardKpiService;

    @Autowired
    DashboardKpiTableRedisRepo dashboardKpiTableRedisRepo;

    @GetMapping(SUMMARY)
    public ResponseEntity summary(
            @RequestHeader(value = JwtConstants.TOKEN_HEADER) String token
            , @ModelAttribute DashboardKpiParams params
    ) {

        Collection<String> authRoles = new LinkedList<>();
        authRoles.add("ROLE_SUPER_ADMIN");
        authRoles.add("ROLE_REPORT");
        authRoles.add("ROLE_ADMIN");
        authRoles.add("ROLE_REPORT_TONASE");
        authRoles.add("ROLE_ASYST_OPS");

        if (!accountService.isAuthorized(token, authRoles)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        DashboardKpiSummary dashboardKpiSummary = new DashboardKpiSummary();
        String period = params.getPeriod_();
        LocalDate date = LocalDate.now(DateTimeCfg.ZONE_ID);
        if (Objects.equals(period, DashboardKpiConstants.PERIOD_TODAY)) {
            System.out.println("period"+period);
            dashboardKpiSummary = dashboardKpiService.getSummaryRange(token, params.getBranch_(), date, date);
        } else if (Objects.equals(period, DashboardKpiConstants.PERIOD_LAST_WEEK)) {
            LocalDate now = LocalDate.now(DateTimeCfg.ZONE_ID);
            LocalDate date1 = now.minusWeeks(1);
            LocalDate date2 = now;
            dashboardKpiSummary = dashboardKpiService.getSummaryRange(token, params.getBranch_(), date1, date2);
        } else if (Objects.equals(period, DashboardKpiConstants.PERIOD_LAST_MONTH)) {
            LocalDate now = LocalDate.now(DateTimeCfg.ZONE_ID);
            LocalDate date1 = now.minusMonths(1);
            LocalDate date2 = now;
            dashboardKpiSummary = dashboardKpiService.getSummaryRange(token, params.getBranch_(), date1, date2);
        } else if (Objects.equals(period, DashboardKpiConstants.PERIOD_LAST_YEAR)) {

            LocalDate now = LocalDate.now(DateTimeCfg.ZONE_ID);
            LocalDate date1 = now.minusYears(1);
            LocalDate date2 = now;
            dashboardKpiSummary = dashboardKpiService.getSummaryRange(token, params.getBranch_(), date1, date2);
        } else if (Objects.equals(period, DashboardKpiConstants.PERIOD_CUSTOM_DATE)) {
            LocalDate date1 = params.getStartDate_();
            LocalDate date2 = params.getEndDate_();
            dashboardKpiSummary = dashboardKpiService.getSummaryRange(token, params.getBranch_(), date1, date2);

        } else if (Objects.equals(period, DashboardKpiConstants.PERIOD_CUSTOM_MONTH)) {
            Integer month = params.getMonth_();
            LocalDate date3 = LocalDate.now(DateTimeCfg.ZONE_ID);

            YearMonth thisYearMonth = YearMonth.of(date3.getYear(), month);
            LocalDate date1 = LocalDate.of(date3.getYear(), month, 01);
            LocalDate date2 = thisYearMonth.atEndOfMonth();

            dashboardKpiSummary = dashboardKpiService.getSummaryRange(token, params.getBranch_(), date1, date2);

        } else if (Objects.equals(period, DashboardKpiConstants.PERIOD_CUSTOM_YEAR)) {
            Integer year = params.getYear_();
            LocalDate date1 = LocalDate.of(year, 01, 01);
            LocalDate date2 = LocalDate.of(year, 12, 31);

            dashboardKpiSummary = dashboardKpiService.getSummaryRange(token, params.getBranch_(), date1, date2);

        }
        return new ResponseEntity(dashboardKpiSummary, HttpStatus.OK);
    }

    @GetMapping(AGENTS)
    public ResponseEntity agent(
            @RequestHeader(value = JwtConstants.TOKEN_HEADER) String token
            , @ModelAttribute DashboardKpiParams params
    ) {

        Collection<String> authRoles = new LinkedList<>();
        authRoles.add("ROLE_SUPER_ADMIN");
        authRoles.add("ROLE_REPORT");
        authRoles.add("ROLE_ADMIN");
        authRoles.add("ROLE_REPORT_TONASE");
        authRoles.add("ROLE_ASYST_OPS");

        if (!accountService.isAuthorized(token, authRoles)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        String dataJson = null;
        String period = params.getPeriod_();
        LocalDate date = LocalDate.now(DateTimeCfg.ZONE_ID);
        if (Objects.equals(period, DashboardKpiConstants.PERIOD_TODAY)) {
            dataJson = dashboardKpiService.getAgent(params.getBranch_(), date, date);
        } else if (Objects.equals(period, DashboardKpiConstants.PERIOD_CUSTOM_DATE)) {
            LocalDate date1 = params.getStartDate_();
            LocalDate date2 = params.getEndDate_();
            dataJson = dashboardKpiService.getAgent(params.getBranch_(), date1, date2);
        }

        return new ResponseEntity(dataJson, HttpStatus.OK);
    }

    @GetMapping(PERIOD)
    public ResponseEntity period(
            @RequestHeader(value = JwtConstants.TOKEN_HEADER) String token
    ) {
        List<String> dataPeriod = new LinkedList<>();
        dataPeriod.add("PERIOD_TODAY");
        dataPeriod.add("PERIOD_LAST_WEEK");
        dataPeriod.add("PERIOD_LAST_MONTH");
        dataPeriod.add("PERIOD_LAST_YEAR");
        dataPeriod.add("PERIOD_CUSTOM_DATE");
        dataPeriod.add("PERIOD_CUSTOM_MONTH");
        dataPeriod.add("PERIOD_CUSTOM_YEAR");

        return new ResponseEntity(dataPeriod, HttpStatus.OK);
    }

    @GetMapping(ARCHIEVEMENTTYPE)
    public ResponseEntity arcievement(
            @RequestHeader(value = JwtConstants.TOKEN_HEADER) String token
    ) {
        List<String> data = new LinkedList<>();
        data.add("BOOKING");
        data.add("RA");
        data.add("WHO");
        data.add("WHI");
        data.add("WHM");
//        data.add("WHT");

        return new ResponseEntity(data, HttpStatus.OK);
    }

    @GetMapping(AIRLINES)
    public ResponseEntity airlines(
            @RequestHeader(value = JwtConstants.TOKEN_HEADER) String token
            , @ModelAttribute DashboardKpiParams params
    ) {

        Collection<String> authRoles = new LinkedList<>();
        authRoles.add("ROLE_SUPER_ADMIN");
        authRoles.add("ROLE_REPORT");
        authRoles.add("ROLE_ADMIN");
        authRoles.add("ROLE_REPORT_TONASE");
        authRoles.add("ROLE_ASYST_OPS");

        if (!accountService.isAuthorized(token, authRoles)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        String dataJson = null;
        String period = params.getPeriod_();
        LocalDate date = LocalDate.now(DateTimeCfg.ZONE_ID);
        if (Objects.equals(period, DashboardKpiConstants.PERIOD_TODAY)) {
            dataJson = dashboardKpiService.getAirlines(params.getBranch_(), date, date);
        } else if (Objects.equals(period, DashboardKpiConstants.PERIOD_CUSTOM_DATE)) {
            LocalDate date1 = params.getStartDate_();
            LocalDate date2 = params.getEndDate_();
            dataJson = dashboardKpiService.getAirlines(params.getBranch_(), date1, date2);
        }

        return new ResponseEntity(dataJson, HttpStatus.OK);
    }

    @GetMapping(AGENTS_CHARTS)
    public ResponseEntity agentsCharts(
            @RequestHeader(value = JwtConstants.TOKEN_HEADER) String token
            , @ModelAttribute DashboardKpiChartRedis params
    ) {
        Collection<String> authRoles = new LinkedList<>();
        authRoles.add("ROLE_SUPER_ADMIN");
        authRoles.add("ROLE_REPORT");
        authRoles.add("ROLE_ADMIN");
        authRoles.add("ROLE_REPORT_TONASE");
        authRoles.add("ROLE_ASYST_OPS");

        if (!accountService.isAuthorized(token, authRoles)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<DashboardKpiChartRedis> dashboardKpiChartRedis = new LinkedList<>();
        String period = params.getParams().getPeriod_();
        LocalDate date = LocalDate.now(DateTimeCfg.ZONE_ID);
        if (Objects.equals(period, DashboardKpiConstants.PERIOD_LAST_WEEK)) {
            LocalDate now = LocalDate.now(DateTimeCfg.ZONE_ID);
            LocalDate date1 = now.minusWeeks(1);
            LocalDate date2 = now;
            return new ResponseEntity<>(dashboardKpiService.getCarts("Agent", params.getParams().getBranch_(), params.getAchievementType(), "PERIOD_LAST_WEEK", date1, date2), HttpStatus.OK);
        } else if (Objects.equals(period, DashboardKpiConstants.PERIOD_LAST_MONTH)) {
            LocalDate now = LocalDate.now(DateTimeCfg.ZONE_ID);
            LocalDate date1 = now.minusMonths(1);
            LocalDate date2 = now;
            return new ResponseEntity<>(dashboardKpiService.getCarts("Agent", params.getParams().getBranch_(), params.getAchievementType(), "PERIOD_LAST_MONTH", date1, date2), HttpStatus.OK);
        } else if (Objects.equals(period, DashboardKpiConstants.PERIOD_CUSTOM_MONTH)) {

            Integer month = params.getParams().getMonth_();
            LocalDate date3 = LocalDate.now(DateTimeCfg.ZONE_ID);

            YearMonth thisYearMonth = YearMonth.of(date3.getYear(), month);
            LocalDate date1 = LocalDate.of(date3.getYear(), month, 01);
            LocalDate date2 = thisYearMonth.atEndOfMonth();

            return new ResponseEntity<>(dashboardKpiService.getCarts("Agent", params.getParams().getBranch_(), params.getAchievementType(), "PERIOD_CUSTOM_MONTH", date1, date2), HttpStatus.OK);
        } else if (Objects.equals(period, DashboardKpiConstants.PERIOD_LAST_YEAR)) {
            LocalDate now = LocalDate.now(DateTimeCfg.ZONE_ID);
            LocalDate date1 = now.minusYears(1);
            LocalDate date2 = now;
            return new ResponseEntity<>(dashboardKpiService.getCarts("Agent", params.getParams().getBranch_(), params.getAchievementType(), "PERIOD_LAST_YEAR", date1, date2), HttpStatus.OK);

        } else if (Objects.equals(period, DashboardKpiConstants.PERIOD_CUSTOM_YEAR)) {
            Integer year = params.getParams().getYear_();
            LocalDate date1 = LocalDate.of(year, 01, 01);
            LocalDate date2 = LocalDate.of(year, 12, 31);

            return new ResponseEntity<>(dashboardKpiService.getCarts("Agent", params.getParams().getBranch_(), params.getAchievementType(), "PERIOD_CUSTOM_YEAR", date1, date2), HttpStatus.OK);
        }

        return new ResponseEntity(dashboardKpiChartRedis, HttpStatus.OK);
    }

    @GetMapping(AIRLINES_CHARTS)
    public ResponseEntity airlinesCharts(
            @RequestHeader(value = JwtConstants.TOKEN_HEADER) String token
            , @ModelAttribute DashboardKpiChartRedis params
    ) {

        Collection<String> authRoles = new LinkedList<>();
        authRoles.add("ROLE_SUPER_ADMIN");
        authRoles.add("ROLE_REPORT");
        authRoles.add("ROLE_ADMIN");
        authRoles.add("ROLE_REPORT_TONASE");
        authRoles.add("ROLE_ASYST_OPS");

        if (!accountService.isAuthorized(token, authRoles)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<DashboardKpiChartRedis> dashboardKpiChartRedis = new LinkedList<>();
        String period = params.getParams().getPeriod_();
        LocalDate date = LocalDate.now(DateTimeCfg.ZONE_ID);
        if (Objects.equals(period, DashboardKpiConstants.PERIOD_LAST_WEEK)) {
            LocalDate now = LocalDate.now(DateTimeCfg.ZONE_ID);
            LocalDate date1 = now.minusWeeks(1);
            LocalDate date2 = now;
            return new ResponseEntity<>(dashboardKpiService.getCarts("Airlines", params.getParams().getBranch_(), params.getAchievementType(), "PERIOD_LAST_WEEK", date1, date2), HttpStatus.OK);
        } else if (Objects.equals(period, DashboardKpiConstants.PERIOD_LAST_MONTH)) {
            LocalDate now = LocalDate.now(DateTimeCfg.ZONE_ID);
            LocalDate date1 = now.minusMonths(1);
            LocalDate date2 = now;
            return new ResponseEntity<>(dashboardKpiService.getCarts("Airlines", params.getParams().getBranch_(), params.getAchievementType(), "PERIOD_LAST_MONTH", date1, date2), HttpStatus.OK);
        } else if (Objects.equals(period, DashboardKpiConstants.PERIOD_CUSTOM_MONTH)) {

            Integer month = params.getParams().getMonth_();
            LocalDate date3 = LocalDate.now(DateTimeCfg.ZONE_ID);

            YearMonth thisYearMonth = YearMonth.of(date3.getYear(), month);
            LocalDate date1 = LocalDate.of(date3.getYear(), month, 01);
            LocalDate date2 = thisYearMonth.atEndOfMonth();
            return new ResponseEntity<>(dashboardKpiService.getCarts("Airlines", params.getParams().getBranch_(), params.getAchievementType(), "PERIOD_CUSTOM_MONTH", date1, date2), HttpStatus.OK);
        } else if (Objects.equals(period, DashboardKpiConstants.PERIOD_LAST_YEAR)) {
            LocalDate now = LocalDate.now(DateTimeCfg.ZONE_ID);
            LocalDate date1 = now.minusYears(1);
            LocalDate date2 = now;
            return new ResponseEntity<>(dashboardKpiService.getCarts("Airlines", params.getParams().getBranch_(), params.getAchievementType(), "PERIOD_LAST_YEAR", date1, date2), HttpStatus.OK);

        } else if (Objects.equals(period, DashboardKpiConstants.PERIOD_CUSTOM_YEAR)) {
            Integer year = params.getParams().getYear_();
            LocalDate date1 = LocalDate.of(year, 01, 01);
            LocalDate date2 = LocalDate.of(year, 12, 31);

            return new ResponseEntity<>(dashboardKpiService.getCarts("Airlines", params.getParams().getBranch_(), params.getAchievementType(), "PERIOD_CUSTOM_YEAR", date1, date2), HttpStatus.OK);
        }

        return new ResponseEntity(dashboardKpiChartRedis, HttpStatus.OK);
    }



}
