package com.access.cargo.dashboard.kpi.config;


public class JwtConstants {
    public static final String TOKEN_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
}
