package com.access.cargo.dashboard.kpi.service;


import com.access.cargo.dashboard.kpi.model.Entity.AwbLoading;
import com.access.cargo.dashboard.kpi.model.redis.DashboardRevenueReportDataDetailRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardRevenueReportSummaryRedis;
import com.access.cargo.dashboard.kpi.repo.AwbLoadingRepo;
import com.access.cargo.dashboard.kpi.repo.DashboardRevenueReportDataDetailRedisRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DashboardRevenueService {
    @Autowired
    DashboardRevenueReportDataDetailRedisRepo dashboardRevenueReportDataDetailRedisRepo;

    @Autowired
    AwbLoadingRepo awbLoadingRepo;

    public DashboardRevenueReportSummaryRedis getSummary(LocalDate date1,LocalDate date2,String agent,String branch){
        Collection<AwbLoading> awbLoadings;

        LocalDateTime localDateTimeStart = date1.atTime(00, 00, 00).minusSeconds(1);
        LocalDateTime localDateTimeEnd = date2.atTime(23, 59, 59).plusSeconds(1);

        if(agent.equals("ALL")){
            awbLoadings = awbLoadingRepo.findAll().stream().filter(x -> x.getState().equals(1) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());

        }else{
            awbLoadings = awbLoadingRepo.findAll().stream().filter(x -> x.getState().equals(1) && x.getAwb().getAgentName().equals(agent) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());

        }
        Collection<AwbLoading> awbLoadings2 = awbLoadings.stream().filter(distinctByKey(x -> x.getAwb())).collect(Collectors.toList());


        BigDecimal sumPiecesManifest = BigDecimal.valueOf(0);
        BigDecimal sumKgManifest = BigDecimal.valueOf(0);
        BigDecimal sumRevenue= BigDecimal.valueOf(0);

        for (AwbLoading amt : awbLoadings2) {
            sumPiecesManifest = sumPiecesManifest.add(BigDecimal.valueOf(amt.getAwb().getPieces() != null ? amt.getAwb().getPieces():0));
            sumKgManifest = sumKgManifest.add(amt.getAwb().getChargeableWeight() != null ? amt.getAwb().getChargeableWeight():BigDecimal.valueOf(0));
            sumRevenue = sumRevenue.add(amt.getAwb().getRatesTotal() != null ? amt.getAwb().getRatesTotal():BigDecimal.valueOf(0));

        }

        DashboardRevenueReportSummaryRedis dashboardRevenueReportSummaryRedis =  new DashboardRevenueReportSummaryRedis();
        dashboardRevenueReportSummaryRedis.setAwbManifest(BigDecimal.valueOf(awbLoadings2.size()));
        dashboardRevenueReportSummaryRedis.setPiecesManifest(sumPiecesManifest);
        dashboardRevenueReportSummaryRedis.setKgManifest(sumKgManifest);
        dashboardRevenueReportSummaryRedis.setRevenue(sumRevenue);
        return dashboardRevenueReportSummaryRedis;
    }

    public Collection<DashboardRevenueReportDataDetailRedis> getDetail(LocalDate date1,LocalDate date2,String agent,String branch){
        Collection<DashboardRevenueReportDataDetailRedis> dashboardRevenueReportDataDetailRedis;
        Collection<AwbLoading> awbLoadings;

        LocalDateTime localDateTimeStart = date1.atTime(00, 00, 00).minusSeconds(1);
        LocalDateTime localDateTimeEnd = date2.atTime(23, 59, 59).plusSeconds(1);

        if(agent.equals("ALL")){
            awbLoadings = awbLoadingRepo.findAll().stream().filter(x -> x.getState().equals(1) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());

        }else{
            awbLoadings = awbLoadingRepo.findAll().stream().filter(x -> x.getState().equals(1) && x.getAwb().getAgentName().equals(agent) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());

        }

        Collection<AwbLoading> awbLoadings2 = awbLoadings.stream().filter(distinctByKey(x -> x.getAwb())).collect(Collectors.toList());
        dashboardRevenueReportDataDetailRedis = mappingData(awbLoadings2);
        return dashboardRevenueReportDataDetailRedis;
    }
    public Collection<DashboardRevenueReportDataDetailRedis> mappingData(Collection<AwbLoading> awbLoadings){
        Collection<DashboardRevenueReportDataDetailRedis> dashboardRevenueReportDataDetailRedis = new LinkedList<>();
        for(AwbLoading awbLoading:awbLoadings){
            DashboardRevenueReportDataDetailRedis dashboardRevenueReportDataDetailRedis1 = new DashboardRevenueReportDataDetailRedis();
            dashboardRevenueReportDataDetailRedis1.setAgent(awbLoading.getAwb().getAgentName());
            dashboardRevenueReportDataDetailRedis1.setAwb(awbLoading.getAwb().getCode());
            dashboardRevenueReportDataDetailRedis1.setPieces(BigDecimal.valueOf(awbLoading.getPieces()));
            dashboardRevenueReportDataDetailRedis1.setKg(awbLoading.getAwb().getChargeableWeight());
            dashboardRevenueReportDataDetailRedis1.setTransaction("Purchase");
            dashboardRevenueReportDataDetailRedis1.setTransactionTypeCode("C");
            dashboardRevenueReportDataDetailRedis1.setAmount(awbLoading.getAwb().getRatesTotal());
            dashboardRevenueReportDataDetailRedis1.setDate(awbLoading.getAwb().getFlightDate());
            dashboardRevenueReportDataDetailRedis.add(dashboardRevenueReportDataDetailRedis1);
        }

        return dashboardRevenueReportDataDetailRedis;
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }


}