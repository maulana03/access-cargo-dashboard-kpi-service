package com.access.cargo.dashboard.kpi.model.wrapper.tes;

import lombok.NoArgsConstructor;

@NoArgsConstructor
@lombok.Data
public class DataGrafiks {
    public String id;
    public String label;
    public String pattern;
    public String type;
    public String role;


}