package com.access.cargo.dashboard.kpi.service;


import com.access.cargo.dashboard.kpi.model.Entity.WhInInvoice;
import com.access.cargo.dashboard.kpi.model.redis.DashboardDeliveryReportDataDetailRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardDeliveryReportSummaryRedis;
import com.access.cargo.dashboard.kpi.repo.DashboardDeliveryReportDataDetailRedisRepo;
import com.access.cargo.dashboard.kpi.repo.WhInvoiceRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.LinkedList;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DashboardDeliveryService {
    @Autowired
    DashboardDeliveryReportDataDetailRedisRepo dashboardDeliveryReportDataDetailRedisRepo;

    @Autowired
    WhInvoiceRepo whInvoiceRepo;
    public DashboardDeliveryReportSummaryRedis getSummary(LocalDate date1,LocalDate date2,String agent,String branch){
        LocalDateTime localDateTime1 = date1.atTime(00,00, 00);
        LocalDateTime localDateTime2 = date2.atTime(23,59, 59);
        Collection<WhInInvoice> whInInvoices;

        if(agent.equals("ALL")){
            whInInvoices = whInvoiceRepo.findAll().stream().filter(x -> x.getModifiedDateTime().isAfter(localDateTime1) && x.getModifiedDateTime().isBefore(localDateTime2)).collect(Collectors.toList());

        }else{
            whInInvoices = whInvoiceRepo.findAll().stream().filter(x -> x.getAgent().equals(agent) && x.getModifiedDateTime().isAfter(localDateTime1) && x.getModifiedDateTime().isBefore(localDateTime2)).collect(Collectors.toList());

        }

        BigDecimal sumPiecesDelivery = BigDecimal.valueOf(0);
        BigDecimal sumKgDelivery = BigDecimal.valueOf(0);
        BigDecimal sumDelivery= BigDecimal.valueOf(0);

        for (WhInInvoice amt : whInInvoices) {
            sumPiecesDelivery = sumPiecesDelivery.add(BigDecimal.valueOf(amt.getPieces()));
            sumKgDelivery = sumKgDelivery.add(amt.getChargeableWeight());
            sumDelivery = sumDelivery.add(amt.getRates());

        }

        DashboardDeliveryReportSummaryRedis dashboardDeliveryReportSummaryRedis =  new DashboardDeliveryReportSummaryRedis();
        dashboardDeliveryReportSummaryRedis.setAwbDelivery(BigDecimal.valueOf(whInInvoices.size()));
        dashboardDeliveryReportSummaryRedis.setPiecesDelivery(sumPiecesDelivery);
        dashboardDeliveryReportSummaryRedis.setKgDelivery(sumKgDelivery);
        dashboardDeliveryReportSummaryRedis.setRevenue(sumDelivery);
        return dashboardDeliveryReportSummaryRedis;
    }

    public Collection<DashboardDeliveryReportDataDetailRedis> getDetail(LocalDate date1,LocalDate date2,String agent,String branch){
        LocalDateTime localDateTime1 = date1.atTime(00,00, 00);
        LocalDateTime localDateTime2 = date2.atTime(23,59, 59);
        Collection<DashboardDeliveryReportDataDetailRedis> dashboardDeliveryReportDataDetailRedis;
        Collection<WhInInvoice> whInInvoices;

        if(agent.equals("ALL")){
            whInInvoices = whInvoiceRepo.findAll().stream().filter(x -> x.getModifiedDateTime().isAfter(localDateTime1) && x.getModifiedDateTime().isBefore(localDateTime2)).collect(Collectors.toList());

        }else{
            whInInvoices = whInvoiceRepo.findAll().stream().filter(x -> x.getAgent().equals(agent) && x.getModifiedDateTime().isAfter(localDateTime1) && x.getModifiedDateTime().isBefore(localDateTime2)).collect(Collectors.toList());

        }
        dashboardDeliveryReportDataDetailRedis = mappingData(whInInvoices);
        return dashboardDeliveryReportDataDetailRedis;
    }

    public Collection<DashboardDeliveryReportDataDetailRedis> mappingData(Collection<WhInInvoice> whInInvoices){
        Collection<DashboardDeliveryReportDataDetailRedis> dashboardDeliveryReportDataDetailRedis = new LinkedList<>();

        for(WhInInvoice whInInvoice : whInInvoices){
            DashboardDeliveryReportDataDetailRedis dashboardDeliveryReportDataDetailRedis1 = new DashboardDeliveryReportDataDetailRedis();
            dashboardDeliveryReportDataDetailRedis1.setAwb(whInInvoice.getAwb());
            dashboardDeliveryReportDataDetailRedis1.setAgent(whInInvoice.getAgent());
            dashboardDeliveryReportDataDetailRedis1.setPieces(BigDecimal.valueOf(whInInvoice.getPieces()));
            dashboardDeliveryReportDataDetailRedis1.setKg(whInInvoice.getChargeableWeight());
            dashboardDeliveryReportDataDetailRedis1.setDuration(String.valueOf(whInInvoice.getDays()));
            dashboardDeliveryReportDataDetailRedis1.setAmount(whInInvoice.getAmount());
            dashboardDeliveryReportDataDetailRedis1.setDate(whInInvoice.getModifiedDateTime());
            dashboardDeliveryReportDataDetailRedis.add(dashboardDeliveryReportDataDetailRedis1);
        }
        return dashboardDeliveryReportDataDetailRedis;
    }

}