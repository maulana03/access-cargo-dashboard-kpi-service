package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.Entity.AwbManManual;
import com.access.cargo.dashboard.kpi.model.Entity.WhInInvoice;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;

@Repository
public interface AwbManManualRepo extends PagingAndSortingRepository<AwbManManual, Long> {

    boolean existsByCodeAndStatus(String code,Integer status);
    boolean existsByIdAndStatus(Long id,Integer status);
    AwbManManual findFirstByCodeAndStatus(String code,Integer status);
    AwbManManual findFirstByIdAndStatus(Long id,Integer status);

    Page<AwbManManual> findByArrivalDateGreaterThanEqualAndArrivalDateLessThanEqualAndDepartureDateGreaterThanEqualAndDepartureDateLessThanEqualAndAgentNameAndAirlineAndCodeAndFlightNoAndStatus(LocalDate arrivalDate, LocalDate arrivalDate2, LocalDate departureDate, LocalDate departureDate2, String agent, String airline, String awb, String flightNo, Integer status,Pageable pageable);
    Page<AwbManManual> findAllByArrivalDateGreaterThanEqualAndArrivalDateLessThanEqualAndDepartureDateGreaterThanEqualAndDepartureDateLessThanEqualAndAgentNameAndStatus(LocalDate arrivalDate, LocalDate arrivalDate2, LocalDate departureDate, LocalDate departureDate2, String agent, Integer status,Pageable pageable);
    Page<AwbManManual> findAllByArrivalDateGreaterThanEqualAndArrivalDateLessThanEqualAndDepartureDateGreaterThanEqualAndDepartureDateLessThanEqualAndAirlineAndStatus(LocalDate arrivalDate,LocalDate arrivalDate2,LocalDate departureDate,LocalDate departureDate2, String airline, Integer statu,Pageable pageable);
    Page<AwbManManual> findByArrivalDateGreaterThanEqualAndArrivalDateLessThanEqualAndDepartureDateGreaterThanEqualAndDepartureDateLessThanEqualAndCodeAndStatus(LocalDate arrivalDate,LocalDate arrivalDate2,LocalDate departureDate,LocalDate departureDate2, String awb, Integer status,Pageable pageable);
    Page<AwbManManual> findAllByArrivalDateGreaterThanEqualAndArrivalDateLessThanEqualAndDepartureDateGreaterThanEqualAndDepartureDateLessThanEqualAndFlightNoAndStatus(LocalDate arrivalDate,LocalDate arrivalDate2,LocalDate departureDate,LocalDate departureDate2, String flightNo, Integer status,Pageable pageable);

    Page<AwbManManual> findByArrivalDateGreaterThanEqualAndArrivalDateLessThanEqualAndAgentNameAndAirlineAndCodeAndFlightNoAndStatus(LocalDate arrivalDate,LocalDate arrivalDate2, String agent, String airline, String awb, String flightNo, Integer status,Pageable pageable);

    Page<AwbManManual> findAllByArrivalDateGreaterThanEqualAndArrivalDateLessThanEqualAndAgentNameAndStatus(LocalDate arrivalDate,LocalDate arrivalDate2, String agent, Integer status,Pageable pageable);
    Page<AwbManManual> findAllByArrivalDateGreaterThanEqualAndArrivalDateLessThanEqualAndAirlineAndStatus(LocalDate arrivalDate,LocalDate arrivalDate2, String airline, Integer status,Pageable pageable);
    Page<AwbManManual> findByArrivalDateGreaterThanEqualAndArrivalDateLessThanEqualAndCodeAndStatus(LocalDate arrivalDate,LocalDate arrivalDate2,String awb, Integer status,Pageable pageable);
    Page<AwbManManual> findAllByArrivalDateGreaterThanEqualAndArrivalDateLessThanEqualAndFlightNoAndStatus(LocalDate arrivalDate,LocalDate arrivalDate2, String flightNo, Integer status,Pageable pageable);

    Page<AwbManManual> findByDepartureDateGreaterThanEqualAndDepartureDateLessThanEqualAndAgentNameAndAirlineAndCodeAndFlightNoAndStatus(LocalDate departureDate,LocalDate departureDate2, String agent, String airline, String awb, String flightNo, Integer status,Pageable pageable);

    Page<AwbManManual> findAllByDepartureDateGreaterThanEqualAndDepartureDateLessThanEqualAndAgentNameAndStatus(LocalDate departureDate,LocalDate departureDate2, String agent, Integer status,Pageable pageable);
    Page<AwbManManual> findAllByDepartureDateGreaterThanEqualAndDepartureDateLessThanEqualAndAirlineAndStatus(LocalDate departureDate,LocalDate departureDate2, String airline, Integer status,Pageable pageable);
    Page<AwbManManual> findByDepartureDateGreaterThanEqualAndDepartureDateLessThanEqualAndCodeAndStatus(LocalDate departureDate,LocalDate departureDate2, String awb, Integer status,Pageable pageable);
    Page<AwbManManual> findAllByDepartureDateGreaterThanEqualAndDepartureDateLessThanEqualAndFlightNoAndStatus(LocalDate departureDate,LocalDate departureDate2, String flightNo, Integer status,Pageable pageable);


    Page<AwbManManual> findAllByArrivalDateGreaterThanEqualAndArrivalDateLessThanEqualAndDepartureDateGreaterThanEqualAndDepartureDateLessThanEqualAndStatus(LocalDate arrivalDate, LocalDate arrivalDate2, LocalDate departureDate, LocalDate departureDate2, Integer status,Pageable pageable);
    Page<AwbManManual> findAllByArrivalDateGreaterThanEqualAndArrivalDateLessThanEqualAndStatus(LocalDate arrivalDate, LocalDate arrivalDate2, Integer status,Pageable pageable);
    Page<AwbManManual> findAllByDepartureDateGreaterThanEqualAndDepartureDateLessThanEqualAndStatus(LocalDate departureDate, LocalDate departureDate2, Integer status,Pageable pageable);
    Page<AwbManManual> findAllByCodeAndStatus(String awb, Integer status,Pageable pageable);
    Page<AwbManManual> findAllByAgentNameAndStatus(String agentName, Integer status,Pageable pageable);
    Page<AwbManManual> findAllByAirlineAndStatus(String airline, Integer status,Pageable pageable);
    Page<AwbManManual> findAllByFlightNoAndStatus(String flightno, Integer status,Pageable pageable);
    Page<AwbManManual> findAllByStatus(Integer status,Pageable pageable);
    Collection<AwbManManual> findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(LocalDateTime dateTime1, LocalDateTime dateTime2, Integer status);



}
