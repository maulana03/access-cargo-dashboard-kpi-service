package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@RedisHash("AgentTransactionRedis")
public class AgentAccountTransactionRedis implements Serializable {

    private static final long serialVersionUID = -6633528012019040773L;
    @Id
    Long id;

    String currency;
    BigDecimal amount;
    String remarks;
    String reference;

    Long agentAccountId;
    String agentAccountNumber;

    Long transactionTypeId;
    String transactionTypeCode;

    BigDecimal currentBalance;
    BigDecimal currentCashBalance;
    BigDecimal currentCreditBalance;
    /**/

    Integer status;
    String statusDesc;

    Long createdById;
    String createdByName;
    LocalDateTime createdDateTime;

    Long modifiedById;
    String modifiedByName;
    LocalDateTime modifiedDateTime;

    String balanceType;
    String agentName;
    String code;

}
