package com.access.cargo.dashboard.kpi.model.Entity;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@NoArgsConstructor
@Data
@Entity
@Table(name = "gerobak")
public class Gerobak implements Serializable {

    private static final long serialVersionUID = -7006245986116128843L;
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "code")
    private String code;

    public Gerobak(Long id) {
        this.id = id;
    }
}
