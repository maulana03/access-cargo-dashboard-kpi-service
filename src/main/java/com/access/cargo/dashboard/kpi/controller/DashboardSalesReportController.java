package com.access.cargo.dashboard.kpi.controller;


import com.access.cargo.dashboard.kpi.config.JwtConstants;
import com.access.cargo.dashboard.kpi.model.redis.DashboardReportParams;
import com.access.cargo.dashboard.kpi.service.AccountService;
import com.access.cargo.dashboard.kpi.service.DashboardSalesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.LinkedList;

@CrossOrigin
@Slf4j
@RestController
@Validated
public class DashboardSalesReportController {
    static final String SALES_REPORT = "sales/dashboardKpi";
    static final String SALES_REPORT_SUMMARY = SALES_REPORT + "/summary";
    static final String SALES_REPORT_DETAIL = SALES_REPORT + "/detail";

    @Autowired
    DashboardSalesService dashboardSalesService;

    @Autowired
    AccountService accountService;

    @GetMapping(SALES_REPORT_SUMMARY)
    public ResponseEntity getSumarySalesReport(
            @RequestHeader(value = JwtConstants.TOKEN_HEADER) String token
            , @ModelAttribute DashboardReportParams params) {
        Collection<String> authRoles = new LinkedList<>();
        authRoles.add("ROLE_SUPER_ADMIN");
        authRoles.add("ROLE_ADMIN");
        authRoles.add("ROLE_REPORT");
        authRoles.add("ROLE_REPORT_TONASE");
        authRoles.add("ROLE_ASYST_OPS");

        if (!accountService.isAuthorized(token, authRoles)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(dashboardSalesService.getSummary(params.getStartDate_(),params.getEndDate_(),params.getAgent_(),params.getBranch_()), HttpStatus.OK);

    }

    @GetMapping(SALES_REPORT_DETAIL)
    public ResponseEntity getDetailSalesReport(
            @RequestHeader(value = JwtConstants.TOKEN_HEADER) String token
            , @ModelAttribute DashboardReportParams params) {
        Collection<String> authRoles = new LinkedList<>();
        authRoles.add("ROLE_SUPER_ADMIN");
        authRoles.add("ROLE_ADMIN");
        authRoles.add("ROLE_REPORT");
        authRoles.add("ROLE_REPORT_TONASE");
        authRoles.add("ROLE_ASYST_OPS");

        if (!accountService.isAuthorized(token, authRoles)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(dashboardSalesService.getDetail(params.getStartDate_(),params.getEndDate_(),params.getAgent_(),params.getBranch_()), HttpStatus.OK);

    }
}
