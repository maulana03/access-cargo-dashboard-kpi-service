package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@RedisHash("DashboardDeliveryReportSummaryRedis")
public class DashboardDeliveryReportSummaryRedis implements Serializable {
    private static final long serialVersionUID = 3183247516666702115L;
    @Id
    Long id;

    BigDecimal awbDelivery;
    BigDecimal piecesDelivery;
    BigDecimal kgDelivery;
    BigDecimal revenue;
}
