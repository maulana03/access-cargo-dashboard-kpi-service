package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@RedisHash("DashboardKpiChartResp")
public class DashboardKpiChartDataRedis implements Serializable {

    private static final long serialVersionUID = -5620366421863319296L;
    String name; // JNE,TIKI
    String label; // 1,2 -> date, month, or day
    BigDecimal value;

}
