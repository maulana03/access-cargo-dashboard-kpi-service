package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.redis.DashboardPreManifestReportDataDetailRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardRegulatedAgentReportDataDetailRedis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DashboardPreManifestReportDataDetailRedisRepo extends JpaRepository<DashboardPreManifestReportDataDetailRedis, String> {
}
