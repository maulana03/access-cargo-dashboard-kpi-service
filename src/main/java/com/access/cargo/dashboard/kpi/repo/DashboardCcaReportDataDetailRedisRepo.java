package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.redis.DashboardCcaReportDataDetailRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardRevenueReportDataDetailRedis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DashboardCcaReportDataDetailRedisRepo extends JpaRepository<DashboardCcaReportDataDetailRedis, String> {
}
