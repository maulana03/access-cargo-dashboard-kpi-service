package com.access.cargo.dashboard.kpi.feign;

import com.access.cargo.dashboard.kpi.config.JwtConstants;
import com.access.cargo.dashboard.kpi.model.redis.AgentAccountTransactionRedis;
import com.access.cargo.dashboard.kpi.model.wrapper.AccountUserResp;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.Collection;

@FeignClient(name = "access-cargo-agent-account-service")
@RibbonClient(name = "access-cargo-agent-account-service")
public interface AgentAccountClient {

    String GET_AGENT_TRANSACTION_BY_PARAMS = "access-cargo-agent-account-service/transactions";

    @GetMapping(GET_AGENT_TRANSACTION_BY_PARAMS)
    ResponseEntity<Collection<AgentAccountTransactionRedis>> getAgentAccountByparams(
            @RequestHeader(value = JwtConstants.TOKEN_HEADER) String token
            ,@RequestParam(value = "agentId", required = false) Long agentId
            ,@RequestParam(value = "date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date
            ,@RequestParam(value = "date2") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date2);

//    @GetMapping(GET_AGENT_TRANSACTION_BY_PARAMS)
//    ResponseEntity<Collection<AgentAccountTransactionRedis>> getAgentAccountByparams(
//            @RequestHeader(value = JwtConstants.TOKEN_HEADER) String token
//            ,@RequestParam(value = "size", required = false) Integer size
//            ,@RequestParam(value = "agentId", required = false) Long agentId
//            ,@RequestParam(value = "date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date
//            ,@RequestParam(value = "date2") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date2);

}

