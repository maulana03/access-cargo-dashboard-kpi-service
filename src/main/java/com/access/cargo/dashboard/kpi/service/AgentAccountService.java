package com.access.cargo.dashboard.kpi.service;

import com.access.cargo.dashboard.kpi.feign.AgentAccountClient;
import com.access.cargo.dashboard.kpi.model.redis.AgentAccountTransactionRedis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Collection;

@Service
public class AgentAccountService {

    @Autowired
    AgentAccountClient agentAccountClient;


    public Collection<AgentAccountTransactionRedis> getAgentAccountByParams(String token, Long agentId, LocalDate date1, LocalDate date2, Pageable page) {
        return agentAccountClient.getAgentAccountByparams(token,agentId,date1,date2).getBody();
    }
}
