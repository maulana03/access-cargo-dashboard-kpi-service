package com.access.cargo.dashboard.kpi.model;

import java.io.Serializable;

public class DashboardKpiConstants implements Serializable {

    public static final String PERIOD_TODAY = "PERIOD_TODAY";
    public static final String PERIOD_LAST_WEEK = "PERIOD_LAST_WEEK";
    public static final String PERIOD_LAST_MONTH = "PERIOD_LAST_MONTH";
    public static final String PERIOD_LAST_YEAR = "PERIOD_LAST_YEAR";
    public static final String PERIOD_CUSTOM_DATE = "PERIOD_CUSTOM_DATE";
    public static final String PERIOD_CUSTOM_MONTH = "PERIOD_CUSTOM_MONTH";
    public static final String PERIOD_CUSTOM_YEAR = "PERIOD_CUSTOM_YEAR";

    private static final long serialVersionUID = 4342237102950032245L;
}
