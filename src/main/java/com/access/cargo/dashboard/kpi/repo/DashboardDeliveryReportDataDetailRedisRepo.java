package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.redis.DashboardDeliveryReportDataDetailRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardRevenueReportDataDetailRedis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DashboardDeliveryReportDataDetailRedisRepo extends JpaRepository<DashboardDeliveryReportDataDetailRedis, String> {
}
