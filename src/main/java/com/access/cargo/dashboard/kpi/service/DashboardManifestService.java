package com.access.cargo.dashboard.kpi.service;


import com.access.cargo.dashboard.kpi.model.Entity.*;
import com.access.cargo.dashboard.kpi.model.redis.DashboardManifestReportDataDetailRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardManifestReportSummaryRedis;
import com.access.cargo.dashboard.kpi.repo.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DashboardManifestService {
    @Autowired
    DashboardManifestReportDataDetailRedisRepo dashboardManifestReportDataDetailRedisRepo;

    @Autowired
    AwbBuildUpRepo awbBuildUpRepo1;

    @Autowired
    AwbLoadingRepo awbLoadingRepo;

    @Autowired
    AwbBuildUpRepo awbBuildUpRepo;

    @Autowired
    OffLoadingRepo offLoadingRepo;

    @Autowired
    AwbKoliBuildUpRepo awbKoliBuildUpRepo;


    public DashboardManifestReportSummaryRedis getSummary(LocalDate dt1,LocalDate dt2,String agent,String branch){
        Collection<AwbLoading> awbLoadings = new LinkedList<>();
        LocalDateTime localDateTimeStart = dt1.atTime(00,00, 00);
        LocalDateTime localDateTimeEnd = dt2.atTime(23,59, 59);


        if (agent.equals("ALL")) {
            awbLoadings = awbLoadingRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndState(localDateTimeStart, localDateTimeEnd, 1);

        } else {
          awbLoadings = awbLoadingRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStateAndAwb_AgentName(localDateTimeStart, localDateTimeEnd, 1,agent);

        }

        awbLoadings.stream().filter(distinctByKey(x -> x.getAwb())).collect(Collectors.toList());

        List<AwbLoading> awbLoadings2 = awbLoadings.stream().filter(distinctByKey(x -> x.getAwb())).collect(Collectors.toList());


        int flightCount = awbLoadings2.stream()
                .collect(Collectors.groupingBy(AwbLoading::getFlightSch, Collectors.counting())).size();

        /*todo:
         *   loading partial dan total/full pieces
         *   partial: harus ada perhitungan pro-rate weight
         *       total 10 koli, yang diloading 8 koli, chargeableweight = 100KG
         *       weight prorate = 8/10 * 100 = 80KG
         *
         * */
        Integer sumTotalPieces = 0;
        BigDecimal sumTotalKg = BigDecimal.ZERO;
        for (AwbLoading awbLoading : awbLoadings) {
            Awb awbTemp = awbLoading.getAwb();
            int piecesTotal = awbTemp.getPieces();
            BigDecimal chWeight = awbTemp.getChargeableWeight();
            int piecesLoading = awbLoading.getPieces();
            BigDecimal chWeightLoading = BigDecimal.valueOf(piecesLoading).divide(BigDecimal.valueOf(piecesTotal), 3, RoundingMode.HALF_UP).multiply(chWeight);

            sumTotalPieces = Integer.sum(sumTotalPieces, piecesLoading);
            sumTotalKg = sumTotalKg.add(chWeightLoading);
        }


        DashboardManifestReportSummaryRedis dashboardManifestReportSummaryRedis = new DashboardManifestReportSummaryRedis();
        dashboardManifestReportSummaryRedis.setTotalFlight(flightCount);
        dashboardManifestReportSummaryRedis.setTotalAwb(BigDecimal.valueOf(awbLoadings2.size()));
        dashboardManifestReportSummaryRedis.setTotalPieces(sumTotalPieces);
        dashboardManifestReportSummaryRedis.setTotalKg(sumTotalKg);
        return dashboardManifestReportSummaryRedis;
    }

    public Collection<DashboardManifestReportDataDetailRedis> getDetail(LocalDate dt1,LocalDate dt2,String agent,String branch){
        Collection<DashboardManifestReportDataDetailRedis> dashboardManifestReportDataDetailRedis;
        List<AwbBuildUp> awbBuildUps = new LinkedList<>();
        List<AwbLoading> awbLoadings = new LinkedList<>();
        List<AwbOffLoading> awbOffLoadings = new LinkedList<>();
        LocalDateTime localDateTimeStart = dt1.atTime(00,00, 00);
        LocalDateTime localDateTimeEnd = dt2.atTime(23,59, 59);

        Collection<AwbLoading> awbLoadingsTmp = new LinkedList<>();
        Collection<AwbBuildUp> awbBuildUpsTmp = new LinkedList<>();
        Collection<AwbOffLoading> awbOffLoadingsTmp = new LinkedList<>();
        if (agent.equals("ALL")) {
            awbBuildUpsTmp = awbBuildUpRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndState(localDateTimeStart, localDateTimeEnd, 1);
            awbBuildUps = awbBuildUpsTmp.stream().collect(Collectors.toList());
            awbLoadingsTmp = awbLoadingRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndState(localDateTimeStart, localDateTimeEnd, 1);
            awbLoadings = awbLoadingsTmp.stream().collect(Collectors.toList());
            awbOffLoadingsTmp = offLoadingRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndState(localDateTimeStart, localDateTimeEnd, 1);
            awbOffLoadings = awbOffLoadingsTmp.stream().collect(Collectors.toList());

        } else {
            awbLoadingsTmp = awbLoadingRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStateAndAwb_AgentName(localDateTimeStart, localDateTimeEnd, 1,agent);
            awbLoadings = awbLoadingsTmp.stream().collect(Collectors.toList());
            awbBuildUpsTmp = awbBuildUpRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStateAndAwb_AgentName(localDateTimeStart, localDateTimeEnd, 1,agent);
            awbBuildUps = awbBuildUpsTmp.stream().collect(Collectors.toList());
            awbOffLoadingsTmp = offLoadingRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStateAndAwb_AgentName(localDateTimeStart, localDateTimeEnd, 1,agent);
            awbOffLoadings = awbOffLoadingsTmp.stream().collect(Collectors.toList());

        }

        awbLoadings.stream().filter(distinctByKey(x -> x.getAwb().getCode())).collect(Collectors.toList());


        awbBuildUps.sort(Comparator.comparing(o -> o.getCreatedDateTime()));
        awbLoadings.sort(Comparator.comparing(o -> o.getCreatedDateTime()));
        awbOffLoadings.sort(Comparator.comparing(o -> o.getCreatedDateTime()));
        dashboardManifestReportDataDetailRedis =  mappingData(awbBuildUps,awbLoadings,awbOffLoadings);
        return dashboardManifestReportDataDetailRedis;
    }

    public Collection<DashboardManifestReportDataDetailRedis> mappingData(List<AwbBuildUp> awbBuildUps, Collection<AwbLoading> awbLoadings, List<AwbOffLoading> awbOffLoadings){
        Collection<DashboardManifestReportDataDetailRedis> dashboardManifestReportDataDetailRedis = new LinkedList<>();

        Map<FlightList, Long> countedAwb = awbLoadings.stream()
                .collect(Collectors.groupingBy(AwbLoading::getFlightSch, Collectors.counting()));

        for (Map.Entry<FlightList, Long> entry : countedAwb.entrySet()) {
            FlightList flightList = entry.getKey();
            Integer sumTotalPieces = 0;
            BigDecimal sumTotalKg= BigDecimal.ZERO;

            int sumawb=0;
            for(AwbBuildUp awbBuildUp : awbBuildUps){
                if(flightList.getId().equals(awbBuildUp.getFlightSch().getId())){
                    sumTotalPieces = Integer.sum(sumTotalPieces,awbBuildUp.getPieces() != null ? awbBuildUp.getPieces():0);
                    sumTotalKg = sumTotalKg.add(awbBuildUp.getWeight() != null ? awbBuildUp.getWeight():BigDecimal.ZERO);
                    sumawb++;
                }
            }

            //get lass transaction
            List<AwbLoading> awbLoadings1 = awbLoadings.stream().filter(x ->x.getFlightSch().getId().equals(flightList.getId())).collect(Collectors.toList());
            int size = awbLoadings1.size();
            AwbLoading lastTransaction = awbLoadings1.get(size-1);


            int sumawbMan=0;
            Integer sumTotalPiecesMan = 0;
            BigDecimal sumTotalKgMan= BigDecimal.ZERO;
            for(AwbLoading awbLoading : awbLoadings){
                if(flightList.getId().equals(awbLoading.getFlightSch().getId())){
                    Awb awbTemp = awbLoading.getAwb();
                    int piecesTotal = awbTemp.getPieces();
                    BigDecimal chWeight = awbTemp.getChargeableWeight();
                    int piecesLoading = awbLoading.getPieces();
                    BigDecimal chWeightLoading = BigDecimal.valueOf(piecesLoading).divide(BigDecimal.valueOf(piecesTotal), 3, RoundingMode.HALF_UP).multiply(chWeight);

//                    sumTotalPieces = Integer.sum(sumTotalPieces, piecesLoading);
//                    sumTotalKg = sumTotalKg.add(chWeightLoading);


                    sumTotalPiecesMan = Integer.sum(sumTotalPiecesMan, piecesLoading);//Integer.sum(sumTotalPiecesMan,awbLoading.getPieces() != null ? awbLoading.getPieces():0);
                    sumTotalKgMan = sumTotalKgMan.add(chWeightLoading);//sumTotalKgMan.add(awbLoading.getWeight() != null ? awbLoading.getWeight():BigDecimal.ZERO);
                    sumawbMan++;
                }
            }

            int sumawbLoa=0;
            Integer sumTotalPiecesLoa = 0;
            BigDecimal sumTotalKgLoa= BigDecimal.ZERO;
            for(AwbOffLoading awbOffLoading : awbOffLoadings){
                if(flightList.getId().equals(awbOffLoading.getFlightSch().getId())){
                    sumTotalPiecesLoa = Integer.sum(sumTotalPiecesLoa,awbOffLoading.getPieces() != null ? awbOffLoading.getPieces():0);
                    sumTotalKgLoa = sumTotalKgLoa.add(awbOffLoading.getAwb().getChargeableWeight()!= null ? awbOffLoading.getAwb().getChargeableWeight():BigDecimal.ZERO);
                    sumawbLoa++;
                }
            }

            DashboardManifestReportDataDetailRedis dashboardManifestReportDataDetailRedis1 = new DashboardManifestReportDataDetailRedis();
            dashboardManifestReportDataDetailRedis1.setPreManAwb(sumawb);
            dashboardManifestReportDataDetailRedis1.setPreManPieces(sumTotalPieces);
            dashboardManifestReportDataDetailRedis1.setPreManKg(sumTotalKg);

            dashboardManifestReportDataDetailRedis1.setManifestTime(lastTransaction.getCreatedDateTime());

            dashboardManifestReportDataDetailRedis1.setManifestAwb(sumawbMan);
            dashboardManifestReportDataDetailRedis1.setManifestPieces(sumTotalPiecesMan);
            dashboardManifestReportDataDetailRedis1.setManifestKg(sumTotalKgMan);

            dashboardManifestReportDataDetailRedis1.setOffLoadAwb(sumawbLoa);
            dashboardManifestReportDataDetailRedis1.setOffLoadPieces(sumTotalPiecesLoa);
            dashboardManifestReportDataDetailRedis1.setOffLoadKg(sumTotalKgLoa);


            AwbLoading data = awbLoadings.stream()
                    .filter(x -> x.getFlightSch().getId().compareTo(flightList.getId()) == 0)
                    .findFirst().orElse(null);
            dashboardManifestReportDataDetailRedis1.setFlight(data.getAwb().getAirline()+data.getAwb().getFlightNo());
            dashboardManifestReportDataDetailRedis1.setFlightNo(data.getAwb().getFlightNo());
            dashboardManifestReportDataDetailRedis1.setFlightId(data.getFlightSch().getId());
            dashboardManifestReportDataDetailRedis1.setAirlineCode(data.getAwb().getAirline());
            dashboardManifestReportDataDetailRedis1.setRoute(flightList.getSector());



            dashboardManifestReportDataDetailRedis.add(dashboardManifestReportDataDetailRedis1);
        }



       /* Map<FlightList, Long> countedAwb = awbLoadings.stream()
                .collect(Collectors.groupingBy(AwbLoading::getFlightSch, Collectors.counting()));

        for (Map.Entry<FlightList, Long> entry : countedAwb.entrySet()) {
                FlightList flightList = entry.getKey();
            System.out.println(flightList);
            System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

                //manifest
            Integer sumTotalPieces = 0;
            BigDecimal sumTotalKg= BigDecimal.ZERO;

            for(AwbLoading awbLoading : awbLoadings){
                    if(flightList.getId().equals(awbLoading.getFlightSch().getId())){
                        sumTotalPieces = Integer.sum(sumTotalPieces,awbLoading.getPieces() != null ? awbLoading.getPieces():0);
                        sumTotalKg = sumTotalKg.add(awbLoading.getWeight() != null ? awbLoading.getWeight():BigDecimal.ZERO);

                    }
            }

            DashboardManifestReportDataDetailRedis dashboardManifestReportDataDetailRedis1 = new DashboardManifestReportDataDetailRedis();
            dashboardManifestReportDataDetailRedis1.setManifestAwb(awbLoadings.size());
            dashboardManifestReportDataDetailRedis1.setManifestPieces(sumTotalPieces);
            dashboardManifestReportDataDetailRedis1.setManifestKg(sumTotalKg);


            dashboardManifestReportDataDetailRedis.add(dashboardManifestReportDataDetailRedis1);
        }*/

        return dashboardManifestReportDataDetailRedis;
    }


    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }


}