package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class DashboardAwbMilestoneRedis implements Serializable {
    String event;
    String description;
    LocalDateTime date;
    String station;
    String flightNo;
    LocalDate flightDate;
    BigDecimal pieces;
    BigDecimal kg;
    String volume;
}
