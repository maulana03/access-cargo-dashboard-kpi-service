package com.access.cargo.dashboard.kpi.service;


import com.access.cargo.dashboard.kpi.config.JwtConstants;
import com.access.cargo.dashboard.kpi.feign.AccountClient;
import com.access.cargo.dashboard.kpi.model.Entity.AccountUser;
import com.access.cargo.dashboard.kpi.model.wrapper.AccountUserResp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.Collection;

@Service
public class AccountService {

    @Autowired
    AccountClient accountClient;

    public ResponseEntity authenticate(String token) {
        return accountClient.authenticate(token);
    }

    public AccountUserResp getUsersByUser(String token) {
        return accountClient.getUsersByUser(token).getBody();
    }

    public ResponseEntity<Collection<String>> getUsersByUserRoles(@RequestHeader(value = JwtConstants.TOKEN_HEADER) String token) {
        return accountClient.getUsersByUserRoles(token);
    }

    public AccountUser getAccountUserByToken(String token) {
        AccountUserResp accountUserResp = getUsersByUser(token);
        AccountUser accountUser = new AccountUser();
        accountUser.setId(accountUserResp.getId());
        accountUser.setName(accountUserResp.getName());
        return accountUser;


    }

    public boolean isAuthorized(String token, Collection<String> authRoles) {
        ResponseEntity<Collection<String>> collectionResponseEntity = accountClient.getUsersByUserRoles(token);
        Collection<String> myRoles = collectionResponseEntity.getBody();

        boolean state = authRoles.stream()
                .filter(authRole -> myRoles.stream().anyMatch(myRole -> myRole.equals(authRole)))
                .findAny()
                .isPresent();

        return state;
    }
}
