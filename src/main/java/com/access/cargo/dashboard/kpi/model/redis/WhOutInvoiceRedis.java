package com.access.cargo.dashboard.kpi.model.redis;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@NoArgsConstructor
@RedisHash("WhOutInvoiceRedis")
public class WhOutInvoiceRedis implements Serializable {


    private static final long serialVersionUID = 2837229511011459838L;
    @Id
    Long id;
    String code;
    String whCode;
    String awb;
    String airline;
    String flightNo;
    String ori;
    String des;
    String route;
    Integer pieces;
    BigDecimal chargeableWeight;

    /**/
    Long agentId;
    String agent;
    String shipper;
    String consignee;

    /**/

    String paymentMethod;
    BigDecimal rates;
    BigDecimal whServiceTotal;
    Integer days;
    BigDecimal amount;
    BigDecimal amountChange;
    BigDecimal subTotal;
    BigDecimal docCharges;
    BigDecimal others;
    BigDecimal ppn;
    BigDecimal ppnPercent;
    BigDecimal pph;
    BigDecimal pphPercent;
    BigDecimal total;
    String [] paymentTypes;

    BigDecimal cargoServiceCharges;
    BigDecimal cargoServiceTotal;
    BigDecimal cargoHandlingCharges;
    BigDecimal cargoHandlingTotal;
    BigDecimal kadeCharges;
    BigDecimal kadeTotal;

    /**/

    Integer status;
    String statusDesc;

    Long createdById;
    String createdByName;
    LocalDateTime createdDateTime;

    Long modifiedById;
    String modifiedByName;
    LocalDateTime modifiedDateTime;

}
