package com.access.cargo.dashboard.kpi.model.Entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@NoArgsConstructor
@Data
@Entity
@Table(name = "uld")
public class Uld implements Serializable {

    private static final long serialVersionUID = 795411050087860918L;

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "code")
    private String code;

    public Uld(Long id) {
        this.id = id;
    }
}
