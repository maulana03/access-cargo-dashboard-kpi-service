package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class DashboardPreManifestReportSummaryRedis implements Serializable {
    private static final long serialVersionUID = -6104655646781565468L;
    @Id
    Long id;

    BigDecimal totalFlight;
    Integer totalAwb;
    Integer totalPieces;
    BigDecimal totalKg;
}
