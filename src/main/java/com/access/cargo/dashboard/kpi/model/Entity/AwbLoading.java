package com.access.cargo.dashboard.kpi.model.Entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "awb_loading")
public class AwbLoading implements Serializable {

    private static final long serialVersionUID = -6303635187670078889L;
    @Id
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "awb", referencedColumnName = "id")
    private Awb awb;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "flight_sch")
    private FlightList flightSch;

    @Column(name = "state")
    private Integer state;

    /**/

    @Column(name = "pieces", columnDefinition = "integer default 0")
    private Integer pieces = 0;

    @Column(name = "pieces_total", columnDefinition = "integer default 0")
    private Integer piecesTotal = 0;

    @Column(name = "shipment_type", length = 1)
    private String shipmentType; /*T:P*/

    @Column(name = "weight", precision = 19, scale = 3)
    private BigDecimal weight;

    @Column(name = "weight_total", precision = 19, scale = 3)
    private BigDecimal weightTotal;

    @Column(name = "created_datetime")
    private LocalDateTime createdDateTime;

    @ManyToOne
    @JoinColumn(name = "created_by", referencedColumnName = "id")
    private AccountUser createdBy;

    @Column(name = "modified_datetime")
    private LocalDateTime modifiedDateTime;

    @ManyToOne
    @JoinColumn(name = "modified_by")
    private AccountUser modifiedBy;
}
