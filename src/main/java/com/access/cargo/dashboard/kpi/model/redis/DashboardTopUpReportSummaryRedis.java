package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@RedisHash("DashboardTopUpReportSummaryRedis")
public class DashboardTopUpReportSummaryRedis implements Serializable {

    private static final long serialVersionUID = -2256824463924717188L;
    @Id
    Long id;

//    DashboardReportParams params;
    BigDecimal success;
    BigDecimal pending;
    BigDecimal failed;
//    String agent;
//    LocalDateTime date;
}
