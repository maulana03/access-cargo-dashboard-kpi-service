package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@RedisHash("DashboardWHAcceptanceReportDetailRedis")
public class DashboardWHAcceptanceReportDataDetailRedis implements Serializable {
    private static final long serialVersionUID = 4391945726904882574L;
    @Id
    Long id;
    String idTranscation;
    String agent;
    String awb;
    BigDecimal bookingPieces;
    BigDecimal bookingKg;
    Integer raPieces;
    BigDecimal raKg;
    Integer acceptedPieces;
    BigDecimal acceptedKg;
    Integer rejectedPieces;
    BigDecimal rejectedKg;
    LocalDate date;
    String branch;

}

