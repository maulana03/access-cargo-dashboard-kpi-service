package com.access.cargo.dashboard.kpi.repo;


import com.access.cargo.dashboard.kpi.model.Entity.RaRates;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface RaRatesRepo extends JpaRepository<RaRates, Long> {


    RaRates findFirstByAgent(String agent);
    RaRates findFirstByAgentAndStartDateAfterAndStopDateBeforeAndStatus(String agent, LocalDate startDate, LocalDate stopDate,Integer status);
    RaRates findFirstByAgentAndStartDateBeforeAndStopDateAfterAndStatus(String agent, LocalDate startDate, LocalDate stopDate,Integer status);

}
