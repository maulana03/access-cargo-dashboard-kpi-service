package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@RedisHash("DashboardTopUpReportDetailRedis")
public class DashboardTopUpReportDataDetailRedis implements Serializable {
    private static final long serialVersionUID = -2116095165846380443L;

    @Id
    Long id;
    String idTranscation;
    String agent;
    String transaction;
    String transactionTypeCode;

    BigDecimal amount;
    LocalDateTime date;
    String status;

}

