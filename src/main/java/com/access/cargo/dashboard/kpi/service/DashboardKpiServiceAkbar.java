package com.access.cargo.dashboard.kpi.service;


import com.access.cargo.dashboard.kpi.config.DateTimeCfg;
import com.access.cargo.dashboard.kpi.model.Entity.*;
import com.access.cargo.dashboard.kpi.model.redis.DashboardKpiChartDataRedis;
import com.access.cargo.dashboard.kpi.model.redis.InvoiceRaRedis;
import com.access.cargo.dashboard.kpi.model.redis.WhInInvoiceRedis;
import com.access.cargo.dashboard.kpi.model.wrapper.AwbRedis;
import com.access.cargo.dashboard.kpi.model.wrapper.AwbStateConstants;
import com.access.cargo.dashboard.kpi.model.wrapper.DashboardKpiSummary;
import com.access.cargo.dashboard.kpi.model.wrapper.Tabel;
import com.access.cargo.dashboard.kpi.model.wrapper.tes.*;
import com.access.cargo.dashboard.kpi.repo.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

@Slf4j
@Service
public class DashboardKpiServiceAkbar {
    @Autowired
    RaAcceptanceRepo raAcceptanceRepo;

    @Autowired
    AwbRepo awbRepo;

    @Autowired
    AwbLoadingRepo awbLoadingRepo;
    @Autowired
    AwbManManualRepo awbManManualRepo;

    @Autowired
    WhOutAndInServiceAkbar whOutAndInService;

    @Autowired
    AwbArrivalRepo awbArrivalRepo;

    @Autowired
    AwbWeighingRepo awbWeighingRepo;

    @Autowired
    RaRatesRepo raRatesRepo;

    @Autowired
    RaCustomerInvoiceSetupRepo raCustomerInvoiceSetupRepo;

    public DashboardKpiSummary getSummaryRange(String token, String branch_, LocalDate date1, LocalDate date2) {
        LocalDateTime localDateTimeStart = date1.atTime(00, 00, 00).minusSeconds(1);
        LocalDateTime localDateTimeEnd = date2.atTime(23, 59, 59).plusSeconds(1);

        LocalDateTime start = LocalDateTime.now(DateTimeCfg.ZONE_ID);
        System.out.println("\n\nstart = " + start);
        //booking
        String[] state1NotList = {AwbStateConstants.VOID, AwbStateConstants.CANCELED, AwbStateConstants.REJECTED, AwbStateConstants.OFFLOAD};
        Collection<Awb> awbs = awbRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndSrcRsvAndState1NotIn(localDateTimeStart, localDateTimeEnd, AwbStateConstants.SRC_RESERVATION, state1NotList);

        //Ra
        Collection<RaAcceptance> raAcceptances = raAcceptanceRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(localDateTimeStart, localDateTimeEnd, 1);
        raAcceptances.removeIf(x -> x.getStatusDesc().equals("XRAY_REJECT"));

        //who
        Collection<AwbWeighing> weighings = awbWeighingRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(localDateTimeStart, localDateTimeEnd, 1);

        //whm
        Collection<AwbLoading> awbLoadings = awbLoadingRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndState(localDateTimeStart, localDateTimeEnd, 1);
        Collection<AwbLoading> awbLoadings2 = awbLoadings.stream().filter(distinctByKey(x -> x.getAwb())).collect(Collectors.toList());

        //whi
        Collection<AwbManManual> awbManManuals = awbManManualRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(localDateTimeStart, localDateTimeEnd, 1);



        double x = 0.00;
        BigDecimal sumBkdWeightAmount = BigDecimal.valueOf(x);
        BigDecimal sumtBkdRevAmount = BigDecimal.valueOf(x);
        sumBkdWeightAmount = awbs.stream().map(b -> b.getChargeableWeight() != null ? b.getChargeableWeight() : BigDecimal.valueOf(0)).reduce(BigDecimal.ZERO, BigDecimal::add);
        sumtBkdRevAmount = awbs.stream().map(b -> b.getRatesTotal() != null ? b.getRatesTotal() : BigDecimal.valueOf(0)).reduce(BigDecimal.ZERO, BigDecimal::add);
//        for (Awb awb : awbs) {
//            sumBkdWeightAmount = sumBkdWeightAmount.add(awb.getChargeableWeight() != null ? awb.getChargeableWeight() : BigDecimal.valueOf(0));
//            sumtBkdRevAmount = sumtBkdRevAmount.add(awb.getRatesTotal() != null ? awb.getRatesTotal() : BigDecimal.valueOf(0));
//        }



        BigDecimal sumRaWeightAmount = BigDecimal.valueOf(x);
        BigDecimal sumtRaRevAmount = BigDecimal.valueOf(x);
        int i = 1;

        sumRaWeightAmount = raAcceptances.stream().map(b -> b.getWeightActual() != null && b.getWeightActual().compareTo(BigDecimal.ZERO) > 0 ? b.getWeightActual() : b.getWeight()).reduce(BigDecimal.ZERO, BigDecimal::add);
        sumtRaRevAmount = raAcceptances.stream().map(b -> this.invoiceRaRedisData(b.getAgent() ,b.getAgentId() ,b.getWeightActual() != null && b.getWeightActual().compareTo(BigDecimal.ZERO) > 0 ? b.getWeightActual() : b.getWeight()).getInvoiceSummary().getTotal()).reduce(BigDecimal.ZERO, BigDecimal::add);

//        for (RaAcceptance raAcceptance : raAcceptances) {
//            BigDecimal weight = raAcceptance.getWeightActual();
//            weight = weight != null && weight.compareTo(BigDecimal.ZERO) > 0 ? weight : raAcceptance.getWeight();
//
//            sumRaWeightAmount = sumRaWeightAmount.add(weight);
//            InvoiceRaRedis invoiceRaRedis = this.invoiceRaRedisData(raAcceptance.getAgent(), weight); /*todo*/
//            sumtRaRevAmount = sumtRaRevAmount.add(invoiceRaRedis.getInvoiceSummary().getTotal());
//            i = i + 1;
//        }



        BigDecimal sumWhoWeightAmount = BigDecimal.valueOf(x);
        BigDecimal sumtWhoRevAmount = BigDecimal.valueOf(x);

        sumWhoWeightAmount = weighings.stream().map(b -> b.getChargeableWeight() != null ? b.getChargeableWeight() : BigDecimal.valueOf(0)).reduce(BigDecimal.ZERO, BigDecimal::add);
        List<String> awbCodeTemp = weighings.stream().map(AwbWeighing::getAwbCode).collect (toList());
        String[] awbCode= awbCodeTemp.toArray(new String[0]);
        Collection<Awb> awbs1 = awbRepo.findAllByCodeIn(awbCode);
        Collection<AwbRedis> awbRedis1 = awbs1.stream().map(b -> this.mappingEntityToRedis(b)).collect(toList());
        sumtWhoRevAmount = awbRedis1.stream().map(b -> whOutAndInService.getByAwbRedisOut(b).getTotal()).reduce(BigDecimal.ZERO, BigDecimal::add);

//        for (AwbWeighing awbWeighing : weighings) {
//            sumWhoWeightAmount = sumWhoWeightAmount.add(awbWeighing.getChargeableWeight() != null ? awbWeighing.getChargeableWeight() : BigDecimal.valueOf(0));
//            Awb awb = awbRepo.findFirstByCode(awbWeighing.getAwbCode()); /*todo*/
//            AwbRedis awbRedis = mappingEntityToRedis(awb);
//            WhOutInvoiceRedis whOutInvoiceRedis = whOutAndInService.getByAwbRedisOut(awbRedis); /*todo*/
//            sumtWhoRevAmount = sumtWhoRevAmount.add(whOutInvoiceRedis.getTotal() != null ? whOutInvoiceRedis.getTotal() : BigDecimal.valueOf(0));
//        }


        BigDecimal sumWhmWeightAmount = BigDecimal.valueOf(x);
        BigDecimal sumWhmRevAmount = BigDecimal.valueOf(x);
        sumWhmWeightAmount = awbLoadings2.stream().map(b -> b.getAwb().getChargeableWeight() != null ? b.getAwb().getChargeableWeight() : BigDecimal.valueOf(0)).reduce(BigDecimal.ZERO, BigDecimal::add);
        sumWhmRevAmount = awbLoadings2.stream().map(b -> b.getAwb().getRatesTotal()  != null ? b.getAwb().getRatesTotal()  : BigDecimal.valueOf(0)).reduce(BigDecimal.ZERO, BigDecimal::add);

//        for (AwbLoading awbLoading : awbLoadings) {
//            sumWhmWeightAmount = sumWhmWeightAmount.add(awbLoading.getAwb().getChargeableWeight() != null ? awbLoading.getAwb().getChargeableWeight() : BigDecimal.valueOf(0));
//            sumWhmRevAmount = sumWhmRevAmount.add(awbLoading.getAwb().getRatesTotal() != null ? awbLoading.getAwb().getRatesTotal() : BigDecimal.valueOf(0));
//        }

        BigDecimal sumWhiWeightAmount = BigDecimal.valueOf(x);
        BigDecimal sumWhiRevAmount = BigDecimal.valueOf(x);

        sumWhiWeightAmount = awbManManuals.stream().map(b -> b.getChargeableWeight() != null ? b.getChargeableWeight() : BigDecimal.valueOf(0)).reduce(BigDecimal.ZERO, BigDecimal::add);
        List<String> awbCodeTempi = awbManManuals.stream().map(AwbManManual::getCode).collect (toList());
        String[] awbCodei= awbCodeTempi.toArray(new String[0]);
        Collection<Awb> awbsi = awbRepo.findAllByCodeIn(awbCodei);

//        Awb[] awbLists = awbsi.toArray(new Awb[0]);
//        Collection<AwbArrival> awbArrivals = awbArrivalRepo.findAllByAwbInAndState(awbLists,1);

        for (Awb awb : awbsi) {

            AwbArrival awbArrival = awbArrivalRepo.findFirstByAwbAndState(awb, 1); /*todo*/
            int diffDays = 1;
            if (awbArrival != null) {
                LocalDateTime arrivalTime = awbArrival.getCreatedDateTime() != null ? awbArrival.getCreatedDateTime() : awbArrival.getCreatedDateTime();
                LocalDateTime currentDateTime = LocalDateTime.now(DateTimeCfg.ZONE_ID);
                Duration duration = Duration.between(arrivalTime, currentDateTime);
                diffDays = (int) Math.abs(duration.toDays());
            }

            WhInInvoiceRedis invoiceRedis = whOutAndInService.getByAwbRedisIn(awb, diffDays); /*todo*/
            sumWhiRevAmount = sumWhiRevAmount.add(invoiceRedis.getTotal() != null ? invoiceRedis.getTotal() : BigDecimal.valueOf(0));

        }

//        for (AwbManManual awbManManual : awbManManuals) {
//            sumWhiWeightAmount = sumWhiWeightAmount.add(awbManManual.getChargeableWeight() != null ? awbManManual.getChargeableWeight() : BigDecimal.valueOf(0));
//
//            Awb awb = awbRepo.findFirstByCode(awbManManual.getCode()); /*todo*/
//            AwbArrival awbArrival = awbArrivalRepo.findFirstByAwbAndState(awb, 1); /*todo*/
//
//            int diffDays = 1;
//            if (awbArrival != null) {
//                LocalDateTime arrivalTime = awbArrival.getCreatedDateTime() != null ? awbArrival.getCreatedDateTime() : awbArrival.getCreatedDateTime();
//                LocalDateTime currentDateTime = LocalDateTime.now(DateTimeCfg.ZONE_ID);
//                Duration duration = Duration.between(arrivalTime, currentDateTime);
//                diffDays = (int) Math.abs(duration.toDays());
//            }
//
//            WhInInvoiceRedis invoiceRedis = whOutAndInService.getByAwbRedisIn(awbManManual, diffDays); /*todo*/
//            sumWhiRevAmount = sumWhiRevAmount.add(invoiceRedis.getTotal() != null ? invoiceRedis.getTotal() : BigDecimal.valueOf(0));
//        }


        LocalDateTime end = LocalDateTime.now();
        System.out.println("\n\nend = " + end);
        long nSeconds = start.until(end, ChronoUnit.SECONDS);
        System.out.println(raAcceptances.size()+"\n\nnSeconds-0 = " + nSeconds);

        DashboardKpiSummary dashboardKpiSummaryRedis = new DashboardKpiSummary();
        dashboardKpiSummaryRedis.setBkdWeightAmount(sumBkdWeightAmount.setScale(0, RoundingMode.HALF_UP));
        dashboardKpiSummaryRedis.setBkdRevAmount(sumtBkdRevAmount.setScale(0, RoundingMode.HALF_UP));
        dashboardKpiSummaryRedis.setRaWeightAmount(sumRaWeightAmount.setScale(0, RoundingMode.HALF_UP));
        dashboardKpiSummaryRedis.setRaRevAmount(sumtRaRevAmount.setScale(0, RoundingMode.HALF_UP));
        dashboardKpiSummaryRedis.setWhoWeightAmount(sumWhoWeightAmount.setScale(0, RoundingMode.HALF_UP));
        dashboardKpiSummaryRedis.setWhoRevAmount(sumtWhoRevAmount.setScale(0, RoundingMode.HALF_UP));
        dashboardKpiSummaryRedis.setWhmWeightAmount(sumWhmWeightAmount.setScale(0, RoundingMode.HALF_UP));
        dashboardKpiSummaryRedis.setWhmRevAmount(sumWhmRevAmount.setScale(0, RoundingMode.HALF_UP));
        dashboardKpiSummaryRedis.setWhiWeightAmount(sumWhiWeightAmount.setScale(0, RoundingMode.HALF_UP));
        dashboardKpiSummaryRedis.setWhiRevAmount(sumWhiRevAmount.setScale(0, RoundingMode.HALF_UP));
        return dashboardKpiSummaryRedis;
    }


    public String getAgent(String branch_, LocalDate date1, LocalDate date2) {
        LocalDateTime localDateTimeStart = date1.atTime(00, 00, 00).minusSeconds(1);
        LocalDateTime localDateTimeEnd = date2.atTime(23, 59, 59).plusSeconds(1);
        //booking
//        Collection<Awb> awbs1 = awbRepo.findAll().stream().filter(x -> x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());
//        Collection<Awb> awbs = awbs1.stream().filter(x -> !x.getState1().equals("VOID") && !x.getState1().equals("REJECTED") && !x.getState1().equals("CANCELED") && !x.getState1().equals("OFFLOAD") && !x.getState1().equals("OFFLOADING")).collect(Collectors.toList());
        String[] state1NotList = {AwbStateConstants.VOID, AwbStateConstants.CANCELED, AwbStateConstants.REJECTED, AwbStateConstants.OFFLOAD};
        Collection<Awb> awbs = awbRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndSrcRsvAndState1NotIn(localDateTimeStart, localDateTimeEnd, AwbStateConstants.SRC_RESERVATION, state1NotList);

        Map<String, BigDecimal> resultAwbs = awbs
                .stream()
                .collect(groupingBy(Awb::getAgentName, mapping(Awb::getChargeableWeight, reducing(BigDecimal.ZERO, BigDecimal::add))));

        //ra
//        Collection<RaAcceptance> raAcceptances = raAcceptanceRepo.findAllByStatus(1).stream().filter(x -> x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());
        Collection<RaAcceptance> raAcceptances1 = raAcceptanceRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(localDateTimeStart, localDateTimeEnd, 1);
        Collection<RaAcceptance> raAcceptances = new LinkedList<>();
        for (RaAcceptance raAcceptance : raAcceptances1) {
            BigDecimal weight = raAcceptance.getWeightActual();
//            Awb awb = awbRepo.findFirstByCode(raAcceptance.getAwb());
//            BigDecimal awbChargeableWeight = BigDecimal.ZERO;
//            if(awb != null){
//                awbChargeableWeight = awb.getChargeableWeight();
//            }
            weight = weight != null && weight.compareTo(BigDecimal.ZERO) > 0 ? weight : raAcceptance.getWeight();
            raAcceptance.setWeightActual(weight);
            raAcceptances.add(raAcceptance);
        }

        raAcceptances.removeIf(x -> x.getStatusDesc().equals("XRAY_REJECT"));


        Map<String, BigDecimal> resultRaAcceptances = raAcceptances
                .stream()
                .collect(groupingBy(RaAcceptance::getAgent, mapping(RaAcceptance::getWeightActual, reducing(BigDecimal.ZERO, BigDecimal::add))));

        //who
        // Collection<AwbLoading> awbLoadings = awbLoadingRepo.findAll().stream().filter(x -> x.getState().equals(1) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());
//        Collection<WhOutInvoice> whOutInvoices = whOutInvoiceRepo.findAll().stream().filter(x -> x.getStatus().equals(1) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());
//        Collection<WhOutInvoice> whOutInvoices = whOutInvoiceRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(localDateTimeStart, localDateTimeEnd, 1);
        Collection<AwbWeighing> weighings = awbWeighingRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(localDateTimeStart, localDateTimeEnd, 1);

        /*Collection<Awb> awbLoadingsAwb = new LinkedList<>();
        for(AwbLoading awbLoading : awbLoadings){
            Collection<Awb> awbs2 = awbRepo.findAllByCode(awbLoading.getAwb().getCode());
            for(Awb awb :awbs2){
                awbLoadingsAwb.add(awb);
            }
        }*/

        Map<String, BigDecimal> resultWhOutInvoices = weighings
                .stream()
                .collect(groupingBy(AwbWeighing::getAgentName, mapping(AwbWeighing::getChargeableWeight, reducing(BigDecimal.ZERO, BigDecimal::add))));

        //whm
//        Collection<Awb> awbManifest = awbs1.stream().filter(x -> x.getState().equals("MAN") || x.getState().equals("DEP") || x.getState().equals("ARR") || x.getState().equals("RCF")).collect(Collectors.toList());
//        Collection<AwbLoading> awbLoadings = awbLoadingRepo.findAll().stream().filter(x -> x.getState().equals(1) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());
        Collection<AwbLoading> awbLoadings = awbLoadingRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndState(localDateTimeStart, localDateTimeEnd, 1);
        Collection<AwbLoading> awbLoadings2 = awbLoadings.stream().filter(distinctByKey(x -> x.getAwb())).collect(Collectors.toList());

        Collection<Awb> awbLoadingsAwb = new LinkedList<>();
        for (AwbLoading awbLoading : awbLoadings2) {
            Collection<Awb> awbs2 = awbRepo.findAllByCode(awbLoading.getAwb().getCode());
            for (Awb awb : awbs2) {
                awbLoadingsAwb.add(awb);
            }
        }
        Map<String, BigDecimal> resultWhManifest = awbLoadingsAwb
                .stream()
                .collect(groupingBy(Awb::getAgentName, mapping(Awb::getChargeableWeight, reducing(BigDecimal.ZERO, BigDecimal::add))));

        //whi
//        Collection<WhInInvoice> whInInvoices = whInvoiceRepo.findAll().stream().filter(x -> x.getStatus().equals(1) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());
//        Collection<WhInInvoice> whInInvoices = whInvoiceRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(localDateTimeStart, localDateTimeEnd, 1);

        Collection<AwbManManual> awbManManuals = awbManManualRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(localDateTimeStart, localDateTimeEnd, 1);

        Map<String, BigDecimal> resultWhInInvoice = awbManManuals
                .stream()
                .collect(groupingBy(AwbManManual::getAgentName, mapping(AwbManManual::getChargeableWeight, reducing(BigDecimal.ZERO, BigDecimal::add))));


        Collection<Tabel> tabel = new LinkedList<>();
        BigDecimal sumBkdWeightAmount = BigDecimal.ZERO;
        BigDecimal bkdPercent = BigDecimal.ZERO;
        for (Awb awb : awbs) {
            sumBkdWeightAmount = sumBkdWeightAmount.add(awb.getChargeableWeight() != null ? awb.getChargeableWeight() : BigDecimal.valueOf(0));
        }

        for (Map.Entry<String, BigDecimal> entry : resultAwbs.entrySet()) {
            Tabel dashboardKpiTableDataRedis1 = new Tabel();
            String key = entry.getKey();
            BigDecimal value = entry.getValue();
//            Agent-1 punya porsi 30.000, maka 30.000/100.000 * 100 = 30%
//            BigDecimal bkdBagi = value.divide(sumBkdWeightAmount,0, RoundingMode.HALF_UP);
            BigDecimal bkdBagi = value.divide(sumBkdWeightAmount, 3, RoundingMode.HALF_UP);
            bkdPercent = bkdBagi.multiply(BigDecimal.valueOf(100));
            dashboardKpiTableDataRedis1.setName("BOOKING");
            dashboardKpiTableDataRedis1.setJenis(key);
            dashboardKpiTableDataRedis1.setWeight(value.setScale(0, RoundingMode.HALF_UP));
//            dashboardKpiTableDataRedis1.setBkdWeightAmount(sumBkdWeightAmount.setScale(0, RoundingMode.HALF_UP));
            dashboardKpiTableDataRedis1.setPercent(bkdPercent.setScale(0, RoundingMode.HALF_UP));

            tabel.add(dashboardKpiTableDataRedis1);
        }


        BigDecimal sumRaWeightAmount = BigDecimal.ZERO;
        BigDecimal raPercent = BigDecimal.ZERO;

        for (RaAcceptance raAcceptance : raAcceptances) {
//            sumRaWeightAmount = sumRaWeightAmount.add(raAcceptance.getWeight() != null ? raAcceptance.getWeight() : BigDecimal.valueOf(0));
            sumRaWeightAmount = sumRaWeightAmount.add(raAcceptance.getWeightActual());
        }

        for (Map.Entry<String, BigDecimal> entry : resultRaAcceptances.entrySet()) {
            Tabel dashboardKpiTableDataRedis2 = new Tabel();
            String key = entry.getKey();
            BigDecimal value = entry.getValue();
            BigDecimal raBagi = value.divide(sumRaWeightAmount, 3, RoundingMode.HALF_UP);
            raPercent = raBagi.multiply(BigDecimal.valueOf(100));

            dashboardKpiTableDataRedis2.setJenis(key);
            dashboardKpiTableDataRedis2.setName("RA");
            dashboardKpiTableDataRedis2.setWeight(value.setScale(0, RoundingMode.HALF_UP));
            dashboardKpiTableDataRedis2.setPercent(raPercent.setScale(0, RoundingMode.HALF_UP));

            tabel.add(dashboardKpiTableDataRedis2);
        }


        BigDecimal sumWhoWeightAmount = BigDecimal.ZERO;
        BigDecimal whoPercent = BigDecimal.ZERO;
        for (AwbWeighing awbWeighing : weighings) {
            sumWhoWeightAmount = sumWhoWeightAmount.add(awbWeighing.getChargeableWeight() != null ? awbWeighing.getChargeableWeight() : BigDecimal.valueOf(0));
        }

        for (Map.Entry<String, BigDecimal> entry : resultWhOutInvoices.entrySet()) {

            Tabel dashboardKpiTableDataRedis3 = new Tabel();
            String key = entry.getKey();
            BigDecimal value = entry.getValue();
            BigDecimal whoBagi = value.divide(sumWhoWeightAmount, 3, RoundingMode.HALF_UP);
            whoPercent = whoBagi.multiply(BigDecimal.valueOf(100));
            dashboardKpiTableDataRedis3.setJenis(key);
            dashboardKpiTableDataRedis3.setName("WHO");
            dashboardKpiTableDataRedis3.setWeight(value.setScale(0, RoundingMode.HALF_UP));
            dashboardKpiTableDataRedis3.setPercent(whoPercent.setScale(0, RoundingMode.HALF_UP));
            tabel.add(dashboardKpiTableDataRedis3);

        }

        BigDecimal sumWhmWeightAmount = BigDecimal.ZERO;
        BigDecimal whmPercent = BigDecimal.ZERO;
        for (AwbLoading awbLoading : awbLoadings2) {
            sumWhmWeightAmount = sumWhmWeightAmount.add(awbLoading.getAwb().getChargeableWeight() != null ? awbLoading.getAwb().getChargeableWeight() : BigDecimal.valueOf(0));
        }

        for (Map.Entry<String, BigDecimal> entry : resultWhManifest.entrySet()) {

            Tabel dashboardKpiTableDataRedis4 = new Tabel();
            String key = entry.getKey();
            BigDecimal value = entry.getValue();
            BigDecimal whmBagi = value.divide(sumWhmWeightAmount, 3, RoundingMode.HALF_UP);
            whmPercent = whmBagi.multiply(BigDecimal.valueOf(100));
            dashboardKpiTableDataRedis4.setJenis(key);
            dashboardKpiTableDataRedis4.setName("WHM");
            dashboardKpiTableDataRedis4.setWeight(value.setScale(0, RoundingMode.HALF_UP));
            dashboardKpiTableDataRedis4.setPercent(whmPercent.setScale(0, RoundingMode.HALF_UP));
            tabel.add(dashboardKpiTableDataRedis4);

        }

        BigDecimal sumWhiWeightAmount = BigDecimal.ZERO;
        BigDecimal whiPercent = BigDecimal.ZERO;
        for (AwbManManual awbManManual : awbManManuals) {
            sumWhiWeightAmount = sumWhiWeightAmount.add(awbManManual.getChargeableWeight() != null ? awbManManual.getChargeableWeight() : BigDecimal.valueOf(0));
        }

        for (Map.Entry<String, BigDecimal> entry : resultWhInInvoice.entrySet()) {

            Tabel dashboardKpiTableDataRedis5 = new Tabel();
            String key = entry.getKey();
            BigDecimal value = entry.getValue();
            BigDecimal whiBagi = value.divide(sumWhiWeightAmount, 3, RoundingMode.HALF_UP);
            whiPercent = whiBagi.multiply(BigDecimal.valueOf(100));
            dashboardKpiTableDataRedis5.setJenis(key);
            dashboardKpiTableDataRedis5.setName("WHI");
            dashboardKpiTableDataRedis5.setWeight(value.setScale(0, RoundingMode.HALF_UP));
            dashboardKpiTableDataRedis5.setPercent(whiPercent.setScale(0, RoundingMode.HALF_UP));
            tabel.add(dashboardKpiTableDataRedis5);

        }


        Map<String, List<Tabel>> dataLabel = tabel.stream().collect(groupingBy(Tabel::getJenis));

        List<Root> roots = new LinkedList<>();
        for (Map.Entry<String, List<Tabel>> entryTabel : dataLabel.entrySet()) {
            Root root = new Root();
            String key = entryTabel.getKey();
            List<Tabel> value = entryTabel.getValue();
            root.setBkdWeightAmount(BigDecimal.valueOf(0));
            root.setBkdRevAmount(BigDecimal.valueOf(0));
            root.setRaWeightAmount(BigDecimal.valueOf(0));
            root.setRaRevAmount(BigDecimal.valueOf(0));
            root.setWhoWeightAmount(BigDecimal.valueOf(0));
            root.setWhoRevAmount(BigDecimal.valueOf(0));
            root.setWhiWeightAmount(BigDecimal.valueOf(0));
            root.setWhiRevAmount(BigDecimal.valueOf(0));
            root.setWhmWeightAmount(BigDecimal.valueOf(0));
            root.setWhmRevAmount(BigDecimal.valueOf(0));
//            root.setWhtWeightAmount(BigDecimal.valueOf(0));
//            root.setWhtRevAmount(BigDecimal.valueOf(0));

            for (Tabel a : value) {

                root.setName(key);
                if (a.getName().equals("BOOKING")) {
                    root.setBkdWeightAmount(a.getWeight().setScale(0, RoundingMode.HALF_UP));
                    root.setBkdRevAmount(a.getPercent().setScale(0, RoundingMode.HALF_UP));
                } else if (a.getName().equals("RA")) {
                    root.setRaWeightAmount(a.getWeight().setScale(0, RoundingMode.HALF_UP));
                    root.setRaRevAmount(a.getPercent().setScale(0, RoundingMode.HALF_UP));
                } else if (a.getName().equals("WHO")) {
                    root.setWhoWeightAmount(a.getWeight().setScale(0, RoundingMode.HALF_UP));
                    root.setWhoRevAmount(a.getPercent().setScale(0, RoundingMode.HALF_UP));
                } else if (a.getName().equals("WHM")) {
                    root.setWhmWeightAmount(a.getWeight().setScale(0, RoundingMode.HALF_UP));
                    root.setWhmRevAmount(a.getPercent().setScale(0, RoundingMode.HALF_UP));
                } else if (a.getName().equals("WHI")) {
                    root.setWhiWeightAmount(a.getWeight().setScale(0, RoundingMode.HALF_UP));
                    root.setWhiRevAmount(a.getPercent().setScale(0, RoundingMode.HALF_UP));
                }
//                else if(a.getName().equals("WHT")){
//                    root.setWhtWeightAmount(BigDecimal.valueOf(0));
//                    root.setWhtRevAmount(BigDecimal.valueOf(0));
//                }
            }

            roots.add(root);
        }

        ObjectMapper objectMapper = new ObjectMapper();
        String dataJson = null;
        try {
//            System.out.println(objectMapper.writeValueAsString(roots));
            dataJson = objectMapper.writeValueAsString(roots);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return dataJson;
    }

    public String getAirlines(String branch_, LocalDate date1, LocalDate date2) {

        LocalDateTime localDateTimeStart = date1.atTime(00, 00, 00).minusSeconds(1);
        LocalDateTime localDateTimeEnd = date2.atTime(23, 59, 59).plusSeconds(1);
//        Collection<Awb> awbs1 = awbRepo.findAll().stream().filter(x -> x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());
//        Collection<Awb> awbs = awbs1.stream().filter(x -> !x.getState1().equals("VOID") && !x.getState1().equals("REJECTED") && !x.getState1().equals("CANCELED") && !x.getState1().equals("OFFLOAD") && !x.getState1().equals("OFFLOADING")).collect(Collectors.toList());
        String[] state1NotList = {AwbStateConstants.VOID, AwbStateConstants.CANCELED, AwbStateConstants.REJECTED, AwbStateConstants.OFFLOAD};
        Collection<Awb> awbs = awbRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndSrcRsvAndState1NotIn(localDateTimeStart, localDateTimeEnd, AwbStateConstants.SRC_RESERVATION, state1NotList);
        Map<String, BigDecimal> resultAwb = awbs
                .stream()
                .collect(groupingBy(Awb::getAirline, mapping(Awb::getChargeableWeight, reducing(BigDecimal.ZERO, BigDecimal::add))));

        //ra
//        Collection<RaAcceptance> raAcceptances = raAcceptanceRepo.findAllByStatus(1).stream().filter(x -> x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());
        Collection<RaAcceptance> raAcceptances1 = raAcceptanceRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(localDateTimeStart, localDateTimeEnd, 1);
        Collection<RaAcceptance> raAcceptances = new LinkedList<>();
        for (RaAcceptance raAcceptance : raAcceptances1) {
            BigDecimal weight = raAcceptance.getWeightActual();
//            Awb awb = awbRepo.findFirstByCode(raAcceptance.getAwb());
//            BigDecimal awbChargeableWeight = BigDecimal.ZERO;
//            if(awb != null){
//                awbChargeableWeight = awb.getChargeableWeight();
//            }
            weight = weight != null && weight.compareTo(BigDecimal.ZERO) > 0 ? weight : raAcceptance.getWeight();
            raAcceptance.setWeightActual(weight);
            raAcceptances.add(raAcceptance);
        }
        raAcceptances.removeIf(x -> x.getStatusDesc().equals("XRAY_REJECT"));

        Map<String, BigDecimal> resultRaAcceptances = raAcceptances
                .stream()
                .collect(groupingBy(RaAcceptance::getAirline, mapping(RaAcceptance::getWeightActual, reducing(BigDecimal.ZERO, BigDecimal::add))));

        //who
//        Collection<AwbLoading> awbLoadings = awbLoadingRepo.findAll().stream().filter(x -> x.getState().equals(1) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());
//        Collection<WhOutInvoice> whOutInvoices = whOutInvoiceRepo.findAll().stream().filter(x -> x.getStatus().equals(1) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());
//        Collection<WhOutInvoice> whOutInvoices = whOutInvoiceRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(localDateTimeStart, localDateTimeEnd, 1);
        Collection<AwbWeighing> weighings = awbWeighingRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(localDateTimeStart, localDateTimeEnd, 1);

        Collection<Awb> awbWeighingAwb = new LinkedList<>();
        for (AwbWeighing awbWeighing : weighings) {
            Collection<Awb> awbs2 = awbRepo.findAllByCode(awbWeighing.getAwb().getCode());
            for (Awb awb : awbs2) {
                awbWeighingAwb.add(awb);
            }
        }
        Map<String, BigDecimal> resultWhOutInvoices = awbWeighingAwb
                .stream()
                .collect(groupingBy(Awb::getAirline, mapping(Awb::getChargeableWeight, reducing(BigDecimal.ZERO, BigDecimal::add))));

        //whm
//        Collection<Awb> awbManifest = awbs1.stream().filter(x -> x.getState().equals("MAN") || x.getState().equals("DEP") || x.getState().equals("ARR") || x.getState().equals("RCF")).collect(Collectors.toList());
//        Collection<AwbLoading> awbLoadings = awbLoadingRepo.findAll().stream().filter(x -> x.getState().equals(1) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());
        Collection<AwbLoading> awbLoadings = awbLoadingRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndState(localDateTimeStart, localDateTimeEnd, 1);
        Collection<AwbLoading> awbLoadings2 = awbLoadings.stream().filter(distinctByKey(x -> x.getAwb())).collect(Collectors.toList());

        Collection<Awb> awbLoadingsAwb = new LinkedList<>();
        for (AwbLoading awbLoading : awbLoadings2) {
            Collection<Awb> awbs2 = awbRepo.findAllByCode(awbLoading.getAwb().getCode());
            for (Awb awb : awbs2) {
                awbLoadingsAwb.add(awb);
            }
        }
        Map<String, BigDecimal> resultWhManifest = awbLoadingsAwb
                .stream()
                .collect(groupingBy(Awb::getAirline, mapping(Awb::getChargeableWeight, reducing(BigDecimal.ZERO, BigDecimal::add))));

        //whi
//        Collection<WhInInvoice> whInInvoices = whInvoiceRepo.findAll().stream().filter(x -> x.getStatus().equals(1) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());
//        Collection<WhInInvoice> whInInvoices = whInvoiceRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(localDateTimeStart, localDateTimeEnd, 1);
        Collection<AwbManManual> awbManManuals = awbManManualRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(localDateTimeStart, localDateTimeEnd, 1);

        Map<String, BigDecimal> resultWhInInvoice = awbManManuals
                .stream()
                .collect(groupingBy(AwbManManual::getAirline, mapping(AwbManManual::getChargeableWeight, reducing(BigDecimal.ZERO, BigDecimal::add))));


        Collection<Tabel> tabel = new LinkedList<>();
        BigDecimal sumBkdWeightAmount = BigDecimal.ZERO;
        BigDecimal bkdPercent = BigDecimal.ZERO;
        for (Awb awb : awbs) {
            sumBkdWeightAmount = sumBkdWeightAmount.add(awb.getChargeableWeight() != null ? awb.getChargeableWeight() : BigDecimal.valueOf(0));
        }

        for (Map.Entry<String, BigDecimal> entry : resultAwb.entrySet()) {
            Tabel dashboardKpiTableDataRedis1 = new Tabel();
            String key = entry.getKey();
            BigDecimal value = entry.getValue();
//            Agent-1 punya porsi 30.000, maka 30.000/100.000 * 100 = 30%
//            BigDecimal bkdBagi = value.divide(sumBkdWeightAmount,0, RoundingMode.HALF_UP);
            BigDecimal bkdBagi = value.divide(sumBkdWeightAmount, 3, RoundingMode.HALF_UP);
            bkdPercent = bkdBagi.multiply(BigDecimal.valueOf(100));
            dashboardKpiTableDataRedis1.setName("BOOKING");
            dashboardKpiTableDataRedis1.setJenis(key);
            dashboardKpiTableDataRedis1.setWeight(value.setScale(0, RoundingMode.HALF_UP));
//            dashboardKpiTableDataRedis1.setBkdWeightAmount(sumBkdWeightAmount.setScale(0, RoundingMode.HALF_UP));
            dashboardKpiTableDataRedis1.setPercent(bkdPercent.setScale(0, RoundingMode.HALF_UP));

            tabel.add(dashboardKpiTableDataRedis1);
        }


        BigDecimal sumRaWeightAmount = BigDecimal.ZERO;
        BigDecimal raPercent = BigDecimal.ZERO;

        for (RaAcceptance raAcceptance : raAcceptances) {
//            sumRaWeightAmount = sumRaWeightAmount.add(raAcceptance.getWeight() != null ? raAcceptance.getWeight() : BigDecimal.valueOf(0));

            sumRaWeightAmount = sumRaWeightAmount.add(raAcceptance.getWeightActual());
        }

        for (Map.Entry<String, BigDecimal> entry : resultRaAcceptances.entrySet()) {
            Tabel dashboardKpiTableDataRedis2 = new Tabel();
            String key = entry.getKey();
            BigDecimal value = entry.getValue();
            BigDecimal raBagi = value.divide(sumRaWeightAmount, 3, RoundingMode.HALF_UP);
            raPercent = raBagi.multiply(BigDecimal.valueOf(100));

            dashboardKpiTableDataRedis2.setJenis(key);
            dashboardKpiTableDataRedis2.setName("RA");
            dashboardKpiTableDataRedis2.setWeight(value.setScale(0, RoundingMode.HALF_UP));
            dashboardKpiTableDataRedis2.setPercent(raPercent.setScale(0, RoundingMode.HALF_UP));

            tabel.add(dashboardKpiTableDataRedis2);
        }


        BigDecimal sumWhoWeightAmount = BigDecimal.ZERO;
        BigDecimal whoPercent = BigDecimal.ZERO;
        for (AwbWeighing awbWeighing : weighings) {
            sumWhoWeightAmount = sumWhoWeightAmount.add(awbWeighing.getChargeableWeight() != null ? awbWeighing.getChargeableWeight() : BigDecimal.valueOf(0));
        }

        for (Map.Entry<String, BigDecimal> entry : resultWhOutInvoices.entrySet()) {

            Tabel dashboardKpiTableDataRedis3 = new Tabel();
            String key = entry.getKey();
            BigDecimal value = entry.getValue();
            BigDecimal whoBagi = value.divide(sumWhoWeightAmount, 3, RoundingMode.HALF_UP);
            whoPercent = whoBagi.multiply(BigDecimal.valueOf(100));

            dashboardKpiTableDataRedis3.setJenis(key);
            dashboardKpiTableDataRedis3.setName("WHO");
            dashboardKpiTableDataRedis3.setWeight(value.setScale(0, RoundingMode.HALF_UP));
            dashboardKpiTableDataRedis3.setPercent(whoPercent.setScale(0, RoundingMode.HALF_UP));
            tabel.add(dashboardKpiTableDataRedis3);

        }

        BigDecimal sumWhmWeightAmount = BigDecimal.ZERO;
        BigDecimal whmPercent = BigDecimal.ZERO;
        for (AwbLoading awbLoading : awbLoadings2) {
            sumWhmWeightAmount = sumWhmWeightAmount.add(awbLoading.getAwb().getChargeableWeight() != null ? awbLoading.getAwb().getChargeableWeight() : BigDecimal.valueOf(0));
        }

        for (Map.Entry<String, BigDecimal> entry : resultWhManifest.entrySet()) {

            Tabel dashboardKpiTableDataRedis4 = new Tabel();
            String key = entry.getKey();
            BigDecimal value = entry.getValue();
            BigDecimal whmBagi = value.divide(sumWhmWeightAmount, 3, RoundingMode.HALF_UP);
            whmPercent = whmBagi.multiply(BigDecimal.valueOf(100));
            dashboardKpiTableDataRedis4.setJenis(key);
            dashboardKpiTableDataRedis4.setName("WHM");
            dashboardKpiTableDataRedis4.setWeight(value.setScale(0, RoundingMode.HALF_UP));
            dashboardKpiTableDataRedis4.setPercent(whmPercent.setScale(0, RoundingMode.HALF_UP));
            tabel.add(dashboardKpiTableDataRedis4);

        }

        BigDecimal sumWhiWeightAmount = BigDecimal.ZERO;
        BigDecimal whiPercent = BigDecimal.ZERO;
        for (AwbManManual awbManManual : awbManManuals) {
            sumWhiWeightAmount = sumWhiWeightAmount.add(awbManManual.getChargeableWeight() != null ? awbManManual.getChargeableWeight() : BigDecimal.valueOf(0));
        }

        for (Map.Entry<String, BigDecimal> entry : resultWhInInvoice.entrySet()) {

            Tabel dashboardKpiTableDataRedis5 = new Tabel();
            String key = entry.getKey();
            BigDecimal value = entry.getValue();
            BigDecimal whiBagi = value.divide(sumWhiWeightAmount, 3, RoundingMode.HALF_UP);
            whiPercent = whiBagi.multiply(BigDecimal.valueOf(100));
            dashboardKpiTableDataRedis5.setJenis(key);
            dashboardKpiTableDataRedis5.setName("WHI");
            dashboardKpiTableDataRedis5.setWeight(value.setScale(0, RoundingMode.HALF_UP));
            dashboardKpiTableDataRedis5.setPercent(whiPercent.setScale(0, RoundingMode.HALF_UP));
            tabel.add(dashboardKpiTableDataRedis5);

        }

        Map<String, List<Tabel>> dataLabel = tabel.stream().collect(groupingBy(Tabel::getJenis));
        List<Root> roots = new LinkedList<>();
        for (Map.Entry<String, List<Tabel>> entryTabel : dataLabel.entrySet()) {
            Root root = new Root();
            String key = entryTabel.getKey();
            List<Tabel> value = entryTabel.getValue();
            root.setBkdWeightAmount(BigDecimal.valueOf(0));
            root.setBkdRevAmount(BigDecimal.valueOf(0));
            root.setRaWeightAmount(BigDecimal.valueOf(0));
            root.setRaRevAmount(BigDecimal.valueOf(0));
            root.setWhoWeightAmount(BigDecimal.valueOf(0));
            root.setWhoRevAmount(BigDecimal.valueOf(0));
            root.setWhiWeightAmount(BigDecimal.valueOf(0));
            root.setWhiRevAmount(BigDecimal.valueOf(0));
            root.setWhmWeightAmount(BigDecimal.valueOf(0));
            root.setWhmRevAmount(BigDecimal.valueOf(0));
//            root.setWhtWeightAmount(BigDecimal.valueOf(0));
//            root.setWhtRevAmount(BigDecimal.valueOf(0));

            for (Tabel a : value) {

                root.setName(key);

                if (a.getName().equals("BOOKING")) {
                    root.setBkdWeightAmount(a.getWeight().setScale(0, RoundingMode.HALF_UP));
                    root.setBkdRevAmount(a.getPercent().setScale(0, RoundingMode.HALF_UP));
                } else if (a.getName().equals("RA")) {
                    root.setRaWeightAmount(a.getWeight().setScale(0, RoundingMode.HALF_UP));
                    root.setRaRevAmount(a.getPercent().setScale(0, RoundingMode.HALF_UP));
                } else if (a.getName().equals("WHO")) {
                    root.setWhoWeightAmount(a.getWeight().setScale(0, RoundingMode.HALF_UP));
                    root.setWhoRevAmount(a.getPercent().setScale(0, RoundingMode.HALF_UP));
                } else if (a.getName().equals("WHM")) {
                    root.setWhmWeightAmount(a.getWeight().setScale(0, RoundingMode.HALF_UP));
                    root.setWhmRevAmount(a.getPercent().setScale(0, RoundingMode.HALF_UP));
                } else if (a.getName().equals("WHI")) {
                    root.setWhiWeightAmount(a.getWeight().setScale(0, RoundingMode.HALF_UP));
                    root.setWhiRevAmount(a.getPercent().setScale(0, RoundingMode.HALF_UP));
                }
//                else if(a.getName().equals("WHT")){
//                    root.setWhtWeightAmount(BigDecimal.valueOf(0));
//                    root.setWhtRevAmount(BigDecimal.valueOf(0));
//                }
            }

            roots.add(root);
        }

        ObjectMapper objectMapper = new ObjectMapper();
        String dataJson = null;
        try {
//            System.out.println(objectMapper.writeValueAsString(roots));
            dataJson = objectMapper.writeValueAsString(roots);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }


        return dataJson;
    }

    public String getCarts(String header, String branch_, String achievementType, String period, LocalDate date1, LocalDate date2) {
        LocalDateTime localDateTimeStart = date1.atTime(00, 00, 00).minusSeconds(1);
        LocalDateTime localDateTimeEnd = date2.atTime(23, 59, 59).plusSeconds(1);

        String dataJson = null;

        Map<String, Map<String, BigDecimal>> result = null;
        Map<String, Map<String, BigDecimal>> result2 = null;

        if (achievementType.equals("BOOKING")) {
//            List<Awb> awbs1 = awbRepo.findAll().stream().filter(x -> x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());
//            List<Awb> awbs = awbs1.stream().filter(x -> !x.getState1().equals("VOID") && !x.getState1().equals("REJECTED") && !x.getState1().equals("CANCELED") && !x.getState1().equals("OFFLOAD") && !x.getState1().equals("OFFLOADING")).collect(Collectors.toList());
            String[] state1NotList = {AwbStateConstants.VOID, AwbStateConstants.CANCELED, AwbStateConstants.REJECTED, AwbStateConstants.OFFLOAD};
            Collection<Awb> awb1 = awbRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndSrcRsvAndState1NotIn(localDateTimeStart, localDateTimeEnd, AwbStateConstants.SRC_RESERVATION, state1NotList);
            List<Awb> awbs = awb1.stream().collect(toList());
            awbs.sort(Comparator.comparing(o -> o.getCreatedDateTime()));


            Collection<DashboardKpiChartDataRedis> dashboardKpiChartDataRedisColl = new LinkedHashSet<>();
            for (Awb awb : awbs) {
                DashboardKpiChartDataRedis dashboardKpiChartDataRedis = new DashboardKpiChartDataRedis();
//                LocalDate date = awb.getFlightDate();
                LocalDateTime date = awb.getCreatedDateTime();

                if (header.equals("Agent")) {
                    dashboardKpiChartDataRedis.setName(awb.getAgentName());
                } else {
                    dashboardKpiChartDataRedis.setName(awb.getAirline());
                }
                dashboardKpiChartDataRedis.setValue(awb.getChargeableWeight() != null ? awb.getChargeableWeight() : BigDecimal.valueOf(0));
                dashboardKpiChartDataRedisColl.add(dashboardKpiChartDataRedis);

                if (period.equals("PERIOD_LAST_WEEK")) {
                    dashboardKpiChartDataRedis.setLabel(date.getDayOfWeek().name() + " " + date.getMonth().name());
                } else if (period.equals("PERIOD_LAST_MONTH") || period.equals("PERIOD_CUSTOM_MONTH")) {
                    dashboardKpiChartDataRedis.setLabel(date.getDayOfMonth() + " " + date.getMonth().name());
                } else if (period.equals("PERIOD_LAST_YEAR") || period.equals("PERIOD_CUSTOM_YEAR")) {
                    dashboardKpiChartDataRedis.setLabel(date.getMonth().name() + " " + date.getYear());
                }
            }

            result = dashboardKpiChartDataRedisColl
                    .stream()
                    .collect(
                            groupingBy(DashboardKpiChartDataRedis::getLabel, groupingBy(DashboardKpiChartDataRedis::getName,
                                    mapping(DashboardKpiChartDataRedis::getValue,
                                            reducing(BigDecimal.ZERO, BigDecimal::add)))));

            result2 = dashboardKpiChartDataRedisColl
                    .stream()
                    .collect(
                            groupingBy(DashboardKpiChartDataRedis::getName, groupingBy(DashboardKpiChartDataRedis::getLabel,
                                    mapping(DashboardKpiChartDataRedis::getValue,
                                            reducing(BigDecimal.ZERO, BigDecimal::add)))));

        } else if (achievementType.equals("RA")) {
            Collection<RaAcceptance> raAcceptances1 = raAcceptanceRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(localDateTimeStart, localDateTimeEnd, 1);
            raAcceptances1.removeIf(x -> x.getStatusDesc().equals("XRAY_REJECT"));
            List<RaAcceptance> raAcceptances = raAcceptances1.stream().collect(toList());
            raAcceptances.sort(Comparator.comparing(o -> o.getCreatedDateTime()));


            Collection<DashboardKpiChartDataRedis> dashboardKpiChartDataRedisColl = new LinkedHashSet<>();
            for (RaAcceptance raAcceptance : raAcceptances) {
                DashboardKpiChartDataRedis dashboardKpiChartDataRedis = new DashboardKpiChartDataRedis();
                LocalDateTime date = raAcceptance.getCreatedDateTime();

                if (period.equals("PERIOD_LAST_WEEK")) {
                    dashboardKpiChartDataRedis.setLabel(date.getDayOfWeek().name() + " " + date.getMonth().name());
                } else if (period.equals("PERIOD_LAST_MONTH") || period.equals("PERIOD_CUSTOM_MONTH")) {
                    dashboardKpiChartDataRedis.setLabel(date.getDayOfMonth() + " " + date.getMonth().name());
                } else if (period.equals("PERIOD_LAST_YEAR") || period.equals("PERIOD_CUSTOM_YEAR")) {
                    dashboardKpiChartDataRedis.setLabel(date.getMonth().name() + " " + date.getYear());
                }

                if (header.equals("Agent")) {
                    dashboardKpiChartDataRedis.setName(raAcceptance.getAgent());
                } else {
                    dashboardKpiChartDataRedis.setName(raAcceptance.getAirline());
                }
                BigDecimal weight = raAcceptance.getWeightActual();
//                Awb awb = awbRepo.findFirstByCode(raAcceptance.getAwb());
//                BigDecimal awbChargeableWeight = BigDecimal.ZERO;
//                if(awb != null){
//                    awbChargeableWeight = awb.getChargeableWeight();
//                }
                weight = weight != null && weight.compareTo(BigDecimal.ZERO) > 0 ? weight : raAcceptance.getWeight();
                dashboardKpiChartDataRedis.setValue(weight);
                dashboardKpiChartDataRedisColl.add(dashboardKpiChartDataRedis);
            }

            result = dashboardKpiChartDataRedisColl
                    .stream()
                    .collect(
                            groupingBy(DashboardKpiChartDataRedis::getLabel, groupingBy(DashboardKpiChartDataRedis::getName,
                                    mapping(DashboardKpiChartDataRedis::getValue,
                                            reducing(BigDecimal.ZERO, BigDecimal::add)))));

            result2 = dashboardKpiChartDataRedisColl
                    .stream()
                    .collect(
                            groupingBy(DashboardKpiChartDataRedis::getName, groupingBy(DashboardKpiChartDataRedis::getLabel,
                                    mapping(DashboardKpiChartDataRedis::getValue,
                                            reducing(BigDecimal.ZERO, BigDecimal::add)))));
        } else if (achievementType.equals("WHO")) {
//            List<WhOutInvoice> whOutInvoices = whOutInvoiceRepo.findAll().stream().filter(x -> x.getStatus().equals(1) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());

//            Collection<WhOutInvoice> whOutInvoices1 = whOutInvoiceRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(localDateTimeStart, localDateTimeEnd, 1);
//            List<WhOutInvoice> whOutInvoices = whOutInvoices1.stream().collect(toList());
//            whOutInvoices.sort(Comparator.comparing(o -> o.getCreatedDateTime()));

            Collection<AwbWeighing> weighings2 = awbWeighingRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(localDateTimeStart, localDateTimeEnd, 1);
            List<AwbWeighing> awbWeighings = weighings2.stream().collect(toList());
            awbWeighings.sort(Comparator.comparing(o -> o.getCreatedDateTime()));

//            List<AwbLoading> awbLoadings = awbLoadingRepo.findAll().stream().filter(x -> x.getState().equals(1) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());
//            awbLoadings.sort(Comparator.comparing(o -> o.getCreatedDateTime()));
//            List<Awb> awbLoadingsAwb = new LinkedList<>();
//            for(AwbLoading awbLoading : awbLoadings){
//                Collection<Awb> awbs2 = awbRepo.findAllByCode(awbLoading.getAwb().getCode());
//                for(Awb awb :awbs2){
//                    awbLoadingsAwb.add(awb);
//                }
//            }
//            awbLoadingsAwb.sort(Comparator.comparing(o -> o.getCreatedDateTime()));


            Collection<DashboardKpiChartDataRedis> dashboardKpiChartDataRedisColl = new LinkedHashSet<>();
            for (AwbWeighing awbWeighing : awbWeighings) {
                DashboardKpiChartDataRedis dashboardKpiChartDataRedis = new DashboardKpiChartDataRedis();
                LocalDateTime date = awbWeighing.getCreatedDateTime();

                if (period.equals("PERIOD_LAST_WEEK")) {
                    dashboardKpiChartDataRedis.setLabel(date.getDayOfWeek().name() + " " + date.getMonth().name());
                } else if (period.equals("PERIOD_LAST_MONTH") || period.equals("PERIOD_CUSTOM_MONTH")) {
                    dashboardKpiChartDataRedis.setLabel(date.getDayOfMonth() + " " + date.getMonth().name());
                } else if (period.equals("PERIOD_LAST_YEAR") || period.equals("PERIOD_CUSTOM_YEAR")) {
                    dashboardKpiChartDataRedis.setLabel(date.getMonth().name() + " " + date.getYear());
                }

                if (header.equals("Agent")) {
                    dashboardKpiChartDataRedis.setName(awbWeighing.getAgentName());
                } else {
                    Awb awb = awbRepo.findFirstByCode(awbWeighing.getAwbCode());
                    dashboardKpiChartDataRedis.setName(awb.getAirline());
                }
                dashboardKpiChartDataRedis.setValue(awbWeighing.getChargeableWeight() != null ? awbWeighing.getChargeableWeight() : BigDecimal.valueOf(0));
                dashboardKpiChartDataRedisColl.add(dashboardKpiChartDataRedis);
            }

            result = dashboardKpiChartDataRedisColl
                    .stream()
                    .collect(
                            groupingBy(DashboardKpiChartDataRedis::getLabel, groupingBy(DashboardKpiChartDataRedis::getName,
                                    mapping(DashboardKpiChartDataRedis::getValue,
                                            reducing(BigDecimal.ZERO, BigDecimal::add)))));

            result2 = dashboardKpiChartDataRedisColl
                    .stream()
                    .collect(
                            groupingBy(DashboardKpiChartDataRedis::getName, groupingBy(DashboardKpiChartDataRedis::getLabel,
                                    mapping(DashboardKpiChartDataRedis::getValue,
                                            reducing(BigDecimal.ZERO, BigDecimal::add)))));
        } else if (achievementType.equals("WHM")) {
//            List<Awb> awbs1 = awbRepo.findAll().stream().filter(x -> x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());
//            List<Awb> awbManifest = awbs1.stream().filter(x -> x.getState().equals("MAN") || x.getState().equals("DEP") || x.getState().equals("ARR") || x.getState().equals("RCF")).collect(Collectors.toList());
//            awbManifest.sort(Comparator.comparing(o -> o.getCreatedDateTime()));


//            List<AwbLoading> awbLoadings = awbLoadingRepo.findAll().stream().filter(x -> x.getState().equals(1) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());
            Collection<AwbLoading> awbLoadings1 = awbLoadingRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndState(localDateTimeStart, localDateTimeEnd, 1);
            Collection<AwbLoading> awbLoadings2 = awbLoadings1.stream().filter(distinctByKey(x -> x.getAwb())).collect(Collectors.toList());

            List<AwbLoading> awbLoadings = awbLoadings2.stream().collect(toList());
            awbLoadings.sort(Comparator.comparing(o -> o.getCreatedDateTime()));
            List<Awb> awbLoadingsAwb = new LinkedList<>();
            for (AwbLoading awbLoading : awbLoadings) {
                Collection<Awb> awbs2 = awbRepo.findAllByCode(awbLoading.getAwb().getCode());
                for (Awb awb : awbs2) {
                    awbLoadingsAwb.add(awb);
                }
            }
            awbLoadingsAwb.sort(Comparator.comparing(o -> o.getCreatedDateTime()));


            Collection<DashboardKpiChartDataRedis> dashboardKpiChartDataRedisColl = new LinkedHashSet<>();
            for (Awb awbWHmanifest : awbLoadingsAwb) {
                DashboardKpiChartDataRedis dashboardKpiChartDataRedis = new DashboardKpiChartDataRedis();
                LocalDateTime date = awbWHmanifest.getCreatedDateTime();

                if (period.equals("PERIOD_LAST_WEEK")) {
                    dashboardKpiChartDataRedis.setLabel(date.getDayOfWeek().name() + " " + date.getMonth().name());
                } else if (period.equals("PERIOD_LAST_MONTH") || period.equals("PERIOD_CUSTOM_MONTH")) {
                    dashboardKpiChartDataRedis.setLabel(date.getDayOfMonth() + " " + date.getMonth().name());
                } else if (period.equals("PERIOD_LAST_YEAR") || period.equals("PERIOD_CUSTOM_YEAR")) {
                    dashboardKpiChartDataRedis.setLabel(date.getMonth().name() + " " + date.getYear());
                }

                if (header.equals("Agent")) {
                    dashboardKpiChartDataRedis.setName(awbWHmanifest.getAgentName());
                } else {
                    dashboardKpiChartDataRedis.setName(awbWHmanifest.getAirline());
                }
                dashboardKpiChartDataRedis.setValue(awbWHmanifest.getChargeableWeight() != null ? awbWHmanifest.getChargeableWeight() : BigDecimal.valueOf(0));
                dashboardKpiChartDataRedisColl.add(dashboardKpiChartDataRedis);
            }

            result = dashboardKpiChartDataRedisColl
                    .stream()
                    .collect(
                            groupingBy(DashboardKpiChartDataRedis::getLabel, groupingBy(DashboardKpiChartDataRedis::getName,
                                    mapping(DashboardKpiChartDataRedis::getValue,
                                            reducing(BigDecimal.ZERO, BigDecimal::add)))));

            result2 = dashboardKpiChartDataRedisColl
                    .stream()
                    .collect(
                            groupingBy(DashboardKpiChartDataRedis::getName, groupingBy(DashboardKpiChartDataRedis::getLabel,
                                    mapping(DashboardKpiChartDataRedis::getValue,
                                            reducing(BigDecimal.ZERO, BigDecimal::add)))));
        } else if (achievementType.equals("WHI")) {
//            List<WhInInvoice> whInInvoices = whInvoiceRepo.findAll().stream().filter(x -> x.getStatus().equals(1) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());
//            Collection<WhInInvoice> whInInvoices1 = whInvoiceRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(localDateTimeStart, localDateTimeEnd, 1);
//            List<WhInInvoice> whInInvoices = whInInvoices1.stream().collect(toList());
//            whInInvoices.sort(Comparator.comparing(o -> o.getCreatedDateTime()));

            Collection<AwbManManual> awbManManuals1 = awbManManualRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(localDateTimeStart, localDateTimeEnd, 1);
            List<AwbManManual> awbManManuals = awbManManuals1.stream().collect(toList());
            awbManManuals.sort(Comparator.comparing(o -> o.getCreatedDateTime()));

            Collection<DashboardKpiChartDataRedis> dashboardKpiChartDataRedisColl = new LinkedHashSet<>();
            for (AwbManManual awbManManual : awbManManuals) {
                DashboardKpiChartDataRedis dashboardKpiChartDataRedis = new DashboardKpiChartDataRedis();
                LocalDateTime date = awbManManual.getCreatedDateTime();

                if (period.equals("PERIOD_LAST_WEEK")) {
                    dashboardKpiChartDataRedis.setLabel(date.getDayOfWeek().name() + " " + date.getMonth().name());
                } else if (period.equals("PERIOD_LAST_MONTH") || period.equals("PERIOD_CUSTOM_MONTH")) {
                    dashboardKpiChartDataRedis.setLabel(date.getDayOfMonth() + " " + date.getMonth().name());
                } else if (period.equals("PERIOD_LAST_YEAR") || period.equals("PERIOD_CUSTOM_YEAR")) {
                    dashboardKpiChartDataRedis.setLabel(date.getMonth().name() + " " + date.getYear());
                }

                if (header.equals("Agent")) {
                    dashboardKpiChartDataRedis.setName(awbManManual.getAgentName());
                } else {
                    dashboardKpiChartDataRedis.setName(awbManManual.getAirline());
                }
                dashboardKpiChartDataRedis.setValue(awbManManual.getChargeableWeight() != null ? awbManManual.getChargeableWeight() : BigDecimal.valueOf(0));
                dashboardKpiChartDataRedisColl.add(dashboardKpiChartDataRedis);
            }

            result = dashboardKpiChartDataRedisColl
                    .stream()
                    .collect(
                            groupingBy(DashboardKpiChartDataRedis::getLabel, groupingBy(DashboardKpiChartDataRedis::getName,
                                    mapping(DashboardKpiChartDataRedis::getValue,
                                            reducing(BigDecimal.ZERO, BigDecimal::add)))));

            result2 = dashboardKpiChartDataRedisColl
                    .stream()
                    .collect(
                            groupingBy(DashboardKpiChartDataRedis::getName, groupingBy(DashboardKpiChartDataRedis::getLabel,
                                    mapping(DashboardKpiChartDataRedis::getValue,
                                            reducing(BigDecimal.ZERO, BigDecimal::add)))));
        }


        //kolom
        List<DataGrafiks> dataList = new ArrayList<>();
        List<DataGrafiks> dataListTemp = new ArrayList<>();
        DataGrafiks data1 = new DataGrafiks();
        data1.setId("");
        data1.setLabel("Day");
        data1.setPattern("");
        data1.setType("string");
        dataList.add(data1);

        for (Map.Entry<String, Map<String, BigDecimal>> entry : result2.entrySet()) {
            String key = entry.getKey();
            DataGrafiks data = new DataGrafiks();
            data.setId("");
            data.setLabel(key);
            data.setPattern("");
            data.setType("number");
            dataList.add(data);
            dataListTemp.add(data);
            DataGrafiks data2 = new DataGrafiks();
            data2.setType("string");
            data2.setRole("annotation");
            dataList.add(data2);

        }

        List<Rows> rowsList2 = new ArrayList<>();
        for (Map.Entry<String, Map<String, BigDecimal>> entry : result.entrySet()) {
            String key2 = entry.getKey();
            Map<String, BigDecimal> s = entry.getValue();
            List<Column> columnList2 = new ArrayList<>();
            Rows rows2 = new Rows();

            //set harinya
            Column column1 = new Column();
            column1.setV(key2);
            column1.setF(null);
            columnList2.add(column1);


            for (DataGrafiks dataGrafiks : dataListTemp) {
                boolean agent = s.keySet().contains(dataGrafiks.getLabel());
                if (agent) {
                    for (Map.Entry<String, BigDecimal> s2 : s.entrySet()) {
                        String key3 = s2.getKey();
                        BigDecimal s3 = s2.getValue();
                        if (key3.equals(dataGrafiks.getLabel())) {
                            Column column2 = new Column();
                            column2.setV(String.valueOf(s3));
                            column2.setF(null);
                            columnList2.add(column2);

                            Column column2a = new Column();
                            column2a.setV(String.valueOf(s3));
                            column2a.setF(null);
                            columnList2.add(column2a);
                        }
                    }

                } else {
                    Column column3 = new Column();
                    column3.setV(null);
                    column3.setF(null);
                    columnList2.add(column3);
                    Column column3a = new Column();
                    column3a.setV(null);
                    column3a.setF(null);
                    columnList2.add(column3a);
                }
            }

            rows2.setC(columnList2);
            rowsList2.add(rows2);

        }


        TableGrafik tableGrafik = new TableGrafik();
        tableGrafik.setCols(dataList);
        tableGrafik.setRows(rowsList2);
        ObjectMapper objectMapper2 = new ObjectMapper();
        try {
            dataJson = objectMapper2.writeValueAsString(tableGrafik);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return dataJson;
    }


    public InvoiceRaRedis invoiceRaRedisData(String agentName,Long agentId, BigDecimal weight) {
        InvoiceRaRedis invoiceRaRedis = new InvoiceRaRedis();
//        Collection<InvoiceRaRedis.InvoiceData> invoiceData = new LinkedList<>();
//        boolean existRaSetup = raCustomerInvoiceSetupRepo.existsByCustomerNameAndStatus(agentName, 1);

        InvoiceRaRedis.InvoiceSummary invoiceSummary = new InvoiceRaRedis.InvoiceSummary();

        BigDecimal ratesSum = BigDecimal.ZERO, ratesTotal = BigDecimal.ZERO;
        ratesSum = ratesRa(agentName);
        invoiceSummary.setRates(ratesSum);
        ratesTotal = ratesTotalRa(ratesSum, weight);
        invoiceSummary.setRatesTotal(ratesTotal);


        BigDecimal discountPercent = BigDecimal.ZERO;
        BigDecimal discount = BigDecimal.ZERO;
        BigDecimal ppn = BigDecimal.ZERO;
        BigDecimal ppnPercent = BigDecimal.ZERO;
        BigDecimal pph = BigDecimal.ZERO;
        BigDecimal dpp = BigDecimal.ZERO;
        BigDecimal pphPercent = BigDecimal.ZERO;


            RaCustomerInvoiceSetup raCustomerInvoiceSetup = raCustomerInvoiceSetupRepo.findFirstByCustomerIdAndStatus(agentId, 1);
        if (raCustomerInvoiceSetup != null) {
            if (raCustomerInvoiceSetup.getDiscountStatus().equals(1)) {
                discountPercent = raCustomerInvoiceSetup.getDiscountAmount();
                discount = ratesTotal.multiply(discountPercent).divide(BigDecimal.valueOf(100)).setScale(0, RoundingMode.HALF_UP);
//                ratesTotal = ratesTotal.subtract(discount);
            }

            if (raCustomerInvoiceSetup.getPpnStatus().equals(1)) {
                ppnPercent = raCustomerInvoiceSetup.getPpnAmount();
                ppn = ratesTotal.multiply(ppnPercent).divide(BigDecimal.valueOf(100)).setScale(0, RoundingMode.HALF_UP);
            }

            if (raCustomerInvoiceSetup.getPphStatus().equals(1)) {
                pphPercent = raCustomerInvoiceSetup.getPphAmount();
                pph = ratesTotal.multiply(pphPercent).divide(BigDecimal.valueOf(100)).setScale(0, RoundingMode.HALF_UP);
            }

            dpp = ratesTotal.subtract(discount);
        }
        BigDecimal total = dpp.add(ppn).subtract(pph);

        invoiceSummary.setDiscountPercent(discountPercent.setScale(0, RoundingMode.HALF_UP));
        invoiceSummary.setDiscount(discount.setScale(0, RoundingMode.HALF_UP));
        invoiceSummary.setSubTotal(dpp.setScale(0, RoundingMode.HALF_UP));
        invoiceSummary.setPpnPercent(ppnPercent.setScale(0, RoundingMode.HALF_UP));
        invoiceSummary.setPpn(ppn.setScale(0, RoundingMode.HALF_UP));
        invoiceSummary.setPphPercent(pphPercent.setScale(0, RoundingMode.HALF_UP));
        invoiceSummary.setPph(pph.setScale(0, RoundingMode.HALF_UP));
        invoiceSummary.setDpp(dpp.setScale(0, RoundingMode.HALF_UP));
        invoiceSummary.setTotal(total.setScale(0, RoundingMode.HALF_UP));

//        invoiceRaRedis.setInvoiceData(invoiceData);
        invoiceRaRedis.setInvoiceSummary(invoiceSummary);

        return invoiceRaRedis;
    }

    //ratesRa
    /*todo:
     * jika ra rates null
     *
     *
     *
     *
     * */
    BigDecimal ratesRa(String agentName) {
        BigDecimal ratesRa = BigDecimal.ZERO;
        LocalDate dateNow = LocalDate.now(DateTimeCfg.ZONE_ID);
        RaRates raRates = raRatesRepo.findFirstByAgentAndStartDateBeforeAndStopDateAfterAndStatus(agentName, dateNow, dateNow, 1);
        if (raRates != null) {
            ratesRa = raRates.getRates() != null ? raRates.getRates() : BigDecimal.valueOf(750);
        } else {
            ratesRa = BigDecimal.valueOf(750);
        }
        return ratesRa;
    }

    //ratesTotalRa
    BigDecimal ratesTotalRa(BigDecimal ratesSum, BigDecimal weight) {
        BigDecimal ratesTotalRa = BigDecimal.ZERO;

        ratesTotalRa = ratesSum.multiply(weight);
        return ratesTotalRa;
    }

    //subtotalRa
    BigDecimal subtotalRa(BigDecimal ratesTotalSum, BigDecimal ppnSum) {
        BigDecimal subtotalRa = BigDecimal.ZERO;
        subtotalRa = ratesTotalSum.add(ppnSum);
        return subtotalRa;
    }

    //totalRa
    BigDecimal totalRa(BigDecimal subTotal, BigDecimal pphSum) {
        BigDecimal totalRa = BigDecimal.ZERO;
        totalRa = subTotal.subtract(pphSum);
        return totalRa;
    }

    public AwbRedis mappingEntityToRedis(Awb awb) {
        AwbRedis awbRedis = new AwbRedis();
        BeanUtils.copyProperties(awb, awbRedis);

        awbRedis.setId(awb.getId());
        awbRedis.setCreatedById(awb.getCreatedBy() != null ? awb.getCreatedBy().getId() : null);
        awbRedis.setCreatedByName(awb.getCreatedBy() != null ? awb.getCreatedBy().getName() : null);
        awbRedis.setCreatedDateTime(awb.getCreatedDateTime());

        awbRedis.setModifiedById(awb.getModifiedBy() != null ? awb.getModifiedBy().getId() : null);
        awbRedis.setModifiedByName(awb.getModifiedBy() != null ? awb.getModifiedBy().getName() : null);
        awbRedis.setModifiedDateTime(awb.getModifiedDateTime());

        return awbRedis;
    }

    public Integer diffDays(LocalDateTime dateTime){
            int diffDays = 1;
            LocalDateTime arrivalTime = dateTime;
            LocalDateTime currentDateTime = LocalDateTime.now(DateTimeCfg.ZONE_ID);
            Duration duration = Duration.between(arrivalTime, currentDateTime);
            diffDays = (int) Math.abs(duration.toDays());
        return diffDays;
    }


    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
}