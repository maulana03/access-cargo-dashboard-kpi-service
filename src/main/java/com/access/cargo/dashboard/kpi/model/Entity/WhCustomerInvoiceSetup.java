package com.access.cargo.dashboard.kpi.model.Entity;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.id.enhanced.SequenceStyleGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@DynamicUpdate
@Table(name = "wh_customer_invoice_setup", schema = "public")
public class WhCustomerInvoiceSetup implements Serializable {
    private static final String ID = "ID";

    private static final String WH_CODE = "WH_CODE"; /*HLP*/
    private static final String WH_NAME = "WH_NAME"; /*HLP*/
    private static final String WH_TYPE = "WH_TYPE"; /*INCOMING, OUTGOING*/

    private static final String CUSTOMER_ID = "CUSTOMER_ID";
    private static final String CUSTOMER_NAME = "CUSTOMER_NAME";

    private static final String WH_CHARGES_STATUS = "WH_CHARGES_STATUS";
    private static final String WH_CHARGES_AMOUNT = "WH_CHARGES_AMOUNT";

    private static final String CARGO_CHARGES_STATUS = "CARGO_CHARGES_STATUS";
    private static final String CARGO_CHARGES_AMOUNT = "CARGO_CHARGES_AMOUNT";

    private static final String HF_CHARGES_STATUS = "HF_CHARGES_STATUS";
    private static final String HF_CHARGES_AMOUNT = "HF_CHARGES_AMOUNT";

    private static final String KADE_CHARGES_STATUS = "KADE_CHARGES_STATUS";
    private static final String KADE_CHARGES_AMOUNT = "KADE_CHARGES_AMOUNT";

    private static final String DOC_CHARGES_STATUS = "DOC_CHARGES_STATUS";
    private static final String DOC_CHARGES_AMOUNT = "DOC_CHARGES_AMOUNT";

    private static final String PPN_STATUS = "PPN_STATUS";
    private static final String PPN_AMOUNT = "PPN_AMOUNT";

    private static final String PPH_STATUS = "PPH_STATUS";
    private static final String PPH_AMOUNT = "PPH_AMOUNT";

    private static final String PAYMENT_TYPES = "PAYMENT_TYPES";
    /*
    * CASH
    * CREDIT
    * CASH,CREDIT
    *
    * */

    /**/
    private static final String STATUS = "STATUS";
    private static final String STATUS_DESC = "STATUS_DESC";
    private static final String CREATED_DATETIME = "CREATED_DATETIME";
    private static final String CREATED_BY = "CREATED_BY";
    private static final String MODIFIED_DATETIME = "MODIFIED_DATETIME";
    private static final String MODIFIED_BY = "MODIFIED_BY";
    private static final long serialVersionUID = -1639217265151040849L;

    /**/

    @Id
    @GeneratedValue(generator = "wh_customer_invoice_setup_seq-generator")
    @GenericGenerator(
            name = "wh_customer_invoice_setup_seq-generator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.SEQUENCE_PARAM, value = "wh_customer_invoice_setup_seq"),
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.INITIAL_PARAM, value = "1"),
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.INCREMENT_PARAM, value = "1")
            }
    )
    @Column(name = ID)
    private Long id;

    @Column(name = WH_CODE, length = 6)
    private String whCode;

    @Column(name = WH_NAME, length = 6)
    private String whName;

    @Column(name = WH_TYPE, length = 15)
    private String whType;

    @Column(name = CUSTOMER_ID)
    private Long customerId;

    @Column(name = CUSTOMER_NAME, length = 50)
    private String customerName;

    @Column(name = WH_CHARGES_STATUS)
    private Integer whChargesStatus;

    @Column(name = WH_CHARGES_AMOUNT, precision = 19, scale = 3)
    private BigDecimal whChargesAmount;
    @Column(name = CARGO_CHARGES_STATUS)
    private Integer whCargoChargesStatus;

    @Column(name = CARGO_CHARGES_AMOUNT, precision = 19, scale = 3)
    private BigDecimal whCargoChargesAmount;

    @Column(name = HF_CHARGES_STATUS)
    private Integer hfChargesStatus;

    @Column(name = HF_CHARGES_AMOUNT, precision = 19, scale = 3)
    private BigDecimal hfChargesAmount;

    @Column(name = KADE_CHARGES_STATUS)
    private Integer kadeChargesStatus;

    @Column(name = KADE_CHARGES_AMOUNT, precision = 19, scale = 3)
    private BigDecimal kadeChargesAmount;

    @Column(name = DOC_CHARGES_STATUS)
    private Integer docChargesStatus;

    @Column(name = DOC_CHARGES_AMOUNT, precision = 19, scale = 3)
    private BigDecimal docChargesAmount;

    @Column(name = PPN_STATUS)
    private Integer ppnStatus;

    @Column(name = PPN_AMOUNT, precision = 19, scale = 3)
    private BigDecimal ppnAmount;

    @Column(name = PPH_STATUS)
    private Integer pphStatus;

    @Column(name = PPH_AMOUNT, precision = 19, scale = 3)
    private BigDecimal pphAmount;

    @Column(name = PAYMENT_TYPES, length = 20)
    private String paymentTypes;


    /**/

    @Column(name = STATUS)
    private Integer status;

    @Column(name = STATUS_DESC, length = 30)
    private String statusDesc;

    @Column(name = CREATED_DATETIME)
    private LocalDateTime createdDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = CREATED_BY)
    private AccountUser createdBy;

    @Column(name = MODIFIED_DATETIME)
    private LocalDateTime modifiedDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = MODIFIED_BY)
    private AccountUser modifiedBy;

}
