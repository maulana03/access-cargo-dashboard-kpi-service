package com.access.cargo.dashboard.kpi.service;


import com.access.cargo.dashboard.kpi.model.Entity.AwbOffLoading;
import com.access.cargo.dashboard.kpi.model.redis.DashboardOffLoadingReportDataDetailRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardOffLoadingReportSummaryRedis;
import com.access.cargo.dashboard.kpi.repo.DashboardOffLoadingReportDataDetailRedisRepo;
import com.access.cargo.dashboard.kpi.repo.OffLoadingRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

@Slf4j
@Service
public class DashboardOffLoadingService {
    @Autowired
    DashboardOffLoadingReportDataDetailRedisRepo dashboardOffLoadingReportDataDetailRedisRepo;

    @Autowired
    OffLoadingRepo offLoadingRepo;

    public DashboardOffLoadingReportSummaryRedis getSummary(LocalDate dt1,LocalDate dt2,String agent,String branch){
        Collection<AwbOffLoading> awbOffLoadings;
        LocalDateTime localDateTimeStart = dt1.atTime(00,00, 00);
        LocalDateTime localDateTimeEnd = dt2.atTime(23,59, 59);
        if(agent.equals("ALL") && branch.equals("ALL")){
            awbOffLoadings = offLoadingRepo.findAll().stream().filter(x -> x.getState().equals(1) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());

        }else{
            awbOffLoadings = offLoadingRepo.findAll().stream().filter(x -> x.getState().equals(1) && x.getAwb().getAgentName().equals(agent) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());

        }

        Collection<AwbOffLoading> awbOffLoadingsFlight = awbOffLoadings.stream().filter(distinctByKey(x -> x.getAwb().getFlightNo())).collect(Collectors.toList());


        BigDecimal sumOffLoadingAwb = BigDecimal.ZERO;
        Integer sumOffLoadingPieces = 0;
        BigDecimal sumOffLoadingKg = BigDecimal.ZERO;

        for (AwbOffLoading amt : awbOffLoadings) {

//            sumOffLoadingAwb = sumOffLoadingAwb.add(BigDecimal.ZERO);
            Integer piecess = amt.getPiecesTotal() - amt.getPieces();
            sumOffLoadingPieces = Integer.sum(sumOffLoadingPieces,piecess);

            BigDecimal kgbagi =BigDecimal.ZERO;
            if(amt.getPieces()!=0){
                kgbagi = amt.getAwb().getChargeableWeight().divide(BigDecimal.valueOf(amt.getPieces()),3, RoundingMode.HALF_UP);

            }
            sumOffLoadingKg = sumOffLoadingKg.add(kgbagi);

        }



        DashboardOffLoadingReportSummaryRedis dashboardOffLoadingReportSummaryRedis =  new DashboardOffLoadingReportSummaryRedis();
        dashboardOffLoadingReportSummaryRedis.setOffLoadingFlight(BigDecimal.valueOf(awbOffLoadingsFlight.size()));
        dashboardOffLoadingReportSummaryRedis.setOffLoadingAwb(BigDecimal.valueOf(awbOffLoadings.size()));
        dashboardOffLoadingReportSummaryRedis.setOffLoadingPieces(sumOffLoadingPieces);
        dashboardOffLoadingReportSummaryRedis.setOffLoadingKg(sumOffLoadingKg);
        return dashboardOffLoadingReportSummaryRedis;
    }

    public Collection<DashboardOffLoadingReportDataDetailRedis> getDetail(LocalDate dt1,LocalDate dt2,String agent,String branch){
        Collection<DashboardOffLoadingReportDataDetailRedis> dashboardOffLoadingReportDataDetailRedis;
        Collection<AwbOffLoading> awbOffLoadings;
        LocalDateTime localDateTimeStart = dt1.atTime(00,00, 00);
        LocalDateTime localDateTimeEnd = dt2.atTime(23,59, 59);
        if(agent.equals("ALL") && branch.equals("ALL")){
            awbOffLoadings = offLoadingRepo.findAll().stream().filter(x -> x.getState().equals(1) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());

        }else{
            awbOffLoadings = offLoadingRepo.findAll().stream().filter(x -> x.getState().equals(1) && x.getAwb().getAgentName().equals(agent) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());

        }
        dashboardOffLoadingReportDataDetailRedis = mappingData(awbOffLoadings);
        return dashboardOffLoadingReportDataDetailRedis;
    }

    public Collection<DashboardOffLoadingReportDataDetailRedis> mappingData(Collection<AwbOffLoading> awbOffLoading){
        Collection<DashboardOffLoadingReportDataDetailRedis> dashboardOffLoadingReportDataDetailRedis =  new LinkedList<>();

//        Map<Object, List<AwbOffLoading>> byFlightNo = awbOffLoading.stream()
//                .collect(groupingBy(p -> p.getAwb().getFlightNo()));
//
//        for (Map.Entry<Object, List<AwbOffLoading>> entry : byFlightNo.entrySet()) {
//            Object getFlightNo = entry.getKey();
//
//            DashboardOffLoadingReportDataDetailRedis dashboardOffLoadingReportDataDetailRedis1 = new DashboardOffLoadingReportDataDetailRedis();
//            dashboardOffLoadingReportDataDetailRedis1.setFlight(getFlightNo.toString());
//
//            List<AwbOffLoading> awbOffLoading1 = entry.getValue();
//
//            for(AwbOffLoading awbOffLoading2:awbOffLoading1){
//
//            }
//        }

        for(AwbOffLoading awbOffLoading1:awbOffLoading){
            DashboardOffLoadingReportDataDetailRedis dashboardOffLoadingReportDataDetailRedis1 = new DashboardOffLoadingReportDataDetailRedis();
            dashboardOffLoadingReportDataDetailRedis1.setOffloadingTime(awbOffLoading1.getCreatedDateTime());
            dashboardOffLoadingReportDataDetailRedis1.setDate(awbOffLoading1.getAwb().getFlightDate());
            dashboardOffLoadingReportDataDetailRedis1.setFlight(awbOffLoading1.getAwb().getFlightNo());
            dashboardOffLoadingReportDataDetailRedis1.setRoute(awbOffLoading1.getAwb().getSector());
            dashboardOffLoadingReportDataDetailRedis1.setAgent(awbOffLoading1.getAwb().getAgentName());
            dashboardOffLoadingReportDataDetailRedis1.setOffLoadAwb(awbOffLoading1.getAwb().getCode());

            Integer piecess = awbOffLoading1.getPiecesTotal() - awbOffLoading1.getPieces();
            dashboardOffLoadingReportDataDetailRedis1.setOffLoadPieces(piecess);

            BigDecimal kgbagi =BigDecimal.ZERO;
            if(awbOffLoading1.getPieces()!=0) {
                kgbagi = awbOffLoading1.getAwb().getChargeableWeight().divide(BigDecimal.valueOf(awbOffLoading1.getPieces()), 3, RoundingMode.HALF_UP);
            }

            dashboardOffLoadingReportDataDetailRedis1.setOffLoadKg(kgbagi);


            dashboardOffLoadingReportDataDetailRedis.add(dashboardOffLoadingReportDataDetailRedis1);

        }

        return dashboardOffLoadingReportDataDetailRedis;
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

}