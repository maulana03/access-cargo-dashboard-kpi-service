package com.access.cargo.dashboard.kpi.model.wrapper;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@NoArgsConstructor
public class InvoiceHeader implements Serializable {
    private static final long serialVersionUID = -2120347088739428435L;

    String invoiceNo;
    LocalDate date;
    LocalDate dateBegin;
    LocalDate dateEnd;
    String office;
    String category;
    String whType;

    Long customerId;
    String customerName;
    String customerAddress;
    String customerPhone;
}
