package com.access.cargo.dashboard.kpi.model.Entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.id.enhanced.SequenceStyleGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@NoArgsConstructor
@Data
@Entity
@DynamicUpdate
@Table(name = "agent", schema = "public")
public class Agent implements Serializable {
    private static final String ID = "ID";
    private static final String CODE = "CODE";
    private static final String NAME = "NAME";
    private static final String IATA_CODE = "IATA_CODE";
    private static final String CASS_CODE = "CASS_CODE";
    private static final String ACCOUNT_NUMBER = "ACCOUNT_NUMBER";
    private static final String ADDRESS_LINE1 = "ADDRESS_LINE1";
    private static final String ADDRESS_LINE2 = "ADDRESS_LINE2";
    private static final String ADDRESS_LINE3 = "ADDRESS_LINE3";
    private static final String PO_BOX = "PO_BOX";
    private static final String POST_CODE = "POST_CODE";
    private static final String CITY = "CITY";
    private static final String COUNTRY = "COUNTRY";
    private static final String PHONE_NUMBER = "PHONE_NUMBER";
    private static final String WA_NUMBER = "WA_NUMBER";
    private static final String EMAIL_ADDRESS = "EMAIL_ADDRESS";
    private static final String PIC = "PIC";
    private static final String CP = "CP";
    private static final String PAYMENT_METHODS = "PAYMENT_METHODS";
    private static final String AGENT_ACCOUNT = "AGENT_ACCOUNT";
    /**/
    private static final String STATUS = "STATUS";
    private static final String STATUS_DESC = "STATUS_DESC";
    private static final String CREATED_DATETIME = "CREATED_DATETIME";
    private static final String CREATED_BY = "CREATED_BY";
    private static final String MODIFIED_DATETIME = "MODIFIED_DATETIME";
    private static final String MODIFIED_BY = "MODIFIED_BY";
    private static final long serialVersionUID = -6755102911035760944L;
    /**/
    @Id
    @GeneratedValue(generator = "agent_seq-generator")
    @GenericGenerator(
            name = "agent_seq-generator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.SEQUENCE_PARAM, value = "agent_seq"),
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.INITIAL_PARAM, value = "1"),
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.INCREMENT_PARAM, value = "1")
            }
    )
    @Column(name = ID)
    private Long id;

    @Column(name = CODE, length = 15)
    private String code;

    @Column(name = NAME, length = 50)
    private String name;

    @Column(name = IATA_CODE, length = 15)
    private String iataCode;

    @Column(name = CASS_CODE, length = 15)
    private String cassCode;

    @Column(name = ACCOUNT_NUMBER, length = 20)
    private String accountNumber;

    @Column(name = ADDRESS_LINE1, length = 50)
    private String addressLine1;

    @Column(name = ADDRESS_LINE2, length = 50)
    private String addressLine2;

    @Column(name = ADDRESS_LINE3, length = 50)
    private String addressLine3;

    @Column(name = POST_CODE, length = 10)
    private String postCode;

    @Column(name = PHONE_NUMBER, length = 20)
    private String phoneNumber;

    @Column(name = WA_NUMBER, length = 20)
    private String waNumber;

    @Column(name = EMAIL_ADDRESS, length = 50)
    private String emailAddress;

    @Column(name = PO_BOX, length = 25)
    private String poBox;

    @Column(name = PIC, length = 50)
    private String pic;

    @Column(name = CP, length = 25)
    private String cp;

    @Column(name = PAYMENT_METHODS, length = 30)
    private String paymentMethods; /*TODO Deposit_Cash | Deposit_Credit | Deposit_Cash, Deposit_Credit*/



    /**/

    @Column(name = STATUS)
    private Integer status;

    @Column(name = STATUS_DESC, length = 30)
    private String statusDesc;

    @Column(name = CREATED_DATETIME)
    private LocalDateTime createdDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = CREATED_BY)
    private AccountUser createdBy;

    @Column(name = MODIFIED_DATETIME)
    private LocalDateTime modifiedDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = MODIFIED_BY)
    private AccountUser modifiedBy;

}
