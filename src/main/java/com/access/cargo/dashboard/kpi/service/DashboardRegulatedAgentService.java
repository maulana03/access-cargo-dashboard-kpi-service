package com.access.cargo.dashboard.kpi.service;


import com.access.cargo.dashboard.kpi.model.Entity.Awb;
import com.access.cargo.dashboard.kpi.model.Entity.RaAcceptance;
import com.access.cargo.dashboard.kpi.model.redis.DashboardRegulatedAgentReportDataDetailRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardRegulatedAgentReportSummaryRedis;
import com.access.cargo.dashboard.kpi.model.redis.InvoiceRaRedis;
import com.access.cargo.dashboard.kpi.model.wrapper.AwbStateConstants;
import com.access.cargo.dashboard.kpi.repo.AwbRepo;
import com.access.cargo.dashboard.kpi.repo.DashboardRegulatedAgentReportDataDetailRedisRepo;
import com.access.cargo.dashboard.kpi.repo.RaAcceptanceRepo;
import com.access.cargo.dashboard.kpi.repo.RaPaymentRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.LinkedList;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DashboardRegulatedAgentService {
    @Autowired
    DashboardRegulatedAgentReportDataDetailRedisRepo dashboardRegulatedAgentReportDataDetailRedisRepo;

    @Autowired
    RaAcceptanceRepo raAcceptanceRepo;

    @Autowired
    AwbRepo awbRepo;

    @Autowired
    RaPaymentRepo raPaymentRepo;

    @Autowired
    DashboardKpiService dashboardKpiService;

    public DashboardRegulatedAgentReportSummaryRedis getSummary(LocalDate dt1, LocalDate dt2, String agent, String branch) {
        Collection<RaAcceptance> raAcceptances;
        LocalDateTime localDateTimeStart = dt1.atTime(00, 00, 00).minusSeconds(1);
        LocalDateTime localDateTimeEnd = dt2.atTime(23, 59, 59).plusSeconds(1);
        if (agent.equals("ALL") && branch.equals("ALL")) {
            raAcceptances = raAcceptanceRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(localDateTimeStart, localDateTimeEnd, 1);
            raAcceptances.removeIf(x -> x.getStatusDesc().equals("XRAY_REJECT"));
        } else {
            raAcceptances = raAcceptanceRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatusAndAgent(localDateTimeStart, localDateTimeEnd, 1, agent);
            raAcceptances.removeIf(x -> x.getStatusDesc().equals("XRAY_REJECT"));
        }


        Integer sumPieces = 0;
        BigDecimal sumKg = BigDecimal.ZERO;
        BigDecimal sumRevenue = BigDecimal.ZERO;

        for (RaAcceptance amt : raAcceptances) {

            sumPieces = Integer.sum(sumPieces, amt.getPiecesActual());

            BigDecimal weight = amt.getWeightActual();
//            Awb awb = awbRepo.findFirstByCode(amt.getAwb());
            weight = weight != null && weight.compareTo(BigDecimal.ZERO) > 0 ? weight : amt.getWeight();
            sumKg = sumKg.add(weight);

//            sumRevenue = sumRevenue.add(amt.getRatesTotal());

//            RaPayment raPayment = raPaymentRepo.findFirstByAwb(amt.getAwb());
//            if (raPayment != null) {
//                sumRevenue = sumRevenue.add(raPayment.getPaymentAmount() != null ? raPayment.getPaymentAmount() : BigDecimal.valueOf(0));
//            }
            InvoiceRaRedis invoiceRaRedis = dashboardKpiService.invoiceRaRedisData(amt.getAgent(), weight);
            sumRevenue = sumRevenue.add(invoiceRaRedis.getInvoiceSummary().getTotal());
        }

        DashboardRegulatedAgentReportSummaryRedis dashboardRegulatedAgentReportSummaryRedis = new DashboardRegulatedAgentReportSummaryRedis();
        dashboardRegulatedAgentReportSummaryRedis.setAwb(BigDecimal.valueOf(raAcceptances.size()));
        dashboardRegulatedAgentReportSummaryRedis.setPieces(sumPieces);
        dashboardRegulatedAgentReportSummaryRedis.setKg(sumKg.setScale(0, RoundingMode.HALF_UP));
        dashboardRegulatedAgentReportSummaryRedis.setRevenue(sumRevenue.setScale(0, RoundingMode.HALF_UP));
        return dashboardRegulatedAgentReportSummaryRedis;
    }

    public Collection<DashboardRegulatedAgentReportDataDetailRedis> getDetail(LocalDate dt1, LocalDate dt2, String agent, String branch) {
        Collection<DashboardRegulatedAgentReportDataDetailRedis> dashboardRegulatedAgentReportDataDetailRedis;
        Collection<RaAcceptance> raAcceptances;

        LocalDateTime localDateTimeStart = dt1.atTime(00, 00, 00).minusSeconds(1);
        LocalDateTime localDateTimeEnd = dt2.atTime(23, 59, 59).plusSeconds(1);

        long start = System.currentTimeMillis();
        if (agent.equals("ALL") && branch.equals("ALL")) {
            raAcceptances = raAcceptanceRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(localDateTimeStart, localDateTimeEnd, 1);
            raAcceptances.removeIf(x -> x.getStatusDesc().equals("XRAY_REJECT"));
        } else {
            raAcceptances = raAcceptanceRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatusAndAgent(localDateTimeStart, localDateTimeEnd, 1, agent);
            raAcceptances.removeIf(x -> x.getStatusDesc().equals("XRAY_REJECT"));
        }
        long end = System.currentTimeMillis();
        long total = end - start;
        System.out.println("total = " + total);

        dashboardRegulatedAgentReportDataDetailRedis = mappingData(raAcceptances);
        return dashboardRegulatedAgentReportDataDetailRedis;
    }

    public Collection<DashboardRegulatedAgentReportDataDetailRedis> mappingData(Collection<RaAcceptance> raAcceptances) {
        Collection<DashboardRegulatedAgentReportDataDetailRedis> dashboardSalesReportDataDetailRedis = new LinkedList<>();
        Collection<String> awbCodes = raAcceptances.stream().map(RaAcceptance::getAwb).collect(Collectors.toList());
        /*todo: distinct of awbCodes*/
        Collection<Awb> awbs = awbRepo.findAllByCodeIn(awbCodes);
        for (RaAcceptance raAcceptance : raAcceptances) {
            DashboardRegulatedAgentReportDataDetailRedis dashboardRegulatedAgentReportDataDetailRedis = new DashboardRegulatedAgentReportDataDetailRedis();
//            dashboardRegulatedAgentReportDataDetailRedis.setIdTranscation("xxxxx");
            dashboardRegulatedAgentReportDataDetailRedis.setAgent(raAcceptance.getAgent());
            dashboardRegulatedAgentReportDataDetailRedis.setAwb(raAcceptance.getAwb());
            Awb awb = awbRepo.findFirstByCode(raAcceptance.getAwb());/*todo*/

            if (awb != null && awb.getSrcRsv() != null && awb.getSrcRsv().equals(AwbStateConstants.SRC_RESERVATION)) {
                dashboardRegulatedAgentReportDataDetailRedis.setBookingPieces(BigDecimal.valueOf(awb.getPieces()) != null ? BigDecimal.valueOf(awb.getPieces()) : BigDecimal.valueOf(0));
                dashboardRegulatedAgentReportDataDetailRedis.setBookingKg(awb.getChargeableWeight().setScale(0, RoundingMode.HALF_UP) != null ? awb.getChargeableWeight().setScale(0, RoundingMode.HALF_UP) : BigDecimal.valueOf(0));

            } else {
                dashboardRegulatedAgentReportDataDetailRedis.setBookingPieces(BigDecimal.valueOf(0));
                dashboardRegulatedAgentReportDataDetailRedis.setBookingKg(BigDecimal.valueOf(0));

            }

            dashboardRegulatedAgentReportDataDetailRedis.setRaPieces(raAcceptance.getPiecesActual());
            BigDecimal weight = raAcceptance.getWeightActual();
            weight = weight != null && weight.compareTo(BigDecimal.ZERO) > 0 ? weight : raAcceptance.getWeight();
            dashboardRegulatedAgentReportDataDetailRedis.setRaKg(weight.setScale(0, RoundingMode.HALF_UP));
            dashboardRegulatedAgentReportDataDetailRedis.setXray(raAcceptance.getStatusDesc() != null ? raAcceptance.getStatusDesc() : "");
            dashboardRegulatedAgentReportDataDetailRedis.setAmount(raAcceptance.getRatesTotal().setScale(0, RoundingMode.HALF_UP));
            dashboardSalesReportDataDetailRedis.add(dashboardRegulatedAgentReportDataDetailRedis);
        }
        return dashboardSalesReportDataDetailRedis;
    }

}