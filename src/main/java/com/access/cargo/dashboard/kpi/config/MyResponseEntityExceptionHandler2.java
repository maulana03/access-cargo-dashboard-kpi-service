package com.access.cargo.dashboard.kpi.config;

import com.access.cargo.dashboard.kpi.model.wrapper.CommonResp;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.FeignException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;

@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
public class MyResponseEntityExceptionHandler2 extends ResponseEntityExceptionHandler {

    private ObjectMapper objectMapper = new ObjectMapper().disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

    @ExceptionHandler({FeignException.class})
    protected ResponseEntity<Object> handleFeignException(FeignException ex) {
        String responseContent = ex.contentUTF8();
        CommonResp commonResp;

        try {
            commonResp = objectMapper.readValue(responseContent, CommonResp.class);
        } catch (JsonProcessingException e) {
            log.error("handleFeignException.JsonProcessingException: ", e);
            return new ResponseEntity<>(new CommonResp(ex.getLocalizedMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        log.error("handleFeignException: ", ex);
        if (commonResp.getMsg() == null) commonResp.setMsg(ex.getLocalizedMessage());
        return new ResponseEntity<>(commonResp, HttpStatus.valueOf(ex.status()));
    }

    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException ex) {
        log.error("handleConstraintViolationException: ", ex);
        CommonResp resp = new CommonResp();
        ex.getConstraintViolations().forEach(constraintViolation -> {
            String message = constraintViolation.getMessage();
            String propertyPath = constraintViolation.getPropertyPath().toString();
            String properties[] = propertyPath.split("\\.");
            if (properties.length > 1) propertyPath = properties[properties.length - 1];
            resp.setMsg(propertyPath + " = " + message);
            if (!propertyPath.isEmpty()) return;
        });
        return new ResponseEntity<>(resp, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.error("handleMethodArgumentNotValid: ", ex);
        StringBuilder errors = new StringBuilder();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.append(fieldName + " = " + errorMessage + "; ");
        });
        CommonResp commonResp = new CommonResp(errors.toString());
        return new ResponseEntity<>(commonResp, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleDefaultException(Exception ex) {
        log.error("handleDefaultException: ", ex);
        return new ResponseEntity<>(new CommonResp(ex.getLocalizedMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
    