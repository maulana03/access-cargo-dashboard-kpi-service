package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.redis.DashboardKpiSummaryRedis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DashboardKpiSummaryRedisRepo extends JpaRepository<DashboardKpiSummaryRedis, String> {
}
