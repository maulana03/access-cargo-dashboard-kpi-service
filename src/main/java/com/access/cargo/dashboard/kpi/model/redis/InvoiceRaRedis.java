package com.access.cargo.dashboard.kpi.model.redis;

import com.access.cargo.dashboard.kpi.model.wrapper.InvoiceCorporateData;
import com.access.cargo.dashboard.kpi.model.wrapper.InvoiceHeader;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;

@Data
@NoArgsConstructor
@RedisHash("InvoiceRaRedis")
public class InvoiceRaRedis implements Serializable {

    private static final long serialVersionUID = -2544701818479735810L;
    @Id
    Long id;

    InvoiceCorporateData corporateData;
    InvoiceHeader invoiceHeader;
    Collection<InvoiceData> invoiceData;
    InvoiceSummary invoiceSummary;

    /*todo:
    * list AWB
    * list RaPayment by list AWB
    * RaRates
    *
    *
    * */

    @Data
    public static class InvoiceData {
        LocalDate date; /*current-date*/
        String csdNo; /*AWB >> RaXray.awb >> RaXrayCsd.raXrayId  --> low-priority*/
        String awb;
        String airline;
        String flightNo;
        String route;
        String commodity;
        Integer pieces;
        BigDecimal grossWeight;
        BigDecimal volumeWeight;
        BigDecimal chargeableWeight;
    }


    @Data
    public static class InvoiceSummary {
        BigDecimal pieces;
        BigDecimal grossWeight;
        BigDecimal volumeWeight;
        BigDecimal chargeableWeight;
        BigDecimal rates;
        BigDecimal ratesTotal; /* rates x chargeableWeight */
        BigDecimal discountPercent; /* 39.394% */
        BigDecimal discount;
        BigDecimal ppnPercent = new BigDecimal(0.1);
        BigDecimal ppn;
        BigDecimal pphPercent = new BigDecimal(0.02);
        BigDecimal pph;
        BigDecimal dpp;
        BigDecimal subTotal;
        BigDecimal total;
        String totalInWord;
    }
}
