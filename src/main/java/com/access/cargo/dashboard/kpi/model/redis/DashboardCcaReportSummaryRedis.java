package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@RedisHash("DashboardCcaReportSummaryRedis")
public class DashboardCcaReportSummaryRedis implements Serializable {
    private static final long serialVersionUID = 8362618780929206415L;
    @Id
    Long id;

//    DashboardReportParams params;
    BigDecimal awbCCA;
    BigDecimal kgCCAPlus;
    BigDecimal kgCCAMinus;
    BigDecimal totalCCA;
}
