package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.redis.DashboardRegulatedAgentReportDataDetailRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardSalesReportDataDetailRedis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DashboardRegulatedAgentReportDataDetailRedisRepo extends JpaRepository<DashboardRegulatedAgentReportDataDetailRedis, String> {
}
