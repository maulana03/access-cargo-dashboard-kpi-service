package com.access.cargo.dashboard.kpi.model.Entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "awb_build_up")
public class AwbBuildUp implements Serializable {

    private static final long serialVersionUID = 6548687611371327643L;

    @Id
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "awb", referencedColumnName = "id")
    private Awb awb;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "flight_sch")
    private FlightList flightSch;

    @Column(name = "status_complete")
    private Integer statusComplete;

    /**/

    @Column(name = "status_confirmation")
    private Integer statusConfirmation;

    @Column(name = "count_confirmation")
    private Integer countConfirmation;

    @Column(name = "loading_status")
    private Integer loadingStatus;

    @Column(name = "loading_count")
    private Integer loadingCount;

    /**/

    @Column(name = "pieces", columnDefinition = "integer default 0")
    private Integer pieces = 0;

    @Column(name = "pieces_total", columnDefinition = "integer default 0")
    private Integer piecesTotal = 0;  // from acceptancePieces

    @Column(name = "shipment_type", length = 1)
    private String shipmentType; /*T:P*/

    @Column(name = "weight", precision = 19, scale = 3)
    private BigDecimal weight;

    @Column(name = "weight_total", precision = 19, scale = 3)
    private BigDecimal weightTotal;

    @Column(name = "flight_number", length = 1)
    private String flightNumber;

    /**/

    @Column(name = "state")
    private Integer state;

    @Column(name = "created_datetime")
    private LocalDateTime createdDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by")
    private AccountUser createdBy;

    @Column(name = "modified_datetime")
    private LocalDateTime modifiedDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "modified_by")
    private AccountUser modifiedBy;
}
