package com.access.cargo.dashboard.kpi.model.Entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "reservation", schema = "public")
public class Reservation implements Serializable {

    private static final long serialVersionUID = 6828466569577559103L;
    @Id
    @Column(name = "id")
    private Long id;


    @Column(name = "flight_date")
    private LocalDate flightDate;

    @Column(name = "chargeable_weight")
    private BigDecimal chargeableWeight;

    @Column(name = "rates_total")
    private BigDecimal ratesTotal;

    @Column(name = "status")
    private Integer status;

    @Column(name = "status_desc")
    private String statusDesc;

    @Column(name = "agent_name")
    private String agentName;

    @Column(name = "airline")
    private String airline;

    @Column(name = "modified_datetime")
    private LocalDateTime modifiedDateTime;

    @Column(name = "pieces")
    private Integer pieces;

    @Column(name = "code")
    private String code;

    @Column(name = "awb")
    private String awb;

}
