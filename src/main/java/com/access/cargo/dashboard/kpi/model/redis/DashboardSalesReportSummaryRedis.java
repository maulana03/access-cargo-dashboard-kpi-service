package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@RedisHash("DashboardSalesReportSummaryRedis")
public class DashboardSalesReportSummaryRedis implements Serializable {

    private static final long serialVersionUID = 2232647050353216004L;
    @Id
    Long id;

//    DashboardReportParams params;
    BigDecimal awbIssued;
    Integer piecesIssued;
    BigDecimal kgIssued;
    BigDecimal sales;
}
