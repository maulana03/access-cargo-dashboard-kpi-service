package com.access.cargo.dashboard.kpi.model.wrapper.tes;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class Cols1 {
    public List<Data> cols;
}


