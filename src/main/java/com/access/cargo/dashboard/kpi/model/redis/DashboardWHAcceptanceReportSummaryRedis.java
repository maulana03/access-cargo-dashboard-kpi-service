package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class DashboardWHAcceptanceReportSummaryRedis implements Serializable {
    private static final long serialVersionUID = -1833063216673413954L;
    @Id
    Long id;

    BigDecimal awbAccepted;
    Integer piecesAccepted;
    BigDecimal kgAccepted;

    BigDecimal awbRejected;
    Integer piecesRejected;
    BigDecimal kgRejected;
}
