package com.access.cargo.dashboard.kpi.service;


import com.access.cargo.dashboard.kpi.model.Entity.Awb;
import com.access.cargo.dashboard.kpi.model.redis.DashboardSalesReportDataDetailRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardSalesReportSummaryRedis;
import com.access.cargo.dashboard.kpi.model.wrapper.AwbStateConstants;
import com.access.cargo.dashboard.kpi.repo.AwbRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.LinkedList;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DashboardSalesService {

    @Autowired
    AwbRepo awbRepo;

    public DashboardSalesReportSummaryRedis getSummary(LocalDate dt1,LocalDate dt2,String agent,String branch){
        LocalDateTime localDateTimeStart = dt1.atTime(00, 00, 00).minusSeconds(1);
        LocalDateTime localDateTimeEnd = dt2.atTime(23, 59, 59).plusSeconds(1);

        Collection<Awb> awbs;
//        Collection<Awb> awbs1 = awbRepo.findAll().stream().filter(x -> x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());

        //booking
        String[] state1NotList = {AwbStateConstants.VOID, AwbStateConstants.CANCELED, AwbStateConstants.REJECTED, AwbStateConstants.OFFLOAD, AwbStateConstants.OFFLOAD};
        Collection<Awb> awbs1 = awbRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndSrcRsvAndState1NotIn(localDateTimeStart, localDateTimeEnd,AwbStateConstants.SRC_RESERVATION, state1NotList);

        if(agent.equals("ALL")){
            awbs = awbs1;
        }else{
            awbs = awbs1.stream().filter(x -> x.getAgentName().equals(agent)).collect(Collectors.toList());
        }

        Integer sumPiecesIssued = 0;
        BigDecimal sumKgIssued = BigDecimal.ZERO;
        BigDecimal sumsales= BigDecimal.ZERO;
        for (Awb amt : awbs) {
            sumPiecesIssued = Integer.sum(sumPiecesIssued,amt.getPieces() != null ? amt.getPieces():1);
            sumKgIssued = sumKgIssued.add(amt.getChargeableWeight() != null ? amt.getChargeableWeight():BigDecimal.valueOf(0));
            sumsales = sumsales.add(amt.getRatesTotal() != null ? amt.getRatesTotal():BigDecimal.valueOf(0));

        }

        DashboardSalesReportSummaryRedis dashboardSalesReportSummaryRedis =  new DashboardSalesReportSummaryRedis();
        dashboardSalesReportSummaryRedis.setAwbIssued(BigDecimal.valueOf(awbs.size()));
        dashboardSalesReportSummaryRedis.setPiecesIssued(sumPiecesIssued);
        dashboardSalesReportSummaryRedis.setKgIssued(sumKgIssued);
        dashboardSalesReportSummaryRedis.setSales(sumsales);
        return dashboardSalesReportSummaryRedis;
    }

    public Collection<DashboardSalesReportDataDetailRedis> getDetail(LocalDate dt1,LocalDate dt2,String agent,String branch){
        Collection<DashboardSalesReportDataDetailRedis> dashboardSalesReportDataDetailRedis;
        LocalDateTime localDateTimeStart = dt1.atTime(00, 00, 00).minusSeconds(1);
        LocalDateTime localDateTimeEnd = dt2.atTime(23, 59, 59).plusSeconds(1);

        Collection<Awb> awbs;
        //booking
        String[] state1NotList = {AwbStateConstants.VOID, AwbStateConstants.CANCELED, AwbStateConstants.REJECTED, AwbStateConstants.OFFLOAD, AwbStateConstants.OFFLOAD};
        Collection<Awb> awbs1 = awbRepo.findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndSrcRsvAndState1NotIn(localDateTimeStart, localDateTimeEnd,AwbStateConstants.SRC_RESERVATION, state1NotList);

        if(agent.equals("ALL")){
            awbs = awbs1;
        }else{
            awbs = awbs1.stream().filter(x -> x.getAgentName().equals(agent)).collect(Collectors.toList());
        }

        dashboardSalesReportDataDetailRedis = mappingData(awbs);
        return dashboardSalesReportDataDetailRedis;
    }

    public Collection<DashboardSalesReportDataDetailRedis> mappingData(Collection<Awb> awb){
        Collection<DashboardSalesReportDataDetailRedis> dashboardSalesReportDataDetailRedis = new LinkedList<>();
        for(Awb awb1 : awb){
            DashboardSalesReportDataDetailRedis dashboardSalesReportDataDetailRedis1 = new DashboardSalesReportDataDetailRedis();
            dashboardSalesReportDataDetailRedis1.setIdTranscation("xxxxx");
            dashboardSalesReportDataDetailRedis1.setAgent(awb1.getAgentName());
            dashboardSalesReportDataDetailRedis1.setAwb(awb1.getCode());
            dashboardSalesReportDataDetailRedis1.setPieces(awb1.getPieces());
            dashboardSalesReportDataDetailRedis1.setKg(awb1.getChargeableWeight());
            dashboardSalesReportDataDetailRedis1.setTransaction("Purchase");
            dashboardSalesReportDataDetailRedis1.setTransactionTypeCode("C");
            dashboardSalesReportDataDetailRedis1.setAmount(awb1.getRatesTotal());
            dashboardSalesReportDataDetailRedis1.setDate(awb1.getCreatedDateTime());
            dashboardSalesReportDataDetailRedis1.setStatus(awb1.getState1());
            dashboardSalesReportDataDetailRedis.add(dashboardSalesReportDataDetailRedis1);
        }
    return dashboardSalesReportDataDetailRedis;
    }
}