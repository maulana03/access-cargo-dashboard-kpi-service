package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.redis.DashboardTopUpReportDataDetailRedis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DashboardTopUpReportDataDetailRedisRepo extends JpaRepository<DashboardTopUpReportDataDetailRedis, String> {
}
