package com.access.cargo.dashboard.kpi.service;


import com.access.cargo.dashboard.kpi.model.Entity.AwbAcceptance;
import com.access.cargo.dashboard.kpi.model.Entity.RaAcceptance;
import com.access.cargo.dashboard.kpi.model.redis.DashboardWHAcceptanceReportDataDetailRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardWHAcceptanceReportSummaryRedis;
import com.access.cargo.dashboard.kpi.repo.AwbAcceptanceRepo;
import com.access.cargo.dashboard.kpi.repo.DashboardWHAcceptanceReportDataDetailRedisRepo;
import com.access.cargo.dashboard.kpi.repo.RaAcceptanceRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.LinkedList;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DashboardWHAcceptanceService {
    @Autowired
    DashboardWHAcceptanceReportDataDetailRedisRepo dashboardWHAcceptanceReportDataDetailRedisRepo;

    @Autowired
    AwbAcceptanceRepo awbAcceptanceRepo;

    @Autowired
    RaAcceptanceRepo raAcceptanceRepo;

    public DashboardWHAcceptanceReportSummaryRedis getSummary(LocalDate dt1,LocalDate dt2,String agent,String branch){
        Collection<AwbAcceptance> awbAcceptances;
        LocalDateTime localDateTimeStart = dt1.atTime(00,00, 00);
        LocalDateTime localDateTimeEnd = dt2.atTime(23,59, 59);

        if(agent.equals("ALL") && branch.equals("ALL")){
            awbAcceptances = awbAcceptanceRepo.findAll().stream().filter(x -> x.getState().equals(1) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());

        }else{
            awbAcceptances = awbAcceptanceRepo.findAll().stream().filter(x -> x.getState().equals(1) && x.getAwb().getAgentName().equals(agent) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());

        }


        BigDecimal sumPiecesBk = BigDecimal.ZERO;
        BigDecimal sumKgBk = BigDecimal.ZERO;

        BigDecimal sumPiecesRa = BigDecimal.ZERO;
        BigDecimal sumKgRa = BigDecimal.ZERO;

        Integer sumPiecesAccepted = 0;
        BigDecimal sumKgAccepted = BigDecimal.ZERO;

        Integer sumPiecesRejected = 0;
        BigDecimal sumKgRejected = BigDecimal.ZERO;

        for (AwbAcceptance amt : awbAcceptances) {
            sumPiecesBk = sumPiecesBk.add(amt.getAwb().getChargeableWeight() != null ? amt.getAwb().getChargeableWeight():BigDecimal.valueOf(0));
            sumKgBk = sumKgBk.add(amt.getAwb().getRatesTotal() != null ? amt.getAwb().getRatesTotal():BigDecimal.valueOf(0));
            RaAcceptance raAcceptance =raAcceptanceRepo.findFirstByAwb(amt.getAwb().getCode());
            if(raAcceptance != null){
                sumPiecesRa = sumPiecesRa.add(raAcceptance.getWeightActual() != null ? raAcceptance.getWeightActual():BigDecimal.valueOf(0));
                sumKgRa = sumKgRa.add(raAcceptance.getRatesTotal() != null ? raAcceptance.getRatesTotal():BigDecimal.valueOf(0));

            }

            sumPiecesAccepted = Integer.sum(sumPiecesAccepted,amt.getPieces() != null ? amt.getPieces():0);

            BigDecimal kgAccptbagi = BigDecimal.valueOf(0);
            if(amt.getPieces() != 0){
                kgAccptbagi = amt.getAwb().getChargeableWeight().divide(BigDecimal.valueOf(amt.getPieces()),3, RoundingMode.HALF_UP);
            }
            BigDecimal kgAccptKali = kgAccptbagi.multiply(amt.getAwb().getRatesTotal());
            sumKgAccepted = sumKgAccepted.add(kgAccptKali);

            Integer reject = amt.getPiecesTotal() - amt.getPieces();
            sumPiecesRejected = Integer.sum(sumPiecesRejected,reject);

            BigDecimal kgRejectbagi = BigDecimal.valueOf(0);
            if(reject != 0){
               kgRejectbagi = amt.getAwb().getChargeableWeight().divide(BigDecimal.valueOf(reject),3, RoundingMode.HALF_UP);
            }

            BigDecimal kgRejectKali = kgRejectbagi.multiply(amt.getAwb().getRatesTotal());
            sumKgRejected = sumKgRejected.add(kgRejectKali);
        }

        DashboardWHAcceptanceReportSummaryRedis dashboardWHAcceptanceReportSummaryRedis =  new DashboardWHAcceptanceReportSummaryRedis();

        dashboardWHAcceptanceReportSummaryRedis.setAwbAccepted(BigDecimal.valueOf(sumPiecesAccepted));
        dashboardWHAcceptanceReportSummaryRedis.setPiecesAccepted(sumPiecesAccepted);
        dashboardWHAcceptanceReportSummaryRedis.setKgAccepted(sumKgAccepted);
        dashboardWHAcceptanceReportSummaryRedis.setAwbRejected(BigDecimal.valueOf(sumPiecesRejected));
        dashboardWHAcceptanceReportSummaryRedis.setPiecesRejected(sumPiecesRejected);
        dashboardWHAcceptanceReportSummaryRedis.setKgRejected(sumKgRejected);
        return dashboardWHAcceptanceReportSummaryRedis;
    }

    public Collection<DashboardWHAcceptanceReportDataDetailRedis> getDetail(LocalDate dt1,LocalDate dt2,String agent,String branch){
        Collection<DashboardWHAcceptanceReportDataDetailRedis> dashboardWHAcceptanceReportDataDetailRedis;
        Collection<AwbAcceptance> awbAcceptances;

        LocalDateTime localDateTimeStart = dt1.atTime(00,00, 00);
        LocalDateTime localDateTimeEnd = dt2.atTime(23,59, 59);

        if(agent.equals("ALL") && branch.equals("ALL")){
            awbAcceptances = awbAcceptanceRepo.findAll().stream().filter(x -> x.getState().equals(1) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());

        }else{
            awbAcceptances = awbAcceptanceRepo.findAll().stream().filter(x -> x.getState().equals(1) && x.getAwb().getAgentName().equals(agent) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());

        }
        dashboardWHAcceptanceReportDataDetailRedis = mappingData(awbAcceptances);
        return dashboardWHAcceptanceReportDataDetailRedis;
    }

    public Collection<DashboardWHAcceptanceReportDataDetailRedis> mappingData(Collection<AwbAcceptance> awbAcceptances){
        Collection<DashboardWHAcceptanceReportDataDetailRedis> dashboardWHAcceptanceReportDataDetailRedisCollection = new LinkedList<>();

        for(AwbAcceptance awbAcceptance : awbAcceptances){
            DashboardWHAcceptanceReportDataDetailRedis dashboardWHAcceptanceReportDataDetailRedis = new DashboardWHAcceptanceReportDataDetailRedis();
//            dashboardWHAcceptanceReportDataDetailRedis.setIdTranscation("");
            dashboardWHAcceptanceReportDataDetailRedis.setAgent(awbAcceptance.getAwb().getAgentName());
            dashboardWHAcceptanceReportDataDetailRedis.setAwb(awbAcceptance.getAwb().getCode());
            dashboardWHAcceptanceReportDataDetailRedis.setBookingPieces(awbAcceptance.getAwb().getChargeableWeight());
            dashboardWHAcceptanceReportDataDetailRedis.setBookingKg(awbAcceptance.getAwb().getRatesTotal());

            RaAcceptance raAcceptance =raAcceptanceRepo.findFirstByAwb(awbAcceptance.getAwb().getCode());
            if(raAcceptance != null){

                dashboardWHAcceptanceReportDataDetailRedis.setRaPieces(raAcceptance.getPiecesActual() != null ? raAcceptance.getPiecesActual():0);
                dashboardWHAcceptanceReportDataDetailRedis.setRaKg(raAcceptance.getRatesTotal() != null ?raAcceptance.getRatesTotal() :BigDecimal.valueOf(0));
            }else{

                dashboardWHAcceptanceReportDataDetailRedis.setRaPieces(0);
                dashboardWHAcceptanceReportDataDetailRedis.setRaKg(BigDecimal.valueOf(0));
            }

            dashboardWHAcceptanceReportDataDetailRedis.setAcceptedPieces(awbAcceptance.getPieces());

            BigDecimal kgAccptbagi = BigDecimal.valueOf(0);
            if(awbAcceptance.getPieces() != 0){
                kgAccptbagi = awbAcceptance.getAwb().getChargeableWeight().divide(BigDecimal.valueOf(awbAcceptance.getPieces()),3, RoundingMode.HALF_UP);
            }

            BigDecimal kgAccptKali = kgAccptbagi.multiply(awbAcceptance.getAwb().getRatesTotal());
            dashboardWHAcceptanceReportDataDetailRedis.setAcceptedKg(kgAccptKali);

            Integer reject = awbAcceptance.getPiecesTotal() - awbAcceptance.getPieces();
            dashboardWHAcceptanceReportDataDetailRedis.setRejectedPieces(reject);

            BigDecimal kgRejectbagi = BigDecimal.valueOf(0);
            if(reject != 0){
                kgRejectbagi = awbAcceptance.getAwb().getChargeableWeight().divide(BigDecimal.valueOf(reject),3, RoundingMode.HALF_UP);
            }

            BigDecimal kgRejectKali = kgRejectbagi.multiply(awbAcceptance.getAwb().getRatesTotal());
            dashboardWHAcceptanceReportDataDetailRedis.setRejectedKg(kgRejectKali);

            dashboardWHAcceptanceReportDataDetailRedisCollection.add(dashboardWHAcceptanceReportDataDetailRedis);
        }

        return dashboardWHAcceptanceReportDataDetailRedisCollection;
    }

}