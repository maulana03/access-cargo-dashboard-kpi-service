package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@RedisHash("DashboardManifestReportDetailRedis")
public class DashboardManifestReportDataDetailRedis implements Serializable {

    private static final long serialVersionUID = 6742493213036753794L;
    @Id
    Long id;
    Long flightId;
    String flight; /* airlineCode+flightNo */
    String flightNo;
    String airlineCode;
    String route;
    Integer preManAwb = 0;
    Integer preManPieces = 0;
    BigDecimal preManKg = BigDecimal.ZERO;
    Integer manifestAwb = 0;
    Integer manifestPieces = 0;
    BigDecimal manifestKg = BigDecimal.ZERO;
    Integer offLoadAwb = 0;
    Integer offLoadPieces = 0;
    BigDecimal offLoadKg = BigDecimal.ZERO;
    LocalDateTime manifestTime;
    String branch;

}

