package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.LinkedList;

@Data
@NoArgsConstructor
@RedisHash("DashboardKpiTableRedis")
public class DashboardKpiTableRedis implements Serializable {

    private static final long serialVersionUID = -5988425928394304170L;

    @Id
    Long id;
    DashboardKpiParams params;

    String header; /*Agent, Airline*/
    LinkedList<DashboardKpiTableDataRedis> data;

}
