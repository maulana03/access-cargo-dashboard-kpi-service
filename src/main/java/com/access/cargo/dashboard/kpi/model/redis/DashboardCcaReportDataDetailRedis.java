package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@RedisHash("DashboardCcaReportDetailRedis")
public class DashboardCcaReportDataDetailRedis implements Serializable {
    private static final long serialVersionUID = -4730080667448799580L;
    @Id
    Long id;
    String idTranscation;
    String agent;
    String awb;
    BigDecimal kgBook;
    BigDecimal kgCca;
    String transaction;
    String transactionTypeCode;
    BigDecimal amount;
//    LocalDateTime dateTime;

    LocalDate date;
    String status;
    String branch;

}

