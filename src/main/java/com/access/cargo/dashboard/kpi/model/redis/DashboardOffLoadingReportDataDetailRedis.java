package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@RedisHash("DashboardOffLoadingReportDetailRedis")
public class DashboardOffLoadingReportDataDetailRedis implements Serializable {

    private static final long serialVersionUID = -8447936246859377198L;
    @Id
    Long id;
    String flight;
    String route;
    String agent;
//    BigDecimal offLoadAwb;
    String offLoadAwb;
    Integer offLoadPieces;
    BigDecimal offLoadKg;
    LocalDate date;
    LocalDateTime offloadingTime;
    String branch;

}

