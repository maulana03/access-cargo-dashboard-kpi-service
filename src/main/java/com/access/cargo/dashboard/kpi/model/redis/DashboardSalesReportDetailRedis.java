package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.LinkedList;

@Data
@NoArgsConstructor
@RedisHash("DashboardSalesReportDetailRedis")
public class DashboardSalesReportDetailRedis implements Serializable {

    private static final long serialVersionUID = -4595051474231785472L;
    @Id
    Long id;

    DashboardReportParams params;
    DashboardSalesReportDataDetailRedis data;
}
