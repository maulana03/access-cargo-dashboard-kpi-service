package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class DashboardOffLoadingReportSummaryRedis implements Serializable {
    private static final long serialVersionUID = -6864907538540331256L;
    @Id
    Long id;

    BigDecimal offLoadingFlight;
    BigDecimal offLoadingAwb;
    Integer offLoadingPieces;
    BigDecimal offLoadingKg;
}
