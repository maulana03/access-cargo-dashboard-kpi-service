package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.Entity.RaCustomerInvoiceSetup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDateTime;

public interface RaCustomerInvoiceSetupRepo extends PagingAndSortingRepository<RaCustomerInvoiceSetup, Long> {
   boolean existsByCustomerNameAndStatus(String agentName,Integer status);
   boolean existsByCustomerIdAndStatus(Long agentId,Integer status);
   boolean existsByIdAndStatus(Long id,Integer status);
   RaCustomerInvoiceSetup findFirstByCustomerNameAndStatus(String code,Integer status);
   RaCustomerInvoiceSetup findFirstByIdAndStatus(Long id,Integer status);
   RaCustomerInvoiceSetup findFirstByCustomerIdAndStatus(Long agentId,Integer status);
   boolean existsByCustomerIdAndStatus(long custId,Integer status);

   boolean existsByCustomerIdAndIdNotAndStatus(Long custId, Long excludeId,Integer status);


   Page<RaCustomerInvoiceSetup> findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndCustomerNameAndStatus(LocalDateTime createDateTime, LocalDateTime createDateTime2, String agentNmae, Integer status, Pageable pageable);
   Page<RaCustomerInvoiceSetup> findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(LocalDateTime createDateTime, LocalDateTime createDateTime2, Integer status, Pageable pageable);
   Page<RaCustomerInvoiceSetup> findAllByCustomerNameAndStatus(String agentName,Integer status,Pageable pageable);
   Page<RaCustomerInvoiceSetup> findAllByStatus(Integer status,Pageable pageable);
   }
