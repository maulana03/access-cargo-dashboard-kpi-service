package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.Entity.RaAcceptance;
import com.access.cargo.dashboard.kpi.model.Entity.WhOutInvoice;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Collection;

@Repository
public interface WhOutInvoiceRepo extends PagingAndSortingRepository<WhOutInvoice, Long> {

    Collection<WhOutInvoice> findAll();

    boolean existsByWhCodeAndAwbAndStatus(String whCode, String awb, Integer status);

    WhOutInvoice findFirstByAwbAndStatus(String awb, Integer status);

    boolean existsByWhCodeAndAwbAndStatusAndIdIsNot(String whCode, String awb, Integer status, Long id);


    Collection<WhOutInvoice> findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(LocalDateTime dateTime1, LocalDateTime dateTime2, Integer status);

    boolean existsByAwbAndStatus(String awb, Integer status);
}
