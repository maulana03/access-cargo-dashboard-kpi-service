package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.Entity.WhCustomerInvoiceSetup;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WhCustomerInvoiceSetupRepo extends PagingAndSortingRepository<WhCustomerInvoiceSetup, Long> {

    WhCustomerInvoiceSetup findFirstByCustomerIdAndWhCodeAndWhTypeAndStatus(Long CustomerId, String whcode, String whType, Integer status);


}
