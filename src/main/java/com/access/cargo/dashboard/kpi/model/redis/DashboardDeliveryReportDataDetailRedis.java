package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@RedisHash("DashboardDeliveryReportDetailRedis")
public class DashboardDeliveryReportDataDetailRedis implements Serializable {

    private static final long serialVersionUID = -4357478242718924835L;
    @Id
    Long id;
//    String idTranscation;
    String agent;
    String awb;
    BigDecimal pieces;
    BigDecimal kg;
    String duration;
    BigDecimal amount;
    LocalDateTime date;


}

