package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.Entity.AwbLoading;
import com.access.cargo.dashboard.kpi.model.Entity.WhOutInvoice;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.Collection;

public interface AwbLoadingRepo extends JpaRepository<AwbLoading, Long> {


    Collection<AwbLoading> findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndState(LocalDateTime dateTime1, LocalDateTime dateTime2, Integer state);
    Collection<AwbLoading> findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStateAndAwb_AgentName(LocalDateTime dateTime1, LocalDateTime dateTime2, Integer state, String agentName);


}
