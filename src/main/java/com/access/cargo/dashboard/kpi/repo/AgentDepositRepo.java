package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.Entity.AgentDeposit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface AgentDepositRepo extends JpaRepository<AgentDeposit, Long> {

    Collection<AgentDeposit> findAllByStatus(Integer status);


}
