package com.access.cargo.dashboard.kpi.model.wrapper;//package com.access.cargo.account.model.wrapper;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommonResp {
    protected String msg;

    public CommonResp(String msg) {
        this.msg = msg;
    }
}
