package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.LinkedList;

@Data
@NoArgsConstructor
@RedisHash("DashboardKpiChartRedis")
public class DashboardKpiChartRedis implements Serializable {

    private static final long serialVersionUID = -145164073046930101L;
    @Id
    Long id;

    DashboardKpiParams params;
    String header; /*Agent, Airline*/
    String achievementType; /*BOOKING, RA, WHO, WHI,  WHT*/
    LinkedList<DashboardKpiChartDataRedis> data;

}
