package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.Entity.Awb;
import com.access.cargo.dashboard.kpi.model.Entity.AwbArrival;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface AwbArrivalRepo extends CrudRepository<AwbArrival, Long> {

    boolean existsByAwbAndState(Awb awb, Integer state);
    AwbArrival findFirstByAwbAndState(Awb awb, Integer state);
    Collection<AwbArrival> findAllByAwbInAndState(Awb[] awbList, Integer state);

}
