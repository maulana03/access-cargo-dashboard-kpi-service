package com.access.cargo.dashboard.kpi.service;

import com.access.cargo.dashboard.kpi.model.Entity.AgentDeposit;
import com.access.cargo.dashboard.kpi.model.Entity.AgentDepositCredit;
import com.access.cargo.dashboard.kpi.model.redis.DashboardTopUpReportDataDetailRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardTopUpReportSummaryRedis;
import com.access.cargo.dashboard.kpi.repo.AgentDepositCreditRepo;
import com.access.cargo.dashboard.kpi.repo.AgentDepositRepo;
import com.access.cargo.dashboard.kpi.repo.DashboardTopUpReportDataDetailRedisRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.LinkedList;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DashboardTopUpService {
    @Autowired
    DashboardTopUpReportDataDetailRedisRepo dashboardTopUpReportDataDetailRedisRepo;

    @Autowired
    AgentDepositRepo agentDepositRepo;

    @Autowired
    AgentDepositCreditRepo agentDepositCreditRepo;

    public DashboardTopUpReportSummaryRedis getSummaryTopUp(LocalDate dt1,LocalDate dt2,String agent){
//        Collection<DashboardTopUpReportDataDetailRedis> dashboardTopUpReportDataDetailRedis;

        Collection<AgentDeposit> agentDeposit;
        Collection<AgentDepositCredit> agentDepositCredits;

        LocalDateTime localDateTimeStart = dt1.atTime(00,00, 00);
        LocalDateTime localDateTimeEnd = dt2.atTime(23,59, 59);
        if(agent.equals("ALL")){
            agentDeposit = agentDepositRepo.findAllByStatus(1).stream().filter(x -> x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());
            agentDepositCredits = agentDepositCreditRepo.findAllByStatus(1).stream().filter(x -> x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());

        }else{
            agentDeposit = agentDepositRepo.findAllByStatus(1).stream().filter(x -> x.getAgent().getName().equals(agent) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());
            agentDepositCredits = agentDepositCreditRepo.findAllByStatus(1).stream().filter(x -> x.getAgent().getName().equals(agent) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());
        }
        BigDecimal sumSuccess = BigDecimal.ZERO;
        BigDecimal sumPending = BigDecimal.ZERO;
        BigDecimal sumFailed = BigDecimal.ZERO;

        for (AgentDeposit amt : agentDeposit) {
            if(amt.getStatusDesc().equals("APPROVED")){
                sumSuccess = sumSuccess.add(amt.getAmount() != null ? amt.getAmount():BigDecimal.valueOf(0));

            }else if(amt.getStatusDesc().equals("PENDING")){
                sumPending = sumPending.add(amt.getAmount() != null ? amt.getAmount():BigDecimal.valueOf(0));

            }else { //if(amt.getStatusDesc().equals("REJECTED"))
                sumFailed = sumFailed.add(amt.getAmount() != null ? amt.getAmount():BigDecimal.valueOf(0));

            }
        }

        for (AgentDepositCredit amt : agentDepositCredits) {
            if(amt.getStatusDesc().equals("APPROVED")){
                sumSuccess = sumSuccess.add(amt.getAmount() != null ? amt.getAmount():BigDecimal.valueOf(0));

            }else if(amt.getStatusDesc().equals("PENDING")){
                sumPending = sumPending.add(amt.getAmount() != null ? amt.getAmount():BigDecimal.valueOf(0));

            }else { //if(amt.getStatusDesc().equals("REJECTED"))
                sumFailed = sumFailed.add(amt.getAmount() != null ? amt.getAmount():BigDecimal.valueOf(0));

            }
        }

        DashboardTopUpReportSummaryRedis dashboardTopUpReportSummaryRedis =  new DashboardTopUpReportSummaryRedis();
        dashboardTopUpReportSummaryRedis.setSuccess(sumSuccess);
        dashboardTopUpReportSummaryRedis.setPending(sumPending);
        dashboardTopUpReportSummaryRedis.setFailed(sumFailed);
        return dashboardTopUpReportSummaryRedis;
    }

    public Collection<DashboardTopUpReportDataDetailRedis> getDetailTopUp(LocalDate dt1,LocalDate dt2,String agent){
        Collection<DashboardTopUpReportDataDetailRedis> dashboardTopUpReportDataDetailRedis = new LinkedList<>();
        Collection<AgentDeposit> agentDeposit;

        Collection<AgentDepositCredit> agentDepositCredits;

        LocalDateTime localDateTimeStart = dt1.atTime(00,00, 00);
        LocalDateTime localDateTimeEnd = dt2.atTime(23,59, 59);
        if(agent.equals("ALL")){
            agentDeposit = agentDepositRepo.findAllByStatus(1).stream().filter(x -> x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());
            agentDepositCredits = agentDepositCreditRepo.findAllByStatus(1).stream().filter(x -> x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());

        }else{
            agentDeposit = agentDepositRepo.findAllByStatus(1).stream().filter(x -> x.getAgent().getName().equals(agent) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());
            agentDepositCredits = agentDepositCreditRepo.findAllByStatus(1).stream().filter(x -> x.getAgent().getName().equals(agent) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());
        }

        dashboardTopUpReportDataDetailRedis=mappingData(agentDeposit,agentDepositCredits);
        return dashboardTopUpReportDataDetailRedis;
    }

    public Collection<DashboardTopUpReportDataDetailRedis> mappingData(Collection<AgentDeposit> agentDeposit, Collection<AgentDepositCredit> agentDepositCredits){
        Collection<DashboardTopUpReportDataDetailRedis> dashboardTopUpReportDataDetailRedisColl = new LinkedList<>();
        
        for(AgentDeposit agentDeposit1 : agentDeposit){
            DashboardTopUpReportDataDetailRedis dashboardTopUpReportDataDetailRedis = new DashboardTopUpReportDataDetailRedis();
            dashboardTopUpReportDataDetailRedis.setIdTranscation(agentDeposit1.getCode());
            dashboardTopUpReportDataDetailRedis.setAgent(agentDeposit1.getAgent().getName());
            dashboardTopUpReportDataDetailRedis.setTransaction("Deposit");
            dashboardTopUpReportDataDetailRedis.setTransactionTypeCode("C");
            dashboardTopUpReportDataDetailRedis.setAmount(agentDeposit1.getAmount());
            dashboardTopUpReportDataDetailRedis.setDate(agentDeposit1.getCreatedDateTime());
            dashboardTopUpReportDataDetailRedis.setStatus(agentDeposit1.getStatusDesc());
            dashboardTopUpReportDataDetailRedisColl.add(dashboardTopUpReportDataDetailRedis);
        }

        for(AgentDepositCredit agentDepositCredit : agentDepositCredits){
            DashboardTopUpReportDataDetailRedis dashboardTopUpReportDataDetailRedis = new DashboardTopUpReportDataDetailRedis();
            dashboardTopUpReportDataDetailRedis.setIdTranscation(agentDepositCredit.getCode());
            dashboardTopUpReportDataDetailRedis.setAgent(agentDepositCredit.getAgent().getName());
            dashboardTopUpReportDataDetailRedis.setTransaction("Deposit Credit");
            dashboardTopUpReportDataDetailRedis.setTransactionTypeCode("C");
            dashboardTopUpReportDataDetailRedis.setAmount(agentDepositCredit.getAmount());
            dashboardTopUpReportDataDetailRedis.setDate(agentDepositCredit.getCreatedDateTime());
            dashboardTopUpReportDataDetailRedis.setStatus(agentDepositCredit.getStatusDesc());
            dashboardTopUpReportDataDetailRedisColl.add(dashboardTopUpReportDataDetailRedis);
        }
        
        return dashboardTopUpReportDataDetailRedisColl;
    }
}