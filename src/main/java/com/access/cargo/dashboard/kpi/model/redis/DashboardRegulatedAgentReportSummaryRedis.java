package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class DashboardRegulatedAgentReportSummaryRedis implements Serializable {
    private static final long serialVersionUID = 4596850732330896623L;
    @Id
    Long id;

    BigDecimal awb;
    Integer pieces;
    BigDecimal kg;
    BigDecimal revenue;
}
