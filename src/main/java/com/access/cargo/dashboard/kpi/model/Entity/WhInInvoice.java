package com.access.cargo.dashboard.kpi.model.Entity;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.id.enhanced.SequenceStyleGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@DynamicUpdate
@Table(name = "wh_in_invoice", schema = "public")
public class WhInInvoice implements Serializable {
    private static final String ID = "ID";
    private static final String CODE = "CODE"; /*auto*/

    private static final String WH_CODE = "WH_CODE";
    private static final String AWB = "AWB";
    private static final String AIRLINE = "AIRLINE";
    private static final String FLIGHT_NO = "FLIGHT_NO";
    private static final String ORI = "ORI";
    private static final String DES = "DES";
    private static final String ROUTE = "ROUTE";
    private static final String PIECES = "PIECES";
    private static final String CHARGEABLE_WEIGHT = "CHARGEABLE_WEIGHT";

    private static final String AGENT = "AGENT";
    private static final String SHIPPER = "SHIPPER";
    private static final String CONSIGNEE = "CONSIGNEE";

    private static final String DAYS = "DAYS";
    private static final String RATES = "RATES"; /*WH-Service-Charges*/
    private static final String WH_SERVICE_TOTAL = "WH_SERVICE_TOTAL"; /*WH-Service-Charges*/

    private static final String CARGO_SERVICE_CHARGES = "CARGO_SERVICE_CHARGES";
    private static final String CARGO_SERVICE_TOTAL = "CARGO_SERVICE_TOTAL";

    private static final String CARGO_HANDLING_CHARGES = "CARGO_HANDLING_CHARGES";
    private static final String CARGO_HANDLING_TOTAL = "CARGO_HANDLING_TOTAL";

    private static final String KADE_CHARGES = "KADE_CHARGES";
    private static final String KADE_TOTAL = "KADE_TOTAL";

    private static final String PAYMENT_METHOD = "PAYMENT_METHOD";
    private static final String AMOUNT = "AMOUNT";
    private static final String AMOUNT_CHANGE = "AMOUNT_CHANGE";
    private static final String SUB_TOTAL = "SUB_TOTAL";
    private static final String PPN = "PPN";
    private static final String TOTAL = "TOTAL";
    /**/
    private static final String STATUS = "STATUS";
    private static final String STATUS_DESC = "STATUS_DESC";
    private static final String CREATED_DATETIME = "CREATED_DATETIME";
    private static final String CREATED_BY = "CREATED_BY";
    private static final String MODIFIED_DATETIME = "MODIFIED_DATETIME";
    private static final String MODIFIED_BY = "MODIFIED_BY";
    private static final long serialVersionUID = 5747322263392381009L;

    /**/

    @Id
    @GeneratedValue(generator = "wh_in_invoice_seq-generator")
    @GenericGenerator(
            name = "wh_in_invoice_seq-generator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.SEQUENCE_PARAM, value = "wh_in_invoice_seq"),
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.INITIAL_PARAM, value = "1"),
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.INCREMENT_PARAM, value = "1")
            }
    )
    @Column(name = ID)
    private Long id;

    @Column(name = CODE, length = 15)
    private String code;

    @Column(name = WH_CODE, length = 15)
    private String whCode;

    @Column(name = AWB, length = 15)
    private String awb;

    @Column(name = AIRLINE, length = 5)
    private String airline;

    @Column(name = FLIGHT_NO, length = 10)
    private String flightNo;

    @Column(name = ORI, length = 5)
    private String ori;

    @Column(name = DES, length = 5)
    private String des;

    @Column(name = ROUTE, length = 20)
    private String route;

    @Column(name = PIECES)
    private Integer pieces;

    @Column(name = CHARGEABLE_WEIGHT, precision = 19, scale = 3)
    private BigDecimal chargeableWeight;

    /**/

    @Column(name = AGENT, length = 30)
    private String agent;

    @Column(name = SHIPPER, length = 30)
    private String shipper;

    @Column(name = CONSIGNEE, length = 30)
    private String consignee;

    /**/


    @Column(name = RATES, precision = 19, scale = 3)
    private BigDecimal rates;

    @Column(name = WH_SERVICE_TOTAL, precision = 19, scale = 3)
    private BigDecimal whServiceTotal;

    @Column(name = DAYS)
    private Integer days;

    @Column(name = CARGO_SERVICE_CHARGES, precision = 19, scale = 3)
    private BigDecimal cargoServiceCharges;

    @Column(name = CARGO_SERVICE_TOTAL, precision = 19, scale = 3)
    private BigDecimal cargoServiceTotal;


    @Column(name = CARGO_HANDLING_CHARGES, precision = 19, scale = 3)
    private BigDecimal cargoHandlingCharges;

    @Column(name = CARGO_HANDLING_TOTAL, precision = 19, scale = 3)
    private BigDecimal cargoHandlingTotal;

    @Column(name = KADE_CHARGES, precision = 19, scale = 3)
    private BigDecimal kadeCharges;

    @Column(name = KADE_TOTAL, precision = 19, scale = 3)
    private BigDecimal kadeTotal;

    /**/

    @Column(name = PAYMENT_METHOD, length = 30)
    private String paymentMethod;

    @Column(name = AMOUNT, precision = 19, scale = 3)
    private BigDecimal amount;

    @Column(name = AMOUNT_CHANGE, precision = 19, scale = 3)
    private BigDecimal amountChange;

    @Column(name = PPN, precision = 19, scale = 3)
    private BigDecimal ppn;

    @Column(name = SUB_TOTAL, precision = 19, scale = 3)
    private BigDecimal subTotal;

    @Column(name = TOTAL, precision = 19, scale = 3)
    private BigDecimal total;


    /**/

    @Column(name = STATUS)
    private Integer status;

    @Column(name = STATUS_DESC, length = 30)
    private String statusDesc;

    @Column(name = CREATED_DATETIME)
    private LocalDateTime createdDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = CREATED_BY)
    private AccountUser createdBy;

    @Column(name = MODIFIED_DATETIME)
    private LocalDateTime modifiedDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = MODIFIED_BY)
    private AccountUser modifiedBy;

}
