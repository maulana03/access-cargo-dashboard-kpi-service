package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.redis.DashboardKpiChartRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardKpiTableRedis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DashboardKpiChartRedisRepo extends JpaRepository<DashboardKpiChartRedis, String> {
}
