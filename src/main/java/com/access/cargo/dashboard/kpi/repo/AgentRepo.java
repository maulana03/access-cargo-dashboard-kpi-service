package com.access.cargo.dashboard.kpi.repo;


import com.access.cargo.dashboard.kpi.model.Entity.Agent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AgentRepo extends JpaRepository<Agent, Long> {
    Agent findFirstByIdAndStatus(Long id, Integer status);
    Agent findFirstByNameAndStatus(String name, Integer status);
}
