package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.redis.DashboardRevenueReportDataDetailRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardSalesReportDataDetailRedis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DashboardRevenueReportDataDetailRedisRepo extends JpaRepository<DashboardRevenueReportDataDetailRedis, String> {
}
