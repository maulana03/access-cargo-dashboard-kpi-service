package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.redis.DashboardAwbInformationRedis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DashboardAwbInformationRedisRepo extends JpaRepository<DashboardAwbInformationRedis, String> {
}
