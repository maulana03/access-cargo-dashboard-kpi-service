package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.redis.DashboardKpiTableRedis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DashboardKpiTableRedisRepo extends JpaRepository<DashboardKpiTableRedis, String> {
}
