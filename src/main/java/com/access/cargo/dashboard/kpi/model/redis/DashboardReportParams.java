package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@NoArgsConstructor
public class DashboardReportParams implements Serializable {

    String branch_;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate startDate_;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate endDate_;
    String agent_;
    Long agentId_;

}
