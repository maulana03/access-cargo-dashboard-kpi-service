package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@RedisHash("DashboardRegulatedAgentReportDetailRedis")
public class DashboardRegulatedAgentReportDataDetailRedis implements Serializable {
    private static final long serialVersionUID = -4305228027092092386L;
    @Id
    Long id;
//    String idTranscation;
    String agent;
    String awb;
    BigDecimal bookingPieces;
    BigDecimal bookingKg;
    BigDecimal amount;

    Integer raPieces;
    BigDecimal raKg;
    String xray;
    LocalDate date;
    String branch;

}

