package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@RedisHash("DashboardRevenueReportDetailRedis")
public class DashboardRevenueReportDataDetailRedis implements Serializable {
    private static final long serialVersionUID = -4305228027092092386L;
    @Id
    Long id;
    String idTranscation;
    String agent;
    String awb;
    BigDecimal pieces;
    BigDecimal kg;
    String transaction;
    String transactionTypeCode;
    BigDecimal amount;
    LocalDate date;
    String status;
    String branch;

}

