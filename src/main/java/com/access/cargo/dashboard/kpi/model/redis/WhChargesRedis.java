package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.LinkedList;

@Data
//@NoArgsConstructor
@RedisHash("WhChargesRedis")
public class WhChargesRedis implements Serializable {


    private static final long serialVersionUID = -7833180535512126140L;
    @Id
    Long id;
    String whCode;
    String whName;
    String whType;
    LocalDate validFrom;
    LocalDate validThrough;
    BigDecimal rates;
    BigDecimal ratesBase;
    Integer ratesBaseDays;

    Collection<AdditionalCharges> additionalCharges = new LinkedList<>();
    Integer status;

    /**/
    String statusDesc;
    Long createdById;
    String createdByName;
    LocalDateTime createdDateTime;
    Long modifiedById;
    String modifiedByName;
    LocalDateTime modifiedDateTime;

    /**/
    public WhChargesRedis() {
        AdditionalCharges ac1 = new AdditionalCharges();
        ac1.setDayFrom(4);
        ac1.setCharges(BigDecimal.valueOf(1050));

        AdditionalCharges ac2 = new AdditionalCharges();
        ac2.setDayFrom(11);
        ac2.setCharges(BigDecimal.valueOf(350));

        additionalCharges.add(ac1);
        additionalCharges.add(ac2);
    }

    @Data
    public static class AdditionalCharges {
        Integer dayFrom;
        BigDecimal charges;
    }

}
