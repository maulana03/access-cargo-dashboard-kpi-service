package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.Entity.AwbBuildUp;
import com.access.cargo.dashboard.kpi.model.Entity.AwbLoading;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.Collection;

public interface AwbBuildUpRepo extends JpaRepository<AwbBuildUp, Long> {
 /*   @Query("SELECT c.flight_sch, COUNT(c.flight_sch) FROM awb_build_up AS c GROUP BY c.flight_sch ORDER BY c.flight_sch DESC")
    List<Object[]> countTotalCommentsByYear();
*/
    Collection<AwbBuildUp> findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndState(LocalDateTime dateTime1, LocalDateTime dateTime2, Integer state);
    Collection<AwbBuildUp> findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStateAndAwb_AgentName(LocalDateTime dateTime1, LocalDateTime dateTime2, Integer state, String agentName);

}
