package com.access.cargo.dashboard.kpi.model.Entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.id.enhanced.SequenceStyleGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
//@DynamicUpdate
@Table(name = "ra_rates", schema = "public")
public class RaRates implements Serializable {
    private static final String ID = "ID";
    private static final String AGENT = "AGENT";
    private static final String RATES = "RATES";
    private static final String START_DATE = "START_DATE";
    private static final String STOP_DATE = "STOP_DATE";
    /**/
    private static final String STATUS = "STATUS";
    private static final String STATUS_DESC = "STATUS_DESC";
    private static final String CREATED_DATETIME = "CREATED_DATETIME";
    private static final String CREATED_BY = "CREATED_BY";
    private static final String MODIFIED_DATETIME = "MODIFIED_DATETIME";
    private static final String MODIFIED_BY = "MODIFIED_BY";
    private static final long serialVersionUID = -1527579744496962778L;

    /**/

    @Id
    @GeneratedValue(generator = "ra_rates_seq-generator")
    @GenericGenerator(
            name = "ra_rates_seq-generator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.SEQUENCE_PARAM, value = "ra_rates_seq"),
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.INITIAL_PARAM, value = "1"),
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.INCREMENT_PARAM, value = "1")
            }
    )
    @Column(name = ID)
    private Long id;

    @Column(name = AGENT, length = 50)
    private String agent; // dari agent raAcceptance

    @Column(name = RATES, precision = 19, scale = 3)
    private BigDecimal rates; //Gross Weight per kilo

    @Column(name = START_DATE)
    private LocalDate startDate;

    @Column(name = STOP_DATE)
    private LocalDate stopDate;


    /**/

    @Column(name = STATUS)
    private Integer status;

    @Column(name = STATUS_DESC, length = 30)
    private String statusDesc;

    @Column(name = CREATED_DATETIME)
    private LocalDateTime createdDateTime;

    @Column(name = MODIFIED_DATETIME)
    private LocalDateTime modifiedDateTime;

}
