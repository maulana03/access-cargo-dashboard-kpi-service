package com.access.cargo.dashboard.kpi.service;


import com.access.cargo.dashboard.kpi.model.Entity.AwbBuildUp;
import com.access.cargo.dashboard.kpi.model.Entity.AwbKoliBuildUp;
import com.access.cargo.dashboard.kpi.model.Entity.FlightList;
import com.access.cargo.dashboard.kpi.model.redis.DashboardPreManifestReportDataDetailRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardPreManifestReportSummaryRedis;
import com.access.cargo.dashboard.kpi.repo.AwbBuildUpRepo;
import com.access.cargo.dashboard.kpi.repo.AwbKoliBuildUpRepo;
import com.access.cargo.dashboard.kpi.repo.DashboardPreManifestReportDataDetailRedisRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DashboardPreManifestService {
    @Autowired
    DashboardPreManifestReportDataDetailRedisRepo dashboardPreManifestReportDataDetailRedisRepo;

    @Autowired
    AwbBuildUpRepo awbBuildUpRepo;

    @Autowired
    AwbKoliBuildUpRepo awbKoliBuildUpRepo;

    public DashboardPreManifestReportSummaryRedis getSummary(LocalDate dt1,LocalDate dt2,String branch){
        Collection<AwbBuildUp> awbBuildUps = new LinkedList<>();
        LocalDateTime localDateTimeStart = dt1.atTime(00,00, 00);
        LocalDateTime localDateTimeEnd = dt2.atTime(23,59, 59);

        if(branch.equals("ALL")){
            awbBuildUps = awbBuildUpRepo.findAll().stream().filter(x -> x.getState().equals(1) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());

        }else{
            //menunggu arahan branch ambil apa
//            awbBuildUps = awbBuildUpRepo.findAll().stream().filter(x -> x.getBranch().equals(branch) && x.getManifestTime().isAfter(dt1) && x.getManifestTime().isBefore(dt2)).collect(Collectors.toList());

        }

        Map<FlightList, Long> countedFlight = awbBuildUps.stream()
                .collect(Collectors.groupingBy(AwbBuildUp::getFlightSch, Collectors.counting()));

        Integer sumTotalPieces = 0;
        BigDecimal sumTotalKg= BigDecimal.ZERO;

        for (AwbBuildUp amt : awbBuildUps) {

            sumTotalPieces = Integer.sum(sumTotalPieces,amt.getPieces()!= null ? amt.getPieces():0);
            sumTotalKg = sumTotalKg.add(amt.getWeight()!= null ? amt.getWeight():BigDecimal.ZERO);

        }

        DashboardPreManifestReportSummaryRedis dashboardPreManifestReportSummaryRedis =  new DashboardPreManifestReportSummaryRedis();
        dashboardPreManifestReportSummaryRedis.setTotalFlight(BigDecimal.valueOf(awbBuildUps.size()));
        dashboardPreManifestReportSummaryRedis.setTotalAwb(countedFlight.size());
        dashboardPreManifestReportSummaryRedis.setTotalPieces(sumTotalPieces);
        dashboardPreManifestReportSummaryRedis.setTotalKg(sumTotalKg);
        return dashboardPreManifestReportSummaryRedis;
    }

    public Collection<DashboardPreManifestReportDataDetailRedis> getDetail(LocalDate dt1,LocalDate dt2,String branch){
        Collection<DashboardPreManifestReportDataDetailRedis> dashboardPreManifestReportDataDetailRedis = new LinkedList<>();

        List<AwbBuildUp> awbBuildUps = new LinkedList<>();
        LocalDateTime localDateTimeStart = dt1.atTime(00,00, 00);
        LocalDateTime localDateTimeEnd = dt2.atTime(23,59, 59);


        if(branch.equals("ALL")){
            awbBuildUps = awbBuildUpRepo.findAll().stream().filter(x -> x.getState().equals(1) && x.getCreatedDateTime().isAfter(localDateTimeStart) && x.getCreatedDateTime().isBefore(localDateTimeEnd)).collect(Collectors.toList());

        }else{
            //menunggu arahan branch ambil apa
//            awbBuildUps = awbBuildUpRepo.findAll().stream().filter(x -> x.getBranch().equals(branch) && x.getManifestTime().isAfter(dt1) && x.getManifestTime().isBefore(dt2)).collect(Collectors.toList());

        }

        awbBuildUps.sort(Comparator.comparing(o -> o.getCreatedDateTime()));

        dashboardPreManifestReportDataDetailRedis =  mappingData(awbBuildUps);
        return dashboardPreManifestReportDataDetailRedis;
    }
    public Collection<DashboardPreManifestReportDataDetailRedis> mappingData(List<AwbBuildUp> awbBuildUps){
        Collection<DashboardPreManifestReportDataDetailRedis> dashboardPreManifestReportDataDetailRedis = new LinkedList<>();

        Map<FlightList, Long> countedAwb = awbBuildUps.stream()
                .collect(Collectors.groupingBy(AwbBuildUp::getFlightSch, Collectors.counting()));

        for (Map.Entry<FlightList, Long> entry : countedAwb.entrySet()) {
            FlightList flightList = entry.getKey();

            Integer sumTotalPieces = 0;
            BigDecimal sumTotalKg= BigDecimal.ZERO;

            DashboardPreManifestReportDataDetailRedis dashboardPreManifestReportDataDetailRedis1 = new DashboardPreManifestReportDataDetailRedis();

            int sumawb=0;
            for(AwbBuildUp awbBuildUp : awbBuildUps){
                if(flightList.getId().equals(awbBuildUp.getFlightSch().getId())){
                    sumTotalPieces = Integer.sum(sumTotalPieces,awbBuildUp.getPieces() != null ? awbBuildUp.getPieces():0);
                    sumTotalKg = sumTotalKg.add(awbBuildUp.getWeight() != null ? awbBuildUp.getWeight():BigDecimal.ZERO);
                sumawb++;
                }
            }

            //get lass transaction
            List<AwbBuildUp> awbBuildUp = awbBuildUps.stream().filter(x -> x.getState().equals(1) && x.getFlightSch().getId().equals(flightList.getId())).collect(Collectors.toList());
            int size = awbBuildUp.size();
            AwbBuildUp lastTransaction = awbBuildUp.get(size-1);

            //get grobak
            AwbKoliBuildUp awbKoliBuildUp =  awbKoliBuildUpRepo.findFirstByFlightSch_DeparatureDateAndFlightSch_DeparatureTime(lastTransaction.getFlightSch().getDeparatureDate(),lastTransaction.getFlightSch().getDeparatureTime());

            dashboardPreManifestReportDataDetailRedis1.setFlight(lastTransaction.getFlightSch().getAirlineCode() != null ? lastTransaction.getFlightSch().getAirlineCode() :"");
            dashboardPreManifestReportDataDetailRedis1.setRoute(lastTransaction.getFlightSch().getSector() != null ? lastTransaction.getFlightSch().getSector() :"");
            if(awbKoliBuildUp != null){
                dashboardPreManifestReportDataDetailRedis1.setType(awbKoliBuildUp.getGerobak() != null ? "Gerobak" :"ULD");
            }else{
                dashboardPreManifestReportDataDetailRedis1.setType("");
            }
            dashboardPreManifestReportDataDetailRedis1.setTotalAwb(sumawb);
            dashboardPreManifestReportDataDetailRedis1.setNoPreman("xxxxx");
            dashboardPreManifestReportDataDetailRedis1.setTotalPieces(sumTotalPieces);
            dashboardPreManifestReportDataDetailRedis1.setTotalKg(sumTotalKg);
            dashboardPreManifestReportDataDetailRedis1.setReleasedTime(lastTransaction.getCreatedDateTime());



            dashboardPreManifestReportDataDetailRedis.add(dashboardPreManifestReportDataDetailRedis1);
        }

        return dashboardPreManifestReportDataDetailRedis;
    }


}