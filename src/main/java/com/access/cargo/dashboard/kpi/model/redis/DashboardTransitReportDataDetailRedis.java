package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@RedisHash("DashboardTransitReportDetailRedis")
public class DashboardTransitReportDataDetailRedis implements Serializable {

    private static final long serialVersionUID = -5070147123176166073L;
    @Id
    Long id;
    String flight;
    String route;
    String agent;
    BigDecimal transitAwb;
    BigDecimal transitPieces;
    BigDecimal transitKg;
    LocalDate date;
    LocalDate offloadingTime;
    String branch;

}

