package com.access.cargo.dashboard.kpi.model.Entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@NamedEntityGraph(
        name = "AwbKoliBuildUp-awbKoli-awb",
        attributeNodes = {@NamedAttributeNode(value = "awbKoli", subgraph = "awbKoli-awb")},
        subgraphs = {
                @NamedSubgraph(name = "awbKoli-awb", attributeNodes = {@NamedAttributeNode("awb")})
        }
)
@Data
@Entity
@Table(name = "awb_koli_build_up")
public class AwbKoliBuildUp implements Serializable {

    private static final long serialVersionUID = 8042503233456596214L;

    @Id
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "awb_koli")
    private AwbKoli awbKoli;

    @Column(name = "flight_no", length = 15)
    private String flightNo;

    @Column(name = "gerobak_code", length = 15)
    private String gerobakCode;

    @Column(name = "uld_code", length = 15)
    private String uldCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gerobak")
    private Gerobak gerobak;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uld")
    private Uld uld;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "flight_sch")
    private FlightList flightSch;

    /**/

    @Column(name = "state")
    private Integer state;

    @Column(name = "created_datetime")
    private LocalDateTime createdDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by")
    private AccountUser createdBy;

    @Column(name = "modified_datetime")
    private LocalDateTime modifiedDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "modified_by")
    private AccountUser modifiedBy;
}
