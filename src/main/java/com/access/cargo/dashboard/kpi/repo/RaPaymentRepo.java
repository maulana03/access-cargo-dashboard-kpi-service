package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.Entity.Awb;
import com.access.cargo.dashboard.kpi.model.Entity.RaAcceptance;
import com.access.cargo.dashboard.kpi.model.Entity.RaPayment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.Collection;

public interface RaPaymentRepo extends JpaRepository<RaPayment, Long> {
RaPayment findFirstByAwb(String Awb);

}
