package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@RedisHash("DashboardSalesReportDetailRedis")
public class DashboardSalesReportDataDetailRedis implements Serializable {
    private static final long serialVersionUID = 553698758109396383L;
    @Id
    Long id;
    String idTranscation;
    String agent;
    String awb;
    Integer pieces;
    BigDecimal kg;
    String transaction;
    String transactionTypeCode;
    BigDecimal amount;
    LocalDateTime date;
    String status;
    String branch;

}

