package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.Entity.AwbLoading;
import com.access.cargo.dashboard.kpi.model.Entity.WhInInvoice;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.Collection;

public interface WhInvoiceRepo extends JpaRepository<WhInInvoice, Long> {
    Collection<WhInInvoice> findAllByCreatedDateTimeAfterAndCreatedDateTimeBeforeAndStatus(LocalDateTime dateTime1, LocalDateTime dateTime2, Integer status);


}
