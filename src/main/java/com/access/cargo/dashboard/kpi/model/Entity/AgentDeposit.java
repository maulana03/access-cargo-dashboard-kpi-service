package com.access.cargo.dashboard.kpi.model.Entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "agent_deposit", schema = "public")
public class AgentDeposit implements Serializable {
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "amount")
    private BigDecimal amount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agent")
    private Agent agent;

    @Column(name = "status")
    private Integer status;

    @Column(name = "status_desc")
    private String statusDesc;;

    @Column(name = "modified_datetime")
    private LocalDateTime modifiedDateTime;

    @Column(name = "created_datetime")
    private LocalDateTime createdDateTime;


}
