package com.access.cargo.dashboard.kpi.model.Entity;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@NamedEntityGraph(
        name = "AwbKoliOffLoading-awbKoli-awb",
        attributeNodes = {@NamedAttributeNode(value = "awbKoli", subgraph = "awbKoli-awb")},
        subgraphs = {
                @NamedSubgraph(name = "awbKoli-awb", attributeNodes = {@NamedAttributeNode("awb")})
        }
)
@Data
@Entity
@Table(name = "awb_koli_offloading")
@DynamicUpdate
public class AwbKoliOffLoading implements Serializable {


    private static final long serialVersionUID = 8294277791076863665L;
    @Id
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "awb_koli")
    private AwbKoli awbKoli;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gerobak")
    private Gerobak gerobak;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uld")
    private Uld uld;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "flight_sch")
    private FlightList flightSch;

    /**/

    @Column(name = "state")
    private Integer state;

    @Column(name = "created_datetime")
    private LocalDateTime createdDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by")
    private AccountUser createdBy;

    @Column(name = "modified_datetime")
    private LocalDateTime modifiedDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "modified_by")
    private AccountUser modifiedBy;
}
