package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.redis.DashboardRegulatedAgentReportDataDetailRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardWHAcceptanceReportDataDetailRedis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DashboardWHAcceptanceReportDataDetailRedisRepo extends JpaRepository<DashboardWHAcceptanceReportDataDetailRedis, String> {
}
