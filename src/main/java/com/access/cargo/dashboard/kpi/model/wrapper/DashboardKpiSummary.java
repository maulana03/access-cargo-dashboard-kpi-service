package com.access.cargo.dashboard.kpi.model.wrapper;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class DashboardKpiSummary implements Serializable {

    private static final long serialVersionUID = -2223502731270725360L;

    BigDecimal bkdWeightAmount;
    BigDecimal bkdRevAmount;

    BigDecimal raWeightAmount;
    BigDecimal raRevAmount;

    BigDecimal whoWeightAmount;
    BigDecimal whoRevAmount;

//    BigDecimal whtWeightAmount;
//    BigDecimal whtRevAmount;

    BigDecimal whiWeightAmount;
    BigDecimal whiRevAmount;

    BigDecimal whmWeightAmount;
    BigDecimal whmRevAmount;

}
