package com.access.cargo.dashboard.kpi.service;

import com.access.cargo.dashboard.kpi.feign.AwbClient;
import com.access.cargo.dashboard.kpi.model.wrapper.AwbRedis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AwbService {

    @Autowired
    AwbClient awbClient;

    public AwbRedis getAwbV2ByAwbCode(String token, String awbCode) {
        return awbClient.getAwbV2ByAwbCode(token, awbCode).getBody();
    }
}
