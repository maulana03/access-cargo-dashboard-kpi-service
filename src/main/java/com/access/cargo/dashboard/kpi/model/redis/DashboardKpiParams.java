package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@NoArgsConstructor
public class DashboardKpiParams implements Serializable {
    String period_;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate startDate_;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate endDate_;
    Integer month_;
    Integer year_;
    String branch_;

}
