package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.redis.DashboardManifestReportDataDetailRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardOffLoadingReportDataDetailRedis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DashboardOffLoadingReportDataDetailRedisRepo extends JpaRepository<DashboardOffLoadingReportDataDetailRedis, String> {
}
