package com.access.cargo.dashboard.kpi.model.Entity;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.id.enhanced.SequenceStyleGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@DynamicUpdate
@Table(name = "agent_deposit_credit", schema = "public")
public class AgentDepositCredit implements Serializable {
    private static final String ID = "ID";
    private static final String CODE = "CODE";
    private static final String TRANSACTION_DATETIME = "TRANSACTION_DATETIME";
    private static final String AMOUNT = "AMOUNT";
    private static final String CURRENCY = "CURRENCY";
    private static final String REFERENCE = "REFERENCE";
    private static final String BANK = "BANK";
    private static final String BANK_NAME = "BANK_NAME";
    private static final String ATTACHMENT = "ATTACHMENT";
    private static final String REMARKS = "REMARKS";
    private static final String AGENT = "AGENT";
    /**/
    /**/
    private static final String STATUS = "STATUS";
    private static final String STATUS_DESC = "STATUS_DESC";
    private static final String CREATED_DATETIME = "CREATED_DATETIME";
    private static final String CREATED_BY = "CREATED_BY";
    private static final String MODIFIED_DATETIME = "MODIFIED_DATETIME";
    private static final String MODIFIED_BY = "MODIFIED_BY";
    private static final long serialVersionUID = 5560046630057621335L;

    /**/

    @Id
    @GeneratedValue(generator = "agent_deposit_credit_seq-generator")
    @GenericGenerator(
            name = "agent_deposit_credit_seq-generator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.SEQUENCE_PARAM, value = "agent_deposit_credit_seq"),
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.INITIAL_PARAM, value = "1"),
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.INCREMENT_PARAM, value = "1")
            }
    )
    @Column(name = ID)
    private Long id;

    @Column(name = CODE, length = 15)
    private String code;

    @Column(name = TRANSACTION_DATETIME)
    private LocalDateTime transactionDateTime;

    @Column(name = AMOUNT, precision = 19, scale = 3)
    private BigDecimal amount;

    @Column(name = CURRENCY, length = 5)
    private String currency;

    @Column(name = REFERENCE, length = 50)
    private String reference;

    @Column(name = BANK_NAME, length = 25)
    private String bankName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = AGENT)
    private Agent agent;

    @Column(name = ATTACHMENT, columnDefinition = "TEXT")
    private String attachment;

    @Column(name = REMARKS, length = 50)
    private String remarks;

    /**/

    @Column(name = STATUS)
    private Integer status;

    @Column(name = STATUS_DESC, length = 30)
    private String statusDesc;

    @Column(name = CREATED_DATETIME)
    private LocalDateTime createdDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = CREATED_BY)
    private AccountUser createdBy;

    @Column(name = MODIFIED_DATETIME)
    private LocalDateTime modifiedDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = MODIFIED_BY)
    private AccountUser modifiedBy;

}
