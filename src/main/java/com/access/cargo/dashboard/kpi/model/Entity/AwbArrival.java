package com.access.cargo.dashboard.kpi.model.Entity;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.id.enhanced.SequenceStyleGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "awb_arrival")
@DynamicUpdate
public class AwbArrival implements Serializable {

    private static final long serialVersionUID = -9068840499298628613L;

    @Id @GeneratedValue(generator = "awb_arrival_seq-generator")
    @GenericGenerator(
            name = "awb_arrival_seq-generator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.SEQUENCE_PARAM, value = "awb_arrival_seq"),
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.INITIAL_PARAM, value = "1"),
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.INCREMENT_PARAM, value = "1")
            }
    )
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "awb")
    private Awb awb;

    @Column(name = "pieces")
    private Integer pieces;

    @Column(name = "pieces_total")
    private Integer piecesTotal;

    /**/

    @Column(name = "state")
    private Integer state;

    @Column(name = "created_datetime")
    private LocalDateTime createdDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by")
    private AccountUser createdBy;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "modified_by")
    private AccountUser modifiedBy;

    @Column(name = "modified_datetime")
    private LocalDateTime modifiedDatetime;

}
