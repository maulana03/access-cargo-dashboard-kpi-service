package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@RedisHash("DashboardRevenueReportSummaryRedis")
public class DashboardRevenueReportSummaryRedis implements Serializable {

    private static final long serialVersionUID = -9175546792795332739L;
    @Id
    Long id;

//    DashboardReportParams params;
    BigDecimal awbManifest;
    BigDecimal piecesManifest;
    BigDecimal kgManifest;
    BigDecimal revenue;
}
