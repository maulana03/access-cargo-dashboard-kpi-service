package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@RedisHash("DashboardKpiSummaryRedis")
public class DashboardKpiSummaryRedis implements Serializable {

    private static final long serialVersionUID = -2223502731270725360L;
    @Id
    Long id;

    DashboardKpiParams params;
    BigDecimal bkdWeightAmount;
    BigDecimal bkdRevAmount;

    BigDecimal raWeightAmount;
    BigDecimal raRevAmount;

    BigDecimal whoWeightAmount;
    BigDecimal whoRevAmount;

    BigDecimal whtWeightAmount;
    BigDecimal whtRevAmount;

    BigDecimal whiWeightAmount;
    BigDecimal whiRevAmount;

}
