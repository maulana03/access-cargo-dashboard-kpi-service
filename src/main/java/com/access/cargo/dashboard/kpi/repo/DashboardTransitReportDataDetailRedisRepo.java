package com.access.cargo.dashboard.kpi.repo;

import com.access.cargo.dashboard.kpi.model.redis.DashboardOffLoadingReportDataDetailRedis;
import com.access.cargo.dashboard.kpi.model.redis.DashboardTransitReportDataDetailRedis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DashboardTransitReportDataDetailRedisRepo extends JpaRepository<DashboardTransitReportDataDetailRedis, String> {
}
