package com.access.cargo.dashboard.kpi.model.Entity;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.id.enhanced.SequenceStyleGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@DynamicUpdate
@Table(name = "ra_payment", schema = "public")
public class RaPayment implements Serializable {
    private static final String ID = "ID";
    private static final String CODE = "CODE";
    private static final String AWB = "AWB";
    private static final String AGENT = "AGENT";
    private static final String PAYMENT_METHOD = "PAYMENT_METHOD";
    private static final String PAYMENT_AMOUNT = "PAYMENT_AMOUNT";
    private static final String PAYMENT_CHANGE = "PAYMENT_CHANGE";
    private static final String TOTAL_INVOICE = "TOTAL_INVOICE";
    /**/
    private static final String STATUS = "STATUS";
    private static final String STATUS_DESC = "STATUS_DESC";
    private static final String CREATED_DATETIME = "CREATED_DATETIME";
    private static final String CREATED_BY = "CREATED_BY";
    private static final String MODIFIED_DATETIME = "MODIFIED_DATETIME";
    private static final String MODIFIED_BY = "MODIFIED_BY";
    private static final long serialVersionUID = 1671624318551222435L;

    /**/

    @Id
    @GeneratedValue(generator = "ra_payment_seq-generator")
    @GenericGenerator(
            name = "ra_payment_seq-generator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.SEQUENCE_PARAM, value = "ra_payment_seq"),
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.INITIAL_PARAM, value = "1"),
                    @org.hibernate.annotations.Parameter(name = SequenceStyleGenerator.INCREMENT_PARAM, value = "1")
            }
    )
    @Column(name = ID)
    private Long id;

    @Column(name = CODE, length = 50)
    private String code;

    @Column(name = AWB, length = 15)
    private String awb;

    @Column(name = AGENT, length = 50)
    private String agent;

    @Column(name = PAYMENT_METHOD, length = 30)
    private String paymentMethod;

    @Column(name = PAYMENT_AMOUNT, precision = 19, scale = 3)
    private BigDecimal paymentAmount;

    @Column(name = PAYMENT_CHANGE, precision = 19, scale = 3)
    private BigDecimal paymentChange;

    @Column(name = TOTAL_INVOICE, precision = 19, scale = 3)
    private BigDecimal totalInvoice;


    /**/

    @Column(name = STATUS)
    private Integer status;

    @Column(name = STATUS_DESC, length = 30)
    private String statusDesc;

    @Column(name = CREATED_DATETIME)
    private LocalDateTime createdDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = CREATED_BY)
    private AccountUser createdBy;

    @Column(name = MODIFIED_DATETIME)
    private LocalDateTime modifiedDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = MODIFIED_BY)
    private AccountUser modifiedBy;

}
