package com.access.cargo.dashboard.kpi.controller;


import com.access.cargo.dashboard.kpi.config.JwtConstants;
import com.access.cargo.dashboard.kpi.model.redis.DashboardReportParams;
import com.access.cargo.dashboard.kpi.service.AccountService;
import com.access.cargo.dashboard.kpi.service.DashboardCcaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.LinkedList;

@CrossOrigin
@Slf4j
@RestController
@Validated
public class DashboardCcaReportController {
    static final String CCA_REPORT = "cca/dashboardKpi";
    static final String CCA_REPORT_SUMMARY = CCA_REPORT + "/summary";
    static final String CCA_REPORT_DETAIL = CCA_REPORT + "/detail";

    @Autowired
    DashboardCcaService dashboardCcaService;

    @Autowired
    AccountService accountService;

    @GetMapping(CCA_REPORT_SUMMARY)
    public ResponseEntity getSummaryCcaReport(
            @RequestHeader(value = JwtConstants.TOKEN_HEADER) String token
            , @ModelAttribute DashboardReportParams params) {

        Collection<String> authRoles = new LinkedList<>();
        authRoles.add("ROLE_SUPER_ADMIN");
        authRoles.add("ROLE_REPORT");
        authRoles.add("ROLE_ADMIN");
        authRoles.add("ROLE_REPORT_TONASE");
        authRoles.add("ROLE_ASYST_OPS");

        if (!accountService.isAuthorized(token, authRoles)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(dashboardCcaService.getSummary(token,params.getStartDate_(),params.getEndDate_(),params.getAgent_(),params.getAgentId_(),params.getBranch_()), HttpStatus.OK);

    }

    @GetMapping(CCA_REPORT_DETAIL)
    public ResponseEntity getDetailCcaReport(
            @RequestHeader(value = JwtConstants.TOKEN_HEADER) String token
            , @ModelAttribute DashboardReportParams params) {

        Collection<String> authRoles = new LinkedList<>();
        authRoles.add("ROLE_SUPER_ADMIN");
        authRoles.add("ROLE_REPORT");
        authRoles.add("ROLE_ADMIN");
        authRoles.add("ROLE_REPORT_TONASE");
        authRoles.add("ROLE_ASYST_OPS");

        if (!accountService.isAuthorized(token, authRoles)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(dashboardCcaService.getDetail(token,params.getStartDate_(),params.getEndDate_(),params.getAgent_(),params.getAgentId_(),params.getBranch_()), HttpStatus.OK);

    }
}
