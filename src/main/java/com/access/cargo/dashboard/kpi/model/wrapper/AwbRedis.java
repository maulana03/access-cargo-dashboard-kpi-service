package com.access.cargo.dashboard.kpi.model.wrapper;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
//@RedisHash("AwbRedis")
public class AwbRedis implements Serializable {

    private static final long serialVersionUID = -5916479251970001913L;
    @Id
    Long id;

    String code;
    BigDecimal grossWeight;
    BigDecimal volumeCbm;
    BigDecimal volumeWeight;
    BigDecimal chargeableWeight;
    BigDecimal length;
    BigDecimal width;
    BigDecimal height;

    Integer pieces;
    Integer piecesPrev;
    Integer piecesRcs;

    String origin;
    String destination;
    String sector;
    Long agentId;
    String agentName;
    String shipperName;
    String consigneeName;
    String airline;
    String flightNo;
    LocalDate flightDate;
    LocalDate issuedDate;

    String state;
    String state1;

    Integer stateMsgTypeB = 0;
    Integer stateIncoming = 0;
    Integer stateOutgoing = 0;
    Integer stateTransit = 0;

    BigDecimal rates;
    BigDecimal ratesTotal;

    String commodityCode;
    String productCode;
    String shc;
    String nog;
    String nogConsol;
    String sph;
    Integer stateSph;
    BigDecimal taxes;
    BigDecimal chargeSummaryTotal;


    Long shipperId;
    String shipperCode;

    Long consigneeId;
    String consigneeCode;

    Long flightListId;
    String airlineCode;
    String flightNumber;

    Long createdById;
    String createdByName;
    LocalDateTime createdDateTime;

    Long modifiedById;
    String modifiedByName;
    LocalDateTime modifiedDateTime;

}
