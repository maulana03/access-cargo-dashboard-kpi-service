package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class DashboardManifestReportSummaryRedis implements Serializable {
    private static final long serialVersionUID = -269298680475558118L;
    @Id
    Long id;

    Integer totalFlight;
    BigDecimal totalAwb;
    Integer totalPieces;
    BigDecimal totalKg;
}
