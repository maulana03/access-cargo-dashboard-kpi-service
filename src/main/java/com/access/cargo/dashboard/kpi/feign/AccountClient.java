package com.access.cargo.dashboard.kpi.feign;

import com.access.cargo.dashboard.kpi.config.JwtConstants;
import com.access.cargo.dashboard.kpi.model.wrapper.AccountUserResp;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.Collection;

@FeignClient(name = "access-cargo-account-service")
@RibbonClient(name = "access-cargo-account-service")
public interface AccountClient {

    String PATH_ = "/access-cargo-account-service";
    String AUTHENTICATE = PATH_ + "/authenticate";
    String USERS_USER = PATH_ + "/users/user";
    String USERS_USER_ROLES = PATH_ + "/users/user/roles";

    @PostMapping(AUTHENTICATE)
    ResponseEntity authenticate(@RequestHeader(value = JwtConstants.TOKEN_HEADER) String token);

    @GetMapping(USERS_USER)
    ResponseEntity<AccountUserResp> getUsersByUser(@RequestHeader(value = JwtConstants.TOKEN_HEADER) String token);

    @GetMapping(USERS_USER_ROLES)
    ResponseEntity<Collection<String>> getUsersByUserRoles(@RequestHeader(value = JwtConstants.TOKEN_HEADER) String token);

}

