package com.access.cargo.dashboard.kpi.model.redis;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.LinkedList;

@Data
@NoArgsConstructor
@RedisHash("DashboardCcaReportDetailRedis")
public class DashboardCcaReportDetailRedis implements Serializable {
    private static final long serialVersionUID = -6470887159400228460L;
    @Id
    Long id;
    DashboardReportParams params;
    DashboardCcaReportDataDetailRedis data;
}
