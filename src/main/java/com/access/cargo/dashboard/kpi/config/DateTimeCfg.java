package com.access.cargo.dashboard.kpi.config;

import org.springframework.stereotype.Component;

import java.time.ZoneId;

@Component
public class DateTimeCfg {
    public static final ZoneId ZONE_ID = ZoneId.of("Asia/Jakarta");
}
