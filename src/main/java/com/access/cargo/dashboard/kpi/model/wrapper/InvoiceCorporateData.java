package com.access.cargo.dashboard.kpi.model.wrapper;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class InvoiceCorporateData implements Serializable {

    private static final long serialVersionUID = -2875177390216401876L;

    String corporateName;
    String addressLine1;
    String addressLine2;
    String bankName;
    String bankAccountName;
    String bankAccountNo;
    String empId;
    String empName;
    String empPosition;
}
